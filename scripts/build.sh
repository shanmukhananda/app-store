#!/bin/bash

cores=$(nproc)
script_dir=$(realpath $(dirname $0))
root_dir=$(realpath ${script_dir}/..)
build_dir=${root_dir}/build

mkdir -p ${build_dir}
cd ${build_dir}

cmake ${root_dir} \
    -DENABLE_WARNINGS=ON \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DENABLE_PROFILING=OFF
 
cmake --build ${build_dir} -- -j${cores}
