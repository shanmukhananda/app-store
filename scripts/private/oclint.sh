#/bin/bash

script_dir=$(realpath $(dirname $0))
root_dir=$(realpath ${script_dir}/../..)
build_dir=${root_dir}/build

rm -rf ${build_dir}

${root_dir}/scripts/build.sh

cd ${build_dir}

oclint-json-compilation-database -p ${build_dir}
