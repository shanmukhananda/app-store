#/bin/bash

script_dir=$(realpath $(dirname $0))
root_dir=$(realpath ${script_dir}/../..)

find ${root_dir}/apps -name '*.cpp' \
    -o -name '*.hpp' \
    -o -name '*.cc' \
    -o -name '*.hh' \
    -o -name '*.c' \
    -o -name '*.h' \
    | xargs clang-format -i -style=file
