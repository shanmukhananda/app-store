#include "Configuration.hpp"

Configuration* Configuration::m_pInstance = NULL; //!OCLINT (old code using
                                                  //! singleton)

Configuration::Configuration()
{
}

QString Configuration::IpAddress()
{
    return m_IpAddress;
}

QString Configuration::Port()
{
    return QString("6667");
}

void Configuration::setIpAddress(const QString& ipAddr)
{
    m_IpAddress = ipAddr;
}

Configuration* Configuration::Instance()
{
    if (NULL == m_pInstance)
    {
        m_pInstance = new Configuration();
    }

    return m_pInstance;
}
