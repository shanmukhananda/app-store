#pragma once

#include "LocationInput.hpp"
#include "Map.hpp"
#include "Poistioning.hpp"
#include "RouteCalculation.hpp"

#include <QMainWindow>

namespace Ui
{
class NaviClient;
}

class NaviClient : public QMainWindow
{
    Q_OBJECT

public:
    explicit NaviClient(QWidget* parent = 0);
    ~NaviClient();
    void showIpAddressDialog();

private slots:
    void on_pushButton_Map_clicked();

    void on_pushButton_LI_clicked();

    void on_pushButton_Positioning_clicked();

    void on_pushButton_Route_clicked();

private:
    Ui::NaviClient* ui;

    Map* m_pMapPage;
    LocationInput* m_LocationInput;
    Poistioning* m_pPositioning;
    RouteCalculation* m_pRouteCalc;
};
