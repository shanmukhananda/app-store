#include "Map.hpp"

#include "ui_Map.h"

Map::Map(QWidget* parent) : QDialog(parent), ui(new Ui::Map)
{
    ui->setupUi(this);
}

Map::~Map()
{
    delete ui;
}

void Map::on_comboBox_MapVisibility_currentIndexChanged(const QString& arg1)
{
    Logic::Instance()->vHandleMapVisibility(arg1);
}

void Map::on_comboBox_MapViewtype_currentIndexChanged(const QString& arg1)
{
    Logic::Instance()->vHandleMapViewType(arg1);
}

void Map::on_comboBox_MapHeading_currentIndexChanged(const QString& arg1)
{
    Logic::Instance()->vHandleMapHeading(arg1);
}
