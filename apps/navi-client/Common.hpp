#pragma once

// QT Header files
#include <QDebug>
#include <QDialog>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QMutex>
#include <QObject>
#include <QString>
#include <QStringListModel>
#include <QTimer>
#include <QtDBus/QDBusArgument>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QtDBus>
#include <QtGlobal>

// C, C++
#include <fstream>
#include <map>
#include <string>

// todo: remove project specific header files as it would
// cause long compilation times on change
// User defined Header files
#include "Configuration.hpp"
#include "Interface.hpp"

#include "Logger.hpp"
#include "Logic.hpp"
#include "Service.hpp"

#include "UILogicData.hpp"
#include "Utility.hpp"

#include "opennav/OpenNavNDR.h"
#include "opennav/OpenNavTypes.h"

const QString NAVI_SERVICE = "com.harman.service.Navigation";
const QString NAVI_PATH = "/com/harman/service/Navigation";

const QString NDR_SERVICE = "com.harman.service.NDR";
const QString NDR_PATH = "/com/harman/service/NDR";

const QString CAN_HMI_SERVICE = "com.harman.service.CAN_HMI";
const QString CAN_HMI_PATH = "/com/harman/service/CAN_HMI";

const QString DBUS_INTERFACE = "com.harman.ServiceIpc";
const QString DBUS_INVOKE = "Invoke";
const QString DBUS_EMIT = "Emit";
const QString TARGET_NAME = "NavigationDevice";

const QString NAVIGATION = "Navigation";
const QString NDR = "NDR";

/**
  @links
  * http://smcv.pseudorandom.co.uk/2008/11/nonblocking/
  *
  http://stackoverflow.com/questions/22607950/qt-slot-pointer-to-member-function-error
  *
  http://stackoverflow.com/questions/10405739/const-ref-when-sending-signals-in-qt
  * http://doc.qt.io/qt-5/implicit-sharing.html
  *
  http://stackoverflow.com/questions/10405739/const-ref-when-sending-signals-in-qt
  *
  http://stackoverflow.com/questions/1935147/argument-type-for-qt-signal-and-slot-does-const-reference-qualifiers-matters
  */
