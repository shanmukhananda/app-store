#pragma once

#include <iostream>
#include <string>

/*
 * EIS = Encoded Information String
 */
class EIS
{
public:
    std::string sGetTagData(const std::string& tag);
    void vAddTagValue(const std::string& tag, const std::string& value);
    void vAppend(const std::string& str);
    std::string GetData();

    EIS(const std::string& data);
    EIS();
    ~EIS();

private:
    std::string m_EISData;
    struct StringPOS
    {
        std::string data;
        size_t pos;
    };
    StringPOS
    getTagDataUnformated(const std::string& sEIS, const std::string& sTag,
                         size_t startIndex = 0);
};
