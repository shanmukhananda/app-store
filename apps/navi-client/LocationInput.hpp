#pragma once

#include "KeyBoard.hpp"

#include <QDialog>

namespace Ui
{
class LocationInput;
}

class LocationInput : public QDialog
{
    Q_OBJECT

public:
    explicit LocationInput(QWidget* parent = 0);
    ~LocationInput();

private slots:
    void on_pushButton_Country_clicked();

private:
    Ui::LocationInput* ui;
    KeyBoard* m_keyboard;
};
