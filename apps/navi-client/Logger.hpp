#pragma once

#include "Common.hpp"

#include <QObject>

class Logger : public QObject
{
    Q_OBJECT
public:
    static Logger* Instance();

    void DR_Speed(int speed);
    void Inst_Speed(int speed);
    void vSetLogState(bool state);

signals:

public slots:
    void vLogSpeed();

private:
    explicit Logger(QObject* parent = 0);
    static Logger* m_pInstance;

    QMutex m_dr_speedMutex;
    QMutex m_inst_speedMutex;
    QTimer m_timer;

    int m_dr_speed;
    int m_inst_speed;
    std::ofstream oFile;
    bool m_bIsLogEnabled;
};
