#pragma once

#include "Common.hpp"

class Configuration
{
public:
    QString IpAddress();
    QString Port();

    void setIpAddress(const QString& ipAddr);
    static Configuration* Instance();

private:
    Configuration();
    QString m_IpAddress;
    QString m_Port();

    static Configuration* m_pInstance;
};
