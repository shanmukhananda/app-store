#include "DialogIP.hpp"
#include "Common.hpp"

#include "ui_DialogIP.h"

DialogIP::DialogIP(QWidget* parent) : QDialog(parent), ui(new Ui::DialogIP)
{
    ui->setupUi(this);
}

DialogIP::~DialogIP()
{
    delete ui;
}

void DialogIP::on_pushButton_Ok_clicked()
{
    QString ip_addr = ui->lineEditIpAddr->text();

    if (ip_addr != "")
    {
        Configuration::Instance()->setIpAddress(ip_addr);
        Service::Instance()->vConnectToDbus();
        close();
    }
}

void DialogIP::on_pushButton_Cancel_clicked()
{
    close();
}
