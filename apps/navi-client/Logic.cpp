#include "Common.hpp"
#include "Interface.hpp"

Logic* Logic::m_pInstance = NULL; //!OCLINT (old code using singleton)

Logic::Logic(QObject* parent) : QObject(parent)
{
    m_pLocationListModel = new QStringListModel(this);
    m_pSpellCharListModel = new QStringListModel(this);

    vRegisterLogicEventHandlers();
}

Logic* Logic::Instance()
{
    if (NULL == m_pInstance)
    {
        m_pInstance = new Logic();
    }

    return m_pInstance;
}

bool Logic::bIsCharInSpellList(QChar spellChar)
{
    QString spellCharStr = spellChar;
    QStringList spellCharList = m_pSpellCharListModel->stringList();
    return spellCharList.contains(spellCharStr, Qt::CaseInsensitive);
}

void Logic::vHandlePositioningEntry()
{
    // json get properties
    JsonGetPropertiesMethodCall jsonGetProp(NDR);
    jsonGetProp.vAddToInputProperty("SEN_GPSInfo");
    jsonGetProp.vAddToInputProperty("SEN_GyroSum");
    jsonGetProp.vAddToInputProperty("POS_CalibrationLevel");
    jsonGetProp.vAddToInputProperty("SEN_ReverseGear");
    jsonGetProp.vAddToInputProperty("POS_Position");
    jsonGetProp.vAddToInputProperty("SEN_Temperature");
    jsonGetProp.vSend(true);

    JsonEnableSignalOutputMethodCall ndrSignals;
    ndrSignals.vSetInSendSignals(true);
    ndrSignals.vSend(true);
}

void Logic::vRegisterLogicEventHandlers()
{
    connect(Service::Instance(),
            SIGNAL(SIG_SR_SENGPSInfo(const SENGPSInfoSignalResponse&)), this,
            SLOT(vSlotSenGpsInfoSignal(const SENGPSInfoSignalResponse&)));
    connect(Service::Instance(),
            SIGNAL(SIG_PR_SENGPSInfo(const SENGPSInfoProperty&)), this,
            SLOT(vSlotSenGpsInfoProperty(const SENGPSInfoProperty&)));
    connect(Service::Instance(),
            SIGNAL(SIG_SR_POSPosition(const POSPositionSignalResponse&)), this,
            SLOT(vSlotPOSPositionSignal(const POSPositionSignalResponse&)));
    connect(Service::Instance(),
            SIGNAL(SIG_PR_POSPosition(const POSPositionProperty&)), this,
            SLOT(vSlotPOSPositionProperty(const POSPositionProperty&)));
    connect(Service::Instance(),
            SIGNAL(SIG_SR_SENReverseGear(const SENReverseGearSignalResponse&)),
            this, SLOT(vSlotSENReverseGearSignal(
                      const SENReverseGearSignalResponse&)));
    connect(Service::Instance(),
            SIGNAL(SIG_PR_SENReverseGear(const SENReverseGearProperty&)), this,
            SLOT(vSlotSENReverseGearProperty(const SENReverseGearProperty&)));
    connect(Service::Instance(), SIGNAL(SIG_SR_POSCalibrationLevel(
                                     const POSCalibrationLevelSignalResponse&)),
            this, SLOT(vSlotPOSCalibirationSignal(
                      const POSCalibrationLevelSignalResponse&)));
    connect(
        Service::Instance(),
        SIGNAL(SIG_PR_POSCalibrationLevel(const POSCalibrationLevelProperty&)),
        this,
        SLOT(vSlotPOSCalibrationProperty(const POSCalibrationLevelProperty&)));
    connect(Service::Instance(),
            SIGNAL(SIG_SR_SENTemperature(const SENTemperatureSignalResponse&)),
            this, SLOT(vSlotSENTemperatureSignal(
                      const SENTemperatureSignalResponse&)));
    connect(Service::Instance(),
            SIGNAL(SIG_PR_SENTemperature(const SENTemperatureProperty&)), this,
            SLOT(vSlotSENTemperatureProperty(const SENTemperatureProperty&)));
    connect(Service::Instance(),
            SIGNAL(SIG_SR_SENGyroSum(const SENGyroSumSignalResponse&)), this,
            SLOT(vSlotSENGyroSumSignal(const SENGyroSumSignalResponse&)));
    connect(Service::Instance(),
            SIGNAL(SIG_PR_SENGyroSum(const SENGyroSumProperty&)), this,
            SLOT(vSlotSENGyroSumProperty(const SENGyroSumProperty&)));
    connect(Service::Instance(),
            SIGNAL(SIG_SR_InstSpeedValue(const InstSpeedValueSignalResponse&)),
            this,
            SLOT(vSlotInstSpeedValue(const InstSpeedValueSignalResponse&)));
}

void Logic::vSlotSenGpsInfoProperty(const SENGPSInfoProperty& gpsSen)
{
    /*
     * int antStateP,
                 int signalQualP,
                 int headingP,
                 int speedP,
                 int fixP,
                 int satsUsedP,
                 int satsVisiP,
                 int northSpeedP,
                 int eastSpeedP,
                 int verticalSpeedP,
                 int hdopP,
                 int pdopP,
                 int vdopP,
                 int height,
                 int horPosErrorP,
                 int vertPosErrorP
     * */
    UISenGPSData gpsProp(
        gpsSen.GetData().antennaState(), gpsSen.GetData().signalQuality(),
        gpsSen.GetData().heading(), gpsSen.GetData().speed(),
        gpsSen.GetData().fix(), gpsSen.GetData().satsUsed(),
        gpsSen.GetData().satsVisible(), gpsSen.GetData().northSpeed(),
        gpsSen.GetData().eastSpeed(), gpsSen.GetData().vertSpeed(),
        gpsSen.GetData().hdop(), gpsSen.GetData().pdop(),
        gpsSen.GetData().vdop(), gpsSen.GetData().height(),
        gpsSen.GetData().horPosError(), gpsSen.GetData().vertPosError());
    emit UISENGPSInfo(gpsProp);
}

void Logic::vSlotSenGpsInfoSignal(const SENGPSInfoSignalResponse& gpsSen)
{
    UISenGPSData gpsProp(gpsSen.antennaState(), gpsSen.signalQuality(),
                         gpsSen.heading(), gpsSen.speed(), gpsSen.fix(),
                         gpsSen.satsUsed(), gpsSen.satsVisible(),
                         gpsSen.northSpeed(), gpsSen.eastSpeed(),
                         gpsSen.vertSpeed(), gpsSen.hdop(), gpsSen.pdop(),
                         gpsSen.vdop(), gpsSen.height(), gpsSen.horPosError(),
                         gpsSen.vertPosError());
    emit UISENGPSInfo(gpsProp);
}

void Logic::vSlotPOSPositionProperty(const POSPositionProperty& posData)
{
    UIPosPositionData posProp(posData.GetData().DR_speed(),
                              posData.GetData().DR_distance(),
                              posData.GetData().DR_heading(),
                              posData.GetData().DR_latitude(),
                              posData.GetData().DR_longitude());

    emit UIPOSPositionInfo(posProp);
}

void Logic::vSlotPOSPositionSignal(const POSPositionSignalResponse& posData)
{
    UIPosPositionData posProp(posData.DR_speed(), posData.DR_distance(),
                              posData.DR_heading(), posData.DR_latitude(),
                              posData.DR_longitude());

    emit UIPOSPositionInfo(posProp);
}

void Logic::vSlotPOSCalibirationSignal(
    const POSCalibrationLevelSignalResponse& calibData)
{
    UICalibrationLevel cLevel(calibData.level());
    emit UICalibrationLevelInfo(cLevel);
}

void Logic::vSlotPOSCalibrationProperty(
    const POSCalibrationLevelProperty& calibData)
{
    UICalibrationLevel cLevel(calibData.GetData().level());
    emit UICalibrationLevelInfo(cLevel);
}

void Logic::vSlotSENTemperatureSignal(
    const SENTemperatureSignalResponse& temperData)
{
    UITemperature temper(temperData.temperature());
    emit UITemperatureInfo(temper);
}

void Logic::vSlotSENTemperatureProperty(const SENTemperatureProperty& temperData)
{
    UITemperature temper(temperData.GetData().temperature());
    emit UITemperatureInfo(temper);
}

void Logic::vSlotSENReverseGearSignal(
    const SENReverseGearSignalResponse& revgear)
{
    UIReverseGear rGear(revgear.reverseValue());
    emit UIReverseGearInfo(rGear);
}

void Logic::vSlotSENReverseGearProperty(const SENReverseGearProperty& revgear)
{
    UIReverseGear rGear(revgear.GetData().reverseValue());
    emit UIReverseGearInfo(rGear);
}

void Logic::vSlotSENGyroSumSignal(const SENGyroSumSignalResponse& gSum)
{
    UIGyroSum gyro(gSum.gyroSum());
    emit UIGyroSumInfo(gyro);
}

void Logic::vSlotSENGyroSumProperty(const SENGyroSumProperty& gSum)
{
    UIGyroSum gyro(gSum.GetData().gyroSum());
    emit UIGyroSumInfo(gyro);
}

void Logic::vSlotInstSpeedValue(const InstSpeedValueSignalResponse& instSpeed)
{
    UIInstSpeed speed(instSpeed.speedValue());
    emit UIInstSpeedInfo(speed);
}

void Logic::vHandleMapVisibility(const QString& /*args*/)
{
    /*
    std::map<std::string, std::string> Table;
    Table["<MID>"] = "map";
    if("False" == args)
    {
        Table["<MVI>"] = "0";
    }
    else if ("Freeze" == args)
    {
        Table["<MVI>"] = "2";
    }
    else if("True" == args)
    {
        Table["<MVI>"] = "1";
    }
    else
    {
        qWarning("%s(%d):%s", __FILE__, __LINE__,
                 "WARNING: Undefined argument");
    }
    */
}

void Logic::vHandleMapViewType(const QString& /*args*/)
{
    /*
    std::map<std::string, std::string> Table;
    Table["<MID>"] = "map";
    if("2D" == args)
    {
        Table["<MVT>"] = QString::number(
                             static_cast<int>(view2D)).toStdString();
    }
    else if ("3D" == args)
    {
        Table["<MVT>"] = QString::number(
                             static_cast<int>(view3D)).toStdString();
    }
    else if ("ViewBird" == args)
    {
        Table["<MVT>"] = QString::number(
                             static_cast<int>(viewBird)).toStdString();
    }
    else if("ViewGarage" == args)
    {
        Table["<MVT>"] = QString::number(
                             static_cast<int>(viewGarage)).toStdString();
    }
    else if("RoutePreview" ==  args)
    {
        Table["<MVT>"] = QString::number(
                             static_cast<int>(viewPreview)).toStdString();
    }
    else
    {
        qWarning("%s(%d):%s", __FILE__, __LINE__,
                 "WARNING: Undefined argument");
    }
    */
}

void Logic::vHandleMapHeading(const QString& /*args*/)
{
    /*
    std::map<std::string, std::string> Table;
    Table["<MID>"] = "map";
    if("NorthUp" == args)
    {
        Table["<MH>"] = QString::number(
                            static_cast<int>(northUp)).toStdString();
    }
    else if("HeadingUp" == args)
    {
        Table["<MH>"] = QString::number(
                            static_cast<int>(headingUp)).toStdString();
    }
    else if("DestinationUp" == args)
    {
        Table["<MH>"] = QString::number(
                            static_cast<int>(destinationUp)).toStdString();
    }
    else
    {
        qWarning("%s(%d):%s", __FILE__, __LINE__,
                 "WARNING: Undefined argument");
    }
    */
}

void Logic::vHandleLIEnterCountry()
{
    /*
    m_currentLIData = "Country";
    std::map<std::string, std::string> Table;
    Table["<IFT>"] = "LI";
    Table["<CTR>"]= "";
    */
}

void Logic::vHandleLIGetSpell(QString str)
{
    qDebug() << __FUNCTION__ << str;
    if (m_currentLIData == "Country")
    {
        qDebug() << m_currentLIData;
    }
}

QStringListModel* Logic::pLocationListModel()
{
    return m_pLocationListModel;
}

QStringListModel* Logic::pSpellCharListModel()
{
    return m_pSpellCharListModel;
}

/*

void Logic::vSlotNextAvailableCharacters(QString spellChars)
{
    qDebug() << spellChars;
    QStringList spellCharsList;

    for (int i = 0; i < spellChars.size(); ++i)
    {
        spellCharsList << static_cast<QString>(spellChars.at(i));
    }

    m_pSpellCharListModel->setStringList(spellCharsList);
}
*/
