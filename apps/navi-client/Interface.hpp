#pragma once

#include "Common.hpp"
#include "EIS.hpp"

#include "opennav/OpenNavNDR.h"
#include "opennav/OpenNavTypes.h"

//----------------------------------------------------------------------------//
// Interfaces
//----------------------------------------------------------------------------//
class IMethodCall
{
public:
    // Warning: Do not override the default paramter in derrived class
    // decleration.
    virtual void vSend(bool bIsSynchronous) = 0;
};

class IMethodResult
{
private:
    virtual void vDemarshall(const QString& methodResult) = 0;
};

class ISignalResponse
{
private:
    virtual void vDemarshall(const QString& signalResponse) = 0;
};

class IProperty
{
public:
    virtual QString GetName() const = 0;
    virtual ~IProperty()
    {
    }
};

//----------------------------------------------------------------------------//
// Methods
//----------------------------------------------------------------------------//
class MapControlsMethodCall : public IMethodCall
{
public:
    void vInsertToMapEIS(const std::string& tag, const std::string& value);
    void vSend(bool bIsSynchronous);

private:
    EIS m_aMapEIS;
};
//----------------------------------------------------------------------------//
class MapControlsMethodResult : public IMethodResult
{
public:
    MapControlsMethodResult(const QString& methodResult);
    EIS outMapControls();
    eNavValidState resultCode();

private:
    void vDemarshall(const QString& methodResult);
    EIS m_aOutMapControls;
    eNavValidState m_iResultCode;
};
//----------------------------------------------------------------------------//
class InfoGetListMethodCall : public IMethodCall
{
public:
    void vInsertToInputEIS(const std::string& tag, const std::string& value);
    void vSetCancelPreviousRequest(bool bCancelPreviousRequest);
    void vSetInResultCount(int iInResoultCount);
    void vSetInSortOption(int iInSortOption);
    void vSetInStartIndex(int iInStartIndex);
    void vSend(bool bIsSynchronous);
    InfoGetListMethodCall();

private:
    bool m_bCancelPreviousRequest;
    EIS m_aInputEIS;
    int m_iInResultCount;
    int m_iInSortOption;
    int m_iInStartIndex;
};
//----------------------------------------------------------------------------//
class InfoGetListMethodResult : public IMethodResult
{
public:
    InfoGetListMethodResult(const QString& methodResult);
    bool complete();
    const std::vector<EIS>& itemList();
    const std::vector<std::string>& outfieldOptions();
    EIS outputEIS();
    int outresultCount();
    int outsortOption();
    int outstartIndex();
    EIS respEIS();
    eNavValidState resultCode();

private:
    void vDemarshall(const QString& methodResult);
    bool m_bComplete;
    std::vector<EIS> m_aItemList;
    std::vector<std::string> m_aOutfieldOptions;
    EIS m_aOutputEIS;
    int m_iOutResultCount;
    int m_eOutSortOption;
    int m_iOutStartIndex;
    EIS m_aRespEIS;
    eNavValidState m_iResultCode;
};
//----------------------------------------------------------------------------//
class MapSetWorldElementPropertiesMethodCall : public IMethodCall
{
public:
    void vInsertToMapEIS(const std::string& tag, const std::string& value);
    void vInsertToItemList(EIS eisTable);
    void vSetWeFunction(int weFunc);
    void vSend(bool bIsSynchronous);
    MapSetWorldElementPropertiesMethodCall();

private:
    std::vector<EIS> m_aItemList;
    EIS m_aMapEIS;
    int m_eWeFunction;
};
//----------------------------------------------------------------------------//
class MapSetWorldElementPropertiesMethodResult : public IMethodResult
{
public:
    MapSetWorldElementPropertiesMethodResult(const QString& methodResult);
    const std::vector<EIS>& itemList();
    EIS mapEIS();
    int weFunctions();
    eNavValidState resultCode();

private:
    void vDemarshall(const QString& methodResult);
    std::vector<EIS> m_aItemList;
    EIS m_aMapEIS;
    int m_eWeFunctions;
    eNavValidState m_eResultCode;
};
//----------------------------------------------------------------------------//
class InfoGetDetailsMethodsCall : public IMethodCall
{
public:
    void vInsertToInputEIS(const std::string& tag, const std::string& value);
    void vInsertToRequestedFields(const std::string& value);
    void vSend(bool bIsSynchronous);

private:
    EIS m_aInputEIS;
    std::vector<std::string> m_aRequestedFields;
};
//----------------------------------------------------------------------------//
class InfoGetDetailsMethodsResult : public IMethodResult
{
public:
    InfoGetDetailsMethodsResult(const QString& methodResult);
    EIS outputEIS();
    EIS respEIS();
    const std::vector<std::string> respFields();
    int resultCode();

private:
    EIS m_aOutputEIS;
    EIS m_aRespEIS;
    std::vector<std::string> m_aRespFields;
    int m_iResultCode;
    void vDemarshall(const QString& methodResult);
};
//----------------------------------------------------------------------------//
class InfoGetSpellMethodCall : public IMethodCall
{
public:
    void vInsertToInputEIS(const std::string& tag, const std::string& value);
    void vSend(bool bIsSynchronous);

private:
    EIS m_aInputEIS;
};
//----------------------------------------------------------------------------//
class JsonGetPropertiesMethodCall : public IMethodCall
{
public:
    JsonGetPropertiesMethodCall(const QString& service);
    void vAddToInputProperty(const std::string& inprop);
    void vSend(bool bIsSynchronous);

private:
    std::vector<std::string> m_aInputProperties;
    QString m_service;
};
//----------------------------------------------------------------------------//
class JsonGetPropertiesMethodResult : public IMethodResult
{
public:
    JsonGetPropertiesMethodResult(const QString& methodResult);
    QJsonObject outprop() const;

private:
    void vDemarshall(const QString& methodResult);
    QJsonObject m_jOutprop;
};
//----------------------------------------------------------------------------//
class JsonEnableSignalOutputMethodCall : public IMethodCall
{
public:
    JsonEnableSignalOutputMethodCall();
    void vSetInSendSignals(bool value);
    void vSend(bool bIsSynchronous);

private:
    bool m_bInSendSignals;
};
//----------------------------------------------------------------------------//
class JsonEnableSignalOutputMethodResult : public IMethodResult
{
public:
    JsonEnableSignalOutputMethodResult(const QString& methodResult);
    QString outprop();

private:
    void vDemarshall(const QString& methodResult);
    QString m_outProp;
};
//----------------------------------------------------------------------------//
class POSResetCalibrationMethodCall : public IMethodCall
{
public:
    void vSend(bool bIsSynchronous);
};
//----------------------------------------------------------------------------//
class POSResetCalibrationMethodResult : public IMethodResult
{
};
//----------------------------------------------------------------------------//
//
//----------------------------------------------------------------------------//
class InstSpeedValueSignalResponse : public ISignalResponse
{
public:
    InstSpeedValueSignalResponse(const QString& signalResponse);
    int speedValue() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_speedValue;
};

//----------------------------------------------------------------------------//
class PosPositionInformationSignalResponse : public ISignalResponse
{
public:
    PosPositionInformationSignalResponse(const QString& signalResponse);
    EIS posInfo();

private:
    void vDemarshall(const QString& signalResponse);
    EIS m_aPosInfo;
};
//----------------------------------------------------------------------------//
class SENGyroSumSignalResponse : public ISignalResponse
{
public:
    SENGyroSumSignalResponse(const QString& signalResponse);
    int gyroSum() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_iGyroSum;
};
//----------------------------------------------------------------------------//
class POSCalibrationLevelSignalResponse : public ISignalResponse
{
public:
    POSCalibrationLevelSignalResponse(const QString& signalResponse);
    int level() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_iLevel;
};
//----------------------------------------------------------------------------//
class SENReverseGearSignalResponse : public ISignalResponse
{
public:
    SENReverseGearSignalResponse(const QString& signalResponse);
    int reverseValue() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_iReverseValue;
};
//----------------------------------------------------------------------------//
class SENTemperatureSignalResponse : public ISignalResponse
{
public:
    SENTemperatureSignalResponse(const QString& signalResponse);
    int temperature() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_iTemperature;
};
//----------------------------------------------------------------------------//
class POSPositionSignalResponse : public ISignalResponse
{
public:
    POSPositionSignalResponse(const QString& signalResponse);
    int DR_distance() const;
    int DR_heading() const;
    int DR_latitude() const;
    int DR_longitude() const;
    int DR_speed() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_iDRDistance;
    int m_iDRHeading;
    int m_iDRLatitude;
    int m_iDRLongitude;
    int m_iDRSpeed;
};
//----------------------------------------------------------------------------//
class SENGPSInfoSignalResponse : public ISignalResponse
{
public:
    SENGPSInfoSignalResponse(const QString& signalResponse);
    int antennaState() const;
    int day() const;
    int eastSpeed() const;
    int fix() const;
    int hdop() const;
    int heading() const;
    int height() const;
    int horPosError() const;
    int hrs() const;
    int latitude() const;
    int longitude() const;
    int min() const;
    int month() const;
    int northSpeed() const;
    int pdop() const;
    int satsUsed() const;
    int satsVisible() const;
    int sec() const;
    int signalQuality() const;
    int speed() const;
    int vdop() const;
    int vertPosError() const;
    int vertSpeed() const;
    int year() const;

private:
    void vDemarshall(const QString& signalResponse);
    int m_iAntennaState;
    int m_iDay;
    int m_iEastSpeed;
    int m_iFix;
    int m_iHdop;
    int m_iHeading;
    int m_iHeight;
    int m_iHorPosError;
    int m_iHrs;
    int m_iLatitude;
    int m_iLongitude;
    int m_iMin;
    int m_iMonth;
    int m_iNorthSpeed;
    int m_iPdop;
    int m_iSatsUsed;
    int m_iSatsVisible;
    int m_iSec;
    int m_iSignalQuality;
    int m_iSpeed;
    int m_iVdop;
    int m_iVertPosError;
    int m_iVertSpeed;
    int m_iYear;
};

//----------------------------------------------------------------------------//
//
//----------------------------------------------------------------------------//
class SENGPSInfoProperty : public IProperty
{
public:
    SENGPSInfoProperty(const QString& property);
    const SENGPSInfoSignalResponse& GetData() const;
    QString GetName() const;
    ~SENGPSInfoProperty();

private:
    SENGPSInfoSignalResponse m_data;
};
//----------------------------------------------------------------------------//
class POSPositionProperty : public IProperty
{
public:
    POSPositionProperty(const QString& property);
    const POSPositionSignalResponse& GetData() const;
    QString GetName() const;
    ~POSPositionProperty();

private:
    POSPositionSignalResponse m_data;
};
//----------------------------------------------------------------------------//
class POSCalibrationLevelProperty : public IProperty
{
public:
    POSCalibrationLevelProperty(const QString& property);
    const POSCalibrationLevelSignalResponse& GetData() const;
    QString GetName() const;
    ~POSCalibrationLevelProperty();

private:
    POSCalibrationLevelSignalResponse m_data;
};
//----------------------------------------------------------------------------//
class SENReverseGearProperty : public IProperty
{
public:
    SENReverseGearProperty(const QString& property);
    const SENReverseGearSignalResponse& GetData() const;
    QString GetName() const;
    ~SENReverseGearProperty();

private:
    SENReverseGearSignalResponse m_data;
};
//----------------------------------------------------------------------------//
class SENGyroSumProperty : public IProperty
{
public:
    SENGyroSumProperty(const QString& property);
    const SENGyroSumSignalResponse& GetData() const;
    QString GetName() const;
    ~SENGyroSumProperty();

private:
    SENGyroSumSignalResponse m_data;
};
//----------------------------------------------------------------------------//
class SENTemperatureProperty : public IProperty
{
public:
    SENTemperatureProperty(const QString& property);
    const SENTemperatureSignalResponse& GetData() const;
    QString GetName() const;
    ~SENTemperatureProperty();

private:
    SENTemperatureSignalResponse m_data;
};
//----------------------------------------------------------------------------//
