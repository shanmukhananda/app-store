#include "Common.hpp"

Service* Service::m_pInstance = NULL; //!OCLINT (old code using singleton)

Service::Service(QObject* parent) : QObject(parent), m_bus(TARGET_NAME)
{
}

Service* Service::Instance()
{
    if (NULL == m_pInstance)
    {
        m_pInstance = new Service();
    }

    return m_pInstance;
}

void Service::vConnectToDbus()
{
    qDebug() << "Connecting...";
    QString ip_addr = Configuration::Instance()->IpAddress();
    QString port = Configuration::Instance()->Port();
    QString address = QString("tcp:host=") + ip_addr + QString(",port=") + port;

    qDebug() << address;
    m_bus = QDBusConnection::connectToBus(address, TARGET_NAME);

    if (m_bus.isConnected())
    {
        qDebug() << "Successfully Connected!";
        vRegisterForSingals();
        vCreateMethodAndSlotMap();
        vCreateSignalMap();
    }
    else
    {
        qFatal("%s(%d):%s, Last error = %s", __FILE__, __LINE__,
               "FATAL ERROR in Connection",
               m_bus.lastError().name().toStdString().c_str());
    }
}

void Service::vInvoke(const QString& sService, const QString& sHighLevelAPI,
                      const QString& sParameter, bool bIsSynchronous)
{
    std::map<std::string, tMethodSlot>::iterator iter =
        m_MethodSlotMap.find(sHighLevelAPI.toStdString());

    if (iter != m_MethodSlotMap.end())
    {
        if (bIsSynchronous)
        {
            vSync_SendMethod(sService, sHighLevelAPI, sParameter,
                             iter->second.methodHandler);
        }
        else
        {
            vAsync_SendMethod(sService, sHighLevelAPI, sParameter,
                              iter->second.slotFunc);
        }
    }
    else
    {
        qCritical("%s(%d):ERROR No Method map found for method %s", __FILE__,
                  __LINE__, sHighLevelAPI.toStdString().c_str());
    }
}

void Service::vRegisterForSingals()
{
    // Connect the signal handler for all signals
    bool bIsConnectedNavi =
        m_bus.connect(NAVI_SERVICE, NAVI_PATH, DBUS_INTERFACE, DBUS_EMIT, this,
                      SLOT(vOnSignalResponse(const QString&, const QString&)));
    assert(bIsConnectedNavi);

    bool bIsConnectedNDR =
        m_bus.connect(NDR_SERVICE, NDR_PATH, DBUS_INTERFACE, DBUS_EMIT, this,
                      SLOT(vOnSignalResponse(const QString&, const QString&)));

    assert(bIsConnectedNDR);

    bool bIsConnectedCanHMI =
        m_bus.connect(CAN_HMI_SERVICE, CAN_HMI_PATH, DBUS_INTERFACE, DBUS_EMIT,
                      this,
                      SLOT(vOnSignalResponse(const QString&, const QString&)));

    assert(bIsConnectedCanHMI);

    bool bIsConnected =
        bIsConnectedNavi && bIsConnectedNDR && bIsConnectedCanHMI;

    if (bIsConnected)
    {
        qDebug("Signal queueing request successfull");
    }
    else
    {
        qCritical("ERROR in queueing signal");
    }
}

QDBusMessage
Service::oGetDbusMessage(const QString& service, const QString& sAPI,
                         const QString& sParam)
{
    QDBusMessage oMessage;

    if (NAVIGATION == service)
    {
        oMessage = oGetNaviDbusMessage(sAPI, sParam);
    }
    else if (NDR == service)
    {
        oMessage = oGetNDRDbusMessage(sAPI, sParam);
    }
    else
    {
        qCritical("%s(%d):ERROR Service:'%s' is currently not supported",
                  __FILE__, __LINE__, service.toStdString().c_str());
    }
    return oMessage;
}

QDBusMessage
Service::oGetNaviDbusMessage(const QString& sAPI, const QString& sParam)
{
    QDBusMessage oMessage = QDBusMessage::createMethodCall(
        NAVI_SERVICE, NAVI_PATH, DBUS_INTERFACE, DBUS_INVOKE);

    QList<QVariant> lData;
    lData.append(sAPI);
    lData.append(sParam);
    oMessage.setArguments(lData);
    return oMessage;
}

QDBusMessage
Service::oGetNDRDbusMessage(const QString& sAPI, const QString& sParam)
{
    QDBusMessage oMessage = QDBusMessage::createMethodCall(
        NDR_SERVICE, NDR_PATH, DBUS_INTERFACE, DBUS_INVOKE);

    QList<QVariant> lData;
    lData.append(sAPI);
    lData.append(sParam);
    oMessage.setArguments(lData);
    return oMessage;
}

void Service::vSync_SendMethod(const QString& service, const QString& method,
                               const QString& sParam,
                               tMethodHandler syncCallback)
{
    QDBusMessage oMessage = oGetDbusMessage(service, method, sParam);

    const QDBusMessage& data = m_bus.call(oMessage);

    const QList<QVariant>& outArgs = data.arguments();

    if (outArgs.empty())
    {
        qCritical(
            "%s(%d): ERROR in dbus reply,Error Message = %s, ErrorName = %s",
            __FILE__, __LINE__, data.errorMessage().toStdString().c_str(),
            data.errorName().toStdString().c_str());

        return;
    }

    const QVariant& first = outArgs.at(0);
    const QString& methodReply = first.toString();
    (this->*syncCallback)(methodReply);
}

void Service::vAsync_SendMethod(const QString& service, const QString& method,
                                const QString& sParam, tSlot slotFuncAsync)
{
    QDBusMessage oMessage = oGetDbusMessage(service, method, sParam);

    bool bIsQueued = m_bus.callWithCallback(oMessage, this, slotFuncAsync);

    if (false == bIsQueued)
    {
        qCritical("%s(%d):%s", __FILE__, __LINE__, "ERROR in queueing method");
    }
}

// high ncss method, allowed as more messges need to handled
void Service::vDispatchProperties( //!OCLINT
    const JsonGetPropertiesMethodResult& jsonGetPropMR)
{
    const QJsonObject& outprop = jsonGetPropMR.outprop();
    const QStringList& keyList = outprop.keys();
    foreach (const QString& key, keyList)
    {
        if ("SEN_GPSInfo" == key)
        {
            QJsonObject gpsInfo = outprop["SEN_GPSInfo"].toObject();
            QString gpsInfoStr = QJsonDocument(gpsInfo).toJson();
            vOn_PR_SEN_GPSInfo(gpsInfoStr);
        }
        else if ("SEN_GyroSum" == key)
        {
            QJsonObject gyroSum = outprop["SEN_GyroSum"].toObject();
            QString gyroSymStr = QJsonDocument(gyroSum).toJson();
            vOn_PR_SEN_GyroSum(gyroSymStr);
        }
        else if ("POS_Position" == key)
        {
            QJsonObject posPosition = outprop["POS_Position"].toObject();
            QString posPositionStr = QJsonDocument(posPosition).toJson();
            vOn_PR_POS_Position(posPositionStr);
        }
        else if ("POS_CalibrationLevel" == key)
        {
            QJsonObject calibLevel = outprop["POS_CalibrationLevel"].toObject();
            QString calibLevelStr = QJsonDocument(calibLevel).toJson();
            vOn_PR_POS_CalibrationLevel(calibLevelStr);
        }
        else if ("SEN_ReverseGear" == key)
        {
            QJsonObject senReverse = outprop["SEN_ReverseGear"].toObject();
            QString reverseStr = QJsonDocument(senReverse).toJson();
            vOn_PR_SEN_ReverseGear(reverseStr);
        }
        else if ("SEN_Temperature" == key)
        {
            QJsonObject senTemper = outprop["SEN_Temperature"].toObject();
            QString temperStr = QJsonDocument(senTemper).toJson();
            vOn_PR_SEN_Temperature(temperStr);
        }
        else
        {
            qCritical("%s(%d):ERROR Property handler not found for property %s",
                      __FILE__, __LINE__, key.toStdString().c_str());
        }
    }
}

void Service::vAddToMethodSlotMap(std::string method, tMethodSlot methodSlot)
{
    bool bIsNewInsertion =
        (m_MethodSlotMap.insert(
             std::pair<std::string, tMethodSlot>(method, methodSlot)))
            .second;

    if (false == bIsNewInsertion)
    {
        qWarning(
            "%s(%d):WARNING: Trying to insert already existing Method %s \
                 to Method-Slot Map",
            __FILE__, __LINE__, method.c_str());
    }
}

void Service::vAddToSignalMap(std::string signal, tSignalHandler slotFunc)
{
    bool bIsNewInsertion =
        (m_SignalMap.insert(
             std::pair<std::string, tSignalHandler>(signal, slotFunc)))
            .second;

    if (false == bIsNewInsertion)
    {
        qWarning(
            "%s(%d):WARNING: Trying to insert already existing Signal %s \
                 to Signal Map",
            __FILE__, __LINE__, signal.c_str());
    }
}

void Service::vOnSignalResponse(const QString& sSignalName,
                                const QString& signalReply)
{
    // todo: remove
    if ("Inst_Speed_Value" == sSignalName.toStdString())
    {
        qDebug() << sSignalName << signalReply;
    }
    std::map<std::string, tSignalHandler>::iterator iter =
        m_SignalMap.find(sSignalName.toStdString());

    if (iter != m_SignalMap.end())
    {
        tSignalHandler slotFunc = iter->second;
        (this->*slotFunc)(signalReply);
    }
}

void Service::vOnError(const QString& reply)
{
    qDebug() << reply;
    qCritical("%s(%d):ERROR in method response", __FILE__, __LINE__);
}

// Methods
void Service::vOn_MR_MAP_Controls(const QString& reply)
{
    MapControlsMethodResult mapControlsMR(reply);
    emit SIG_MR_MapControls(mapControlsMR);
}

void Service::vOn_MR_Info_GetSpell(const QString& /*reply*/)
{
}

void Service::vOn_MR_Info_GetAvailableInformation(const QString& /*reply*/)
{
}

void Service::vOn_MR_Info_GetList(const QString& /*reply*/)
{
}

void Service::vOn_MR_MAP_SetWorldElementProperties(const QString& reply)
{
    MapSetWorldElementPropertiesMethodResult mapWorldEle(reply);
    emit SIG_MR_MapSetWorldElementProperties(mapWorldEle);
}

void Service::vOn_MR_JSON_GetProperties(const QString& reply)
{
    JsonGetPropertiesMethodResult jsonGetProp(reply);
    vDispatchProperties(jsonGetProp);
    emit SIG_MR_JsonGetProperties(jsonGetProp);
}

void Service::vOn_MR_JSON_EnableSignalOutput(const QString& reply)
{
    qDebug() << reply;
}

void Service::vOn_MR_POS_ResetCalibration(const QString& reply)
{
    qDebug() << reply;
}

// Signals
void Service::vOn_SR_POS_PositionInformation(const QString& reply)
{
    PosPositionInformationSignalResponse positionInfo(reply);
    emit SIG_SR_PosPositionInformation(positionInfo);
}

void Service::vOn_SR_RT_NextManeuverInfo(const QString& /*reply*/)
{
}

void Service::vOn_SR_ADA_PromptReady(const QString& /*reply*/)
{
}

void Service::vOn_SR_SEN_GPSInfo(const QString& reply)
{
    SENGPSInfoSignalResponse gpsSenInfo(reply);
    emit SIG_SR_SENGPSInfo(gpsSenInfo);
}

void Service::vOn_SR_SEN_GyroSum(const QString& reply)
{
    SENGyroSumSignalResponse gSum(reply);
    emit SIG_SR_SENGyroSum(gSum);
}

void Service::vOn_SR_POS_CalibrationLevel(const QString& reply)
{
    POSCalibrationLevelSignalResponse calib(reply);
    emit SIG_SR_POSCalibrationLevel(calib);
}

void Service::vOn_SR_SEN_ReverseGear(const QString& reply)
{
    SENReverseGearSignalResponse revGear(reply);
    emit SIG_SR_SENReverseGear(revGear);
}

void Service::vOn_SR_POS_Position(const QString& reply)
{
    POSPositionSignalResponse posPosition(reply);
    emit SIG_SR_POSPosition(posPosition);
}

void Service::vOn_SR_SEN_Temperature(const QString& reply)
{
    SENTemperatureSignalResponse temper(reply);
    emit SIG_SR_SENTemperature(temper);
}

void Service::vON_SR_Inst_Speed_Value(const QString& reply)
{
    qDebug() << reply;
    InstSpeedValueSignalResponse speedo(reply);
    emit SIG_SR_InstSpeedValue(speedo);
}

void Service::vOn_PR_SEN_GPSInfo(const QString& reply)
{
    SENGPSInfoProperty propSenGPSInfo(reply);
    emit SIG_PR_SENGPSInfo(propSenGPSInfo);
}

void Service::vOn_PR_SEN_GyroSum(const QString& reply)
{
    SENGyroSumProperty gyroProp(reply);
    emit SIG_PR_SENGyroSum((gyroProp));
}

void Service::vOn_PR_POS_CalibrationLevel(const QString& reply)
{
    POSCalibrationLevelProperty calibProp(reply);
    emit SIG_PR_POSCalibrationLevel(calibProp);
}

void Service::vOn_PR_SEN_ReverseGear(const QString& reply)
{
    SENReverseGearProperty revGear(reply);
    emit SIG_PR_SENReverseGear(revGear);
}

void Service::vOn_PR_POS_Position(const QString& reply)
{
    POSPositionProperty posPosition(reply);
    emit SIG_PR_POSPosition(posPosition);
}

void Service::vOn_PR_SEN_Temperature(const QString& reply)
{
    SENTemperatureProperty temper(reply);
    emit SIG_PR_SENTemperature(temper);
}

void Service::vCreateMethodAndSlotMap()
{
    vAddToMethodSlotMap("MAP_Controls",
                        tMethodSlot(&Service::vOn_MR_MAP_Controls,
                                    SLOT(vOn_MR_MAP_Controls(const QString&))));

    vAddToMethodSlotMap(
        "Info_GetSpell",
        tMethodSlot(&Service::vOn_MR_Info_GetSpell,
                    SLOT(vOn_MR_Info_GetSpell(const QString&))));

    vAddToMethodSlotMap(
        "Info_GetAvailableInformation",
        tMethodSlot(&Service::vOn_MR_Info_GetAvailableInformation,
                    SLOT(vOn_MR_Info_GetAvailableInformation(const QString&))));

    vAddToMethodSlotMap("Info_GetList",
                        tMethodSlot(&Service::vOn_MR_Info_GetList,
                                    SLOT(vOn_MR_Info_GetList(const QString&))));

    vAddToMethodSlotMap(
        "JSON_GetProperties",
        tMethodSlot(&Service::vOn_MR_JSON_GetProperties,
                    SLOT(vOn_MR_JSON_GetProperties(const QString&))));

    // NDR
    vAddToMethodSlotMap(
        "JSON_enableSignalOutput",
        tMethodSlot(&Service::vOn_MR_JSON_EnableSignalOutput,
                    SLOT(vOn_MR_JSON_EnableSignalOutput(const QString&))));

    vAddToMethodSlotMap(
        "POS_resetCalibration",
        tMethodSlot(&Service::vOn_MR_POS_ResetCalibration,
                    SLOT(vOn_MR_POS_ResetCalibration(const QString&))));
}

void Service::vCreateSignalMap()
{
    vAddToSignalMap("POS_PositionInformation",
                    &Service::vOn_SR_POS_PositionInformation);
    vAddToSignalMap("RT_NextManeuverInfo",
                    &Service::vOn_SR_RT_NextManeuverInfo);
    vAddToSignalMap("ADA_PromptReady", &Service::vOn_SR_ADA_PromptReady);
    // NDR
    vAddToSignalMap("SEN_GPSInfo", &Service::vOn_SR_SEN_GPSInfo);
    vAddToSignalMap("SEN_GyroSum", &Service::vOn_SR_SEN_GyroSum);
    vAddToSignalMap("POS_CalibrationLevel",
                    &Service::vOn_SR_POS_CalibrationLevel);
    vAddToSignalMap("SEN_ReverseGear", &Service::vOn_SR_SEN_ReverseGear);
    vAddToSignalMap("POS_Position", &Service::vOn_SR_POS_Position);
    vAddToSignalMap("SEN_Temperature", &Service::vOn_SR_SEN_Temperature);

    //
    vAddToSignalMap("Inst_Speed_Value", &Service::vON_SR_Inst_Speed_Value);
}
