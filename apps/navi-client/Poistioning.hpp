#pragma once

#include "Common.hpp"

#include <QDialog>

namespace Ui
{
class Poistioning;
}

class Poistioning : public QDialog
{
    Q_OBJECT

public:
    explicit Poistioning(QWidget* parent = 0);
    ~Poistioning();

private slots:

    void onUISenGPSData(UISenGPSData&);
    void onUIPOSPosData(UIPosPositionData&);
    void onUIReverseGearData(UIReverseGear&);
    void onUICalibrationData(UICalibrationLevel&);
    void onUIGyrosumData(UIGyroSum&);
    void onUITemperatureData(UITemperature&);
    void onUIInstSpeedData(UIInstSpeed&);

    void finished();

    void on_pushButton_reset_calib_clicked();

    void on_checkBox_logspeed_stateChanged(int arg1);

private:
    Ui::Poistioning* ui;
};
