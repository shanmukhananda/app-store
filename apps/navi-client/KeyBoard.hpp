#pragma once

#include "Common.hpp"

#include <QDialog>

namespace Ui
{
class KeyBoard;
}

class KeyBoard : public QDialog
{
    Q_OBJECT

public:
    explicit KeyBoard(QWidget* parent = 0);
    ~KeyBoard();

private slots:

    void on_lineEdit_locationInput_cursorPositionChanged(int arg1, int arg2);

    void on_listView_LocationList_doubleClicked(const QModelIndex& index);

    void on_listView_SpellCharacters_doubleClicked(const QModelIndex& index);

private:
    Ui::KeyBoard* ui;
};
