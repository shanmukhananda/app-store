#pragma once

#include <assert.h>
#include <math.h>
#include <time.h>
#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "Common.hpp"

// todo: use EIS type instead of map
typedef std::map<std::string, std::string> tEISTable;

namespace Utility
{
const double PI = 3.14159265358979323846;
const double MILE_TO_KM = 1.609344; // 1 Mile = 1.609344 Kilometer
const double NM_TO_SM = 1.15078;  // 1 Nautical Mile = 1.15078 Statute Mile (or
                                  // simply Mile)
const double KM_TO_M = 1000.0;    // 1 Kilometer = 1000 meter
const double DEG_TO_NM = 60.0405; // 1 Nautical Mile = 1/60 of a degree // #Link
                                  // 1
const double DEC_TO_HEX = 11930464.711111;    // Hex LatLong = Decimal LatLong *
                                              // 11930464.711111
const double EARTH_RADIUS = 6371.01;          // Earth Radius in km
const double WGS84_Semi_Major = 6378137.0;    // [m]  #Link 2
const double WGS84_Semi_Minor = 6356752.3142; // [m]  #Link 2

struct Location
{
    double latitude;
    double longitude;

    Location() : latitude(0.0), longitude(0.0)
    {
    }

    Location(double latParam, double longParam)
        : latitude(latParam), longitude(longParam)
    {
    }
};

struct BoundingBox
{
    Location lowerLeft;
    Location upperRight;

    BoundingBox()
    {
    }

    BoundingBox(const Location& ll, const Location& ur)
        : lowerLeft(ll), upperRight(ur)
    {
    }
};

double decimalToHex(const double value);

double degreeToRadian(const double deg);

double radianToDegree(const double rad);

double kilometerToMeter(const double lengthInKm);

double degreeToNauticalMile(const double deg);

double nauticalMileToStatuteMile(const double nautialMile);

double mileToKiloMeter(const double mile);

double DistanceBetweenPlaces(const Location& PointA, const Location& PointB);

//#Link 10
double AngleBetweenTwoPoints(const Location& PointA, const Location& PointB);

bool bIsInsideBoundingBox(const BoundingBox& box, const Location& point);

bool bIsInsideCirle(const Location& center, const double radiusKm,
                    const Location& point);

double WGS84EarthRadius(const double lat);

BoundingBox GetBoundingBox(const Location& point, const double halfSideInKm);

double MOSTAngleToNormal(const double mostAngle);

double DirectionBasedOnHeading(const double direction, const double heading);

//#Link 9
std::string GetEightPointCompassDirection(const double angle);

QJsonObject QStringToJobject(const QString& jsonString);

template <typename T>
std::string NumberToString(T Number)
{
    std::ostringstream ss;
    ss << Number;
    return ss.str();
}

template <typename T>
T StringToNumber(const std::string& Text)
{
    std::istringstream ss(Text);
    T result;
    ss >> result;
    if (ss.fail())
    {
        result = 0;
        assert(0);
    }

    return result;
}

std::string GetDateTimeStamp();

} // namespace NaviUtility

//#Link 1:  http://www.mathworks.in/help/map/ref/deg2nm.html
//#Link 2:  http://home.online.no/~sigurdhu/WGS84_Eng.html
//#Link 3:  http://www.movable-type.co.uk/scripts/latlong.html
//#Link 4:  http://www.geodatasource.com/developers/c
//#Link 5:  http://andrew.hedges.name/experiments/haversine/
//#Link 6:  http://en.wikipedia.org/wiki/Earth_radius
//#Link 7:  http://poiplaza.com/
//#Link 8:  http://stackoverflow.com/questions/1875255/open-source-poi-database
//#Link 9:
// http://stackoverflow.com/questions/7490660/converting-wind-direction-in-angles-to-text-words
//#Link 10:
// http://gis.stackexchange.com/questions/29239/calculate-bearing-between-two-decimal-gps-coordinates
