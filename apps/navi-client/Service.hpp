#pragma once

#include "Common.hpp"

#include <QtDBus/QDBusConnection>
/**
 * @brief The "Service" class
 * Responsible for handling connection with remote target using dbus over TCP.
 * Provides interfaces to invoke OpenNav APIs.
 * Provides both synchronous and asynchronous ways of calling dbus methods.
 * Declares & defines method response and signal handlers.
 * Registers for all signals under a specific dbus service instead of individual
 * signals.
 * Parses method and signals responses and emits appropriate signals.
 * LIMITATIONS: Currently if the same siganl name, method name is exposed by
 * two different service, then program will result in undefined bheaviour.
 * Changes are required to handle this case.
 * MR = Method Response
 * SR = Signal Response
 * PR = Property Response
 */

class Service : public QObject
{
    Q_OBJECT

    typedef void (Service::*tSignalHandler)(const QString&);
    typedef void (Service::*tMethodHandler)(const QString&);
    typedef const char* tSlot;

public:
    static Service* Instance();

    void vConnectToDbus();

    void vInvoke(const QString& sService, const QString& sHighLevelAPI,
                 const QString& sParameter, bool bIsSynchronous);

signals:
    // Navi Methods
    void SIG_MR_MapControls(const MapControlsMethodResult&);
    void SIG_MR_InfoGetList(const InfoGetListMethodResult&);
    void SIG_MR_MapSetWorldElementProperties(
        const MapSetWorldElementPropertiesMethodResult&);
    void SIG_MR_JsonGetProperties(const JsonGetPropertiesMethodResult&);
    // Navi Signals
    void
    SIG_SR_PosPositionInformation(const PosPositionInformationSignalResponse&);

    // NDR Signals
    void SIG_SR_SENGPSInfo(const SENGPSInfoSignalResponse&);
    void SIG_SR_SENGyroSum(const SENGyroSumSignalResponse&);
    void SIG_SR_POSCalibrationLevel(const POSCalibrationLevelSignalResponse&);
    void SIG_SR_SENReverseGear(const SENReverseGearSignalResponse&);
    void SIG_SR_POSPosition(const POSPositionSignalResponse&);
    void SIG_SR_SENTemperature(const SENTemperatureSignalResponse&);

    // CAN HMI
    void SIG_SR_InstSpeedValue(const InstSpeedValueSignalResponse&);

    // NDR Properties
    void SIG_PR_SENGPSInfo(const SENGPSInfoProperty&);
    void SIG_PR_SENGyroSum(const SENGyroSumProperty&);
    void SIG_PR_POSCalibrationLevel(const POSCalibrationLevelProperty&);
    void SIG_PR_SENReverseGear(const SENReverseGearProperty&);
    void SIG_PR_POSPosition(const POSPositionProperty&);
    void SIG_PR_SENTemperature(const SENTemperatureProperty&);

private slots:
    // All signal handler
    void
    vOnSignalResponse(const QString& signalName, const QString& signalReply);

    // Specific signal handler
    // NAVI Signals
    void vOn_SR_POS_PositionInformation(const QString& reply);
    void vOn_SR_RT_NextManeuverInfo(const QString& reply);
    void vOn_SR_ADA_PromptReady(const QString& reply);

    // NDR Signals
    void vOn_SR_SEN_GPSInfo(const QString& reply);
    void vOn_SR_SEN_GyroSum(const QString& reply);
    void vOn_SR_POS_CalibrationLevel(const QString& reply);
    void vOn_SR_SEN_ReverseGear(const QString& reply);
    void vOn_SR_POS_Position(const QString& reply);
    void vOn_SR_SEN_Temperature(const QString& reply);

    // CAN HMI
    void vON_SR_Inst_Speed_Value(const QString& reply);

    // NDR Properties
    void vOn_PR_SEN_GPSInfo(const QString& reply);
    void vOn_PR_SEN_GyroSum(const QString& reply);
    void vOn_PR_POS_CalibrationLevel(const QString& reply);
    void vOn_PR_SEN_ReverseGear(const QString& reply);
    void vOn_PR_POS_Position(const QString& reply);
    void vOn_PR_SEN_Temperature(const QString& reply);

    // Specific Method Response handler
    // NAVI
    void vOn_MR_MAP_Controls(const QString& reply);
    void vOn_MR_Info_GetSpell(const QString& reply);
    void vOn_MR_Info_GetAvailableInformation(const QString& reply);
    void vOn_MR_Info_GetList(const QString& reply);
    void vOn_MR_MAP_SetWorldElementProperties(const QString& reply);
    void vOn_MR_JSON_GetProperties(const QString& reply);

    // NDR
    void vOn_MR_JSON_EnableSignalOutput(const QString& reply);
    void vOn_MR_POS_ResetCalibration(const QString& reply);

    // All dbus error handler
    // todo: check the prototype
    void vOnError(const QString& reply);

private:
    // Slots are required because currently QDBusConnecton::callWithCallback
    // does not support normal function pointers
    struct tMethodSlot
    {
        tMethodHandler methodHandler;
        tSlot slotFunc;
        tMethodSlot(tMethodHandler methodHandlerP, tSlot slotFuncP)
            : methodHandler(methodHandlerP), slotFunc(slotFuncP)
        {
        }
    };

    explicit Service(QObject* parent = 0);

    Service(const Service&);

    Service& operator=(const Service&);

    void vRegisterForSingals();

    QDBusMessage oGetDbusMessage(const QString& service, const QString& sAPI,
                                 const QString& sParam);

    QDBusMessage
    oGetNaviDbusMessage(const QString& sAPI, const QString& sParam);

    QDBusMessage oGetNDRDbusMessage(const QString& sAPI, const QString& sParam);

    void vCreateMethodAndSlotMap();

    void vCreateSignalMap();

    void vCreatePropertyMap();

    void vAddToMethodSlotMap(std::string method, tMethodSlot methodSlot);

    void vAddToSignalMap(std::string signal, tSignalHandler slotFunc);

    void vSync_SendMethod(const QString& service, const QString& method,
                          const QString& sParam, tMethodHandler syncCallback);

    void vAsync_SendMethod(const QString& service, const QString& method,
                           const QString& sParam, tSlot slotFuncAsync);

    void
    vDispatchProperties(const JsonGetPropertiesMethodResult& jsonGetPropMR);

    static Service* m_pInstance;

    QDBusConnection m_bus;

    std::map<std::string, tMethodSlot> m_MethodSlotMap;

    std::map<std::string, tSignalHandler> m_SignalMap;
};
