#include "UILogicData.hpp"

// too many paramters, old code
UISenGPSData::UISenGPSData(int antStateP, int signalQualP, //!OCLINT
                           int headingP, int speedP, int fixP, int satsUsedP,
                           int satsVisiP, int northSpeedP, int eastSpeedP,
                           int verticalSpeedP, int hdopP, int pdopP, int vdopP,
                           int heightP, int horPosErrorP, int vertPosErrorp)
    : m_iAntennaState(antStateP)
    , m_iSignalQuality(signalQualP)
    , m_iHeading(headingP)
    , m_iSpeed(speedP)
    , m_iFix(fixP)
    , m_iSatsUsed(satsUsedP)
    , m_iSatsVisible(satsVisiP)
    , m_iNorthSpeed(northSpeedP)
    , m_iEastSpeed(eastSpeedP)
    , m_iVerticalSpeed(verticalSpeedP)
    , m_iHdop(hdopP)
    , m_iPdop(pdopP)
    , m_iVdop(vdopP)
    , m_iHeight(heightP)
    , m_iHorPosError(horPosErrorP)
    , m_iVertPosError(vertPosErrorp)
{
}

QString UISenGPSData::getAntennaStateString()
{
    QString retAntState = "--";
    switch (m_iAntennaState)
    {
        case 0:
        {
            retAntState = "UNKNOWN";
            break;
        }
        case 1:
        {
            retAntState = "OK";
            break;
        }
        case 2:
        {
            retAntState = "OPEN";
            break;
        }
        case 3:
        {
            retAntState = "SHORT";
            break;
        }
        case 4:
        {
            retAntState = "OFF";
            break;
        }
        default:
        {
            retAntState = "NONE";
            break;
        }
    }

    return retAntState;
}

QString UISenGPSData::getSignalQualityString()
{
    QString retSigQual = "--";

    switch (m_iSignalQuality)
    {
        case 0:
        {
            retSigQual = "UNKNOWN";
            break;
        }
        case 1:
        {
            retSigQual = "NULL";
            break;
        }
        case 2:
        {
            retSigQual = "BAD";
            break;
        }
        case 3:
        {
            retSigQual = "MINIMAL";
            break;
        }
        case 4:
        {
            retSigQual = "GOOD";
            break;
        }
        default:
        {
            retSigQual = "NONE";
            break;
        }
    }

    return retSigQual;
}

UIPosPositionData::UIPosPositionData(int speedP, int distanceP, int headingP,
                                     int latP, int longP)
    : m_iDRSpeed(speedP)
    , m_iDRDistance(distanceP)
    , m_iDRHeading(headingP)
    , m_iDRLatitude(latP)
    , m_iDRLongitude(longP)
{
}

UIReverseGear::UIReverseGear(int reverseP) : m_iReverseGear(reverseP)
{
}

UICalibrationLevel::UICalibrationLevel(int calibP) : m_iCalibrationLevel(calibP)
{
}

UIGyroSum::UIGyroSum(int gyroSumP) : m_iGyroSum(gyroSumP)
{
}

UITemperature::UITemperature(int temperP) : m_iTemperature(temperP)
{
}

UIInstSpeed::UIInstSpeed(int speedP) : m_speed(speedP)
{
}
