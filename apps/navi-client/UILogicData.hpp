#pragma once

#include <QString>

class UISenGPSData
{
public:
    UISenGPSData(int antStateP, int signalQualP, int headingP, int speedP,
                 int fixP, int satsUsedP, int satsVisiP, int northSpeedP,
                 int eastSpeedP, int verticalSpeedP, int hdopP, int pdopP,
                 int vdopP, int height, int horPosErrorP, int vertPosErrorP);

    int m_iAntennaState;
    int m_iSignalQuality;
    int m_iHeading;
    int m_iSpeed;
    int m_iFix;
    int m_iSatsUsed;
    int m_iSatsVisible;
    int m_iNorthSpeed;
    int m_iEastSpeed;
    int m_iVerticalSpeed;
    int m_iHdop;
    int m_iPdop;
    int m_iVdop;
    int m_iHeight;
    int m_iHorPosError;
    int m_iVertPosError;

    QString getAntennaStateString();
    QString getSignalQualityString();
};

class UIPosPositionData
{
public:
    UIPosPositionData(int speedP, int distanceP, int headingP, int latP,
                      int longP);

    int m_iDRSpeed;
    int m_iDRDistance;
    int m_iDRHeading;
    int m_iDRLatitude;
    int m_iDRLongitude;
};

class UIReverseGear
{
public:
    UIReverseGear(int reverseP);
    int m_iReverseGear;
};

class UICalibrationLevel
{
public:
    UICalibrationLevel(int calibP);
    int m_iCalibrationLevel;
};

class UIGyroSum
{
public:
    UIGyroSum(int gyroSumP);
    int m_iGyroSum;
};

class UITemperature
{
public:
    UITemperature(int temperP);
    int m_iTemperature;
};

class UIInstSpeed
{
public:
    UIInstSpeed(int speedP);
    int m_speed;
};
