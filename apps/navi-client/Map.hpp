#pragma once

#include "Common.hpp"

#include <QDialog>

namespace Ui
{
class Map;
}

class Map : public QDialog
{
    Q_OBJECT

public:
    explicit Map(QWidget* parent = 0);
    ~Map();

private slots:
    void on_comboBox_MapVisibility_currentIndexChanged(const QString& arg1);

    void on_comboBox_MapViewtype_currentIndexChanged(const QString& arg1);

    void on_comboBox_MapHeading_currentIndexChanged(const QString& arg1);

private:
    Ui::Map* ui;
};
