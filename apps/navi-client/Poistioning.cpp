#include "Poistioning.hpp"
#include "Common.hpp"

#include "ui_Poistioning.h"

#include <time.h>

Poistioning::Poistioning(QWidget* parent)
    : QDialog(parent), ui(new Ui::Poistioning)
{
    ui->setupUi(this);
    connect(this, SIGNAL(finished(int)), this, SLOT(finished()));
    connect(Logic::Instance(), SIGNAL(UISENGPSInfo(UISenGPSData&)), this,
            SLOT(onUISenGPSData(UISenGPSData&)));
    connect(Logic::Instance(), SIGNAL(UIPOSPositionInfo(UIPosPositionData&)),
            this, SLOT(onUIPOSPosData(UIPosPositionData&)));
    connect(Logic::Instance(), SIGNAL(UIReverseGearInfo(UIReverseGear&)), this,
            SLOT(onUIReverseGearData(UIReverseGear&)));
    connect(Logic::Instance(),
            SIGNAL(UICalibrationLevelInfo(UICalibrationLevel&)), this,
            SLOT(onUICalibrationData(UICalibrationLevel&)));
    connect(Logic::Instance(), SIGNAL(UIGyroSumInfo(UIGyroSum&)), this,
            SLOT(onUIGyrosumData(UIGyroSum&)));
    connect(Logic::Instance(), SIGNAL(UITemperatureInfo(UITemperature&)), this,
            SLOT(onUITemperatureData(UITemperature&)));
    connect(Logic::Instance(), SIGNAL(UIInstSpeedInfo(UIInstSpeed&)), this,
            SLOT(onUIInstSpeedData(UIInstSpeed&)));
}

Poistioning::~Poistioning()
{
    delete ui;
}

void Poistioning::onUISenGPSData(UISenGPSData& gpsSen)
{
    ui->lineEdit_antennaState->setText(gpsSen.getAntennaStateString());
    ui->lineEdit_gps_sigquality->setText(gpsSen.getSignalQualityString());
    ui->lineEdit_heading->setText(QString::number(gpsSen.m_iHeading));
    ui->lineEdit_gps_speed->setText(QString::number(gpsSen.m_iSpeed));
    ui->lineEdit_fix->setText(QString::number(gpsSen.m_iFix));
    ui->lineEdit_satsUsed->setText(QString::number(gpsSen.m_iSatsUsed));
    ui->lineEdit_satsVisible->setText(QString::number(gpsSen.m_iSatsVisible));
    /*
    northSpeed, east speed, verticalspeed
    hdop, pdop, vdop
    height, hosposerrror, vertposerrror
    */
    ui->lineEdit_gps_northSpeed->setText(QString::number(gpsSen.m_iNorthSpeed));
    ui->lineEdit_gps_eastSpeed->setText(QString::number(gpsSen.m_iEastSpeed));
    ui->lineEdit_gps_vertSpeed->setText(
        QString::number(gpsSen.m_iVerticalSpeed));
    ui->lineEdit_hdop->setText(QString::number(gpsSen.m_iHdop));
    ui->lineEdit_pdop->setText(QString::number(gpsSen.m_iPdop));
    ui->lineEdit_vdop->setText(QString::number(gpsSen.m_iVdop));
    ui->lineEdit_height->setText(QString::number(gpsSen.m_iHeight));
    ui->lineEdit_horPosError->setText(QString::number(gpsSen.m_iHorPosError));
    ui->lineEdit_vertPosError->setText(QString::number(gpsSen.m_iVertPosError));
}

void Poistioning::onUIPOSPosData(UIPosPositionData& posData)
{
    ui->lineEdit_dr_speed->setText(QString::number(posData.m_iDRSpeed));
    ui->lineEdit_dr_distance->setText(QString::number(posData.m_iDRDistance));
    ui->lineEdit_dr_heading->setText(QString::number(posData.m_iDRHeading));
    ui->lineEdit_dr_latitude->setText(QString::number(posData.m_iDRLatitude));
    ui->lineEdit_dr_longitude->setText(QString::number(posData.m_iDRLongitude));
    Logger::Instance()->DR_Speed(posData.m_iDRSpeed);
}

void Poistioning::onUIReverseGearData(UIReverseGear& rgear)
{
    ui->lineEdit_reversegear->setText(QString::number(rgear.m_iReverseGear));
}

void Poistioning::onUICalibrationData(UICalibrationLevel& calib)
{
    ui->lineEdit_calibLevel->setText(
        QString::number(calib.m_iCalibrationLevel));
}

void Poistioning::onUIGyrosumData(UIGyroSum& gyro)
{
    ui->lineEdit_gyrosum->setText(QString::number(gyro.m_iGyroSum));
}

void Poistioning::onUITemperatureData(UITemperature& temper)
{
    ui->lineEdit_temperature->setText(QString::number(temper.m_iTemperature));
}

void Poistioning::onUIInstSpeedData(UIInstSpeed& speedo)
{
    ui->lineEdit_speedometer->setText(QString::number(speedo.m_speed));
    Logger::Instance()->Inst_Speed(speedo.m_speed);
}

void Poistioning::finished()
{
    qDebug() << "finished";

    JsonEnableSignalOutputMethodCall ndrSignals;
    ndrSignals.vSetInSendSignals(false);
    ndrSignals.vSend(true);
}

void Poistioning::on_pushButton_reset_calib_clicked()
{
    POSResetCalibrationMethodCall resetCalib;
    resetCalib.vSend(true);
}

void Poistioning::on_checkBox_logspeed_stateChanged(int arg1)
{
    if (Qt::Unchecked == arg1)
    {
        Logger::Instance()->vSetLogState(false);
    }
    else if (Qt::Checked == arg1)
    {
        Logger::Instance()->vSetLogState(true);
    }
}
