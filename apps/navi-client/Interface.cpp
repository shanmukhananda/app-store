#include "Common.hpp"
//----------------------------------------------------------------------------//
//
//----------------------------------------------------------------------------//
void MapControlsMethodCall::vInsertToMapEIS(const std::string& tag,
                                            const std::string& value)
{
    m_aMapEIS.vAddTagValue(tag, value);
}

void MapControlsMethodCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    jobj["mapEIS"] = QString(m_aMapEIS.GetData().c_str());
    QString mapCtrlParam = QJsonDocument(jobj).toJson();
    Service::Instance()->vInvoke(NAVIGATION, "MAP_Controls", mapCtrlParam,
                                 bIsSynchronous);
}
//----------------------------------------------------------------------------//
MapControlsMethodResult::MapControlsMethodResult(const QString& methodResult)
{
    vDemarshall(methodResult);
}

EIS MapControlsMethodResult::outMapControls()
{
    return m_aOutMapControls;
}

eNavValidState MapControlsMethodResult::resultCode()
{
    return m_iResultCode;
}

void MapControlsMethodResult::vDemarshall(const QString& methodResult)
{
    QJsonObject jobj = Utility::QStringToJobject(methodResult);
    QString outMapEIS = jobj["outmapControls"].toString();
    m_aOutMapControls = outMapEIS.toStdString();
    m_iResultCode = static_cast<eNavValidState>(jobj["resultCode"].toInt());
}
//----------------------------------------------------------------------------//
void InfoGetListMethodCall::vInsertToInputEIS(const std::string& tag,
                                              const std::string& value)
{
    m_aInputEIS.vAddTagValue(tag, value);
}

void InfoGetListMethodCall::vSetCancelPreviousRequest(bool bCancelPrevReq)
{
    m_bCancelPreviousRequest = bCancelPrevReq;
}

void InfoGetListMethodCall::vSetInResultCount(int iInResultCount)
{
    m_iInResultCount = iInResultCount;
}

void InfoGetListMethodCall::vSetInSortOption(int iInSortOption)
{
    m_iInSortOption = iInSortOption;
}

void InfoGetListMethodCall::vSetInStartIndex(int iInStartIndex)
{
    m_iInStartIndex = iInStartIndex;
}

void InfoGetListMethodCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    std::string str = m_aInputEIS.GetData();
    jobj["cancelPreviousRequest"] = m_bCancelPreviousRequest;
    jobj["inputEIS"] = QString(str.c_str());
    jobj["inresultCount"] = m_iInResultCount;
    jobj["insortOption"] = m_iInSortOption;
    jobj["instartIndex"] = m_iInStartIndex;

    QString infoGetListParam = QJsonDocument(jobj).toJson();
    Service::Instance()->vInvoke(NAVIGATION, "Info_GetList", infoGetListParam,
                                 bIsSynchronous);
}

InfoGetListMethodCall::InfoGetListMethodCall()
    : m_bCancelPreviousRequest(false)
    , m_iInResultCount(0)
    , m_iInSortOption(0)
    , m_iInStartIndex(1)
{
}
//----------------------------------------------------------------------------//
InfoGetListMethodResult::InfoGetListMethodResult(const QString& methodResult)
{
    vDemarshall(methodResult);
}

bool InfoGetListMethodResult::complete()
{
    return m_bComplete;
}

const std::vector<EIS>& InfoGetListMethodResult::itemList()
{
    return m_aItemList;
}

const std::vector<std::string>& InfoGetListMethodResult::outfieldOptions()
{
    return m_aOutfieldOptions;
}

EIS InfoGetListMethodResult::outputEIS()
{
    return m_aOutputEIS;
}

int InfoGetListMethodResult::outresultCount()
{
    return m_iOutResultCount;
}

int InfoGetListMethodResult::outsortOption()
{
    return m_eOutSortOption;
}

int InfoGetListMethodResult::outstartIndex()
{
    return m_iOutStartIndex;
}

EIS InfoGetListMethodResult::respEIS()
{
    return m_aRespEIS;
}

eNavValidState InfoGetListMethodResult::resultCode()
{
    return m_iResultCode;
}

void InfoGetListMethodResult::vDemarshall(const QString& methodResult)
{
    QJsonObject jobj = Utility::QStringToJobject(methodResult);

    m_bComplete = jobj["complete"].toBool();
    m_iOutResultCount = jobj["outresultCount"].toInt();
    m_eOutSortOption = static_cast<eSortOption>(jobj["outsortOption"].toInt());
    m_iOutStartIndex = jobj["outstartIndex"].toInt();
    m_iResultCode = static_cast<eNavValidState>(jobj["resultCode"].toInt());

    QString outEIS = jobj["outputEIS"].toString();
    m_aOutputEIS = outEIS.toStdString();

    QString resEIS = jobj["respEIS"].toString();
    m_aRespEIS = resEIS.toStdString();

    const QJsonArray& itemListTemp = jobj["itemList"].toArray();

    foreach (const QJsonValue& value, itemListTemp)
    {
        QString item = value.toString();
        EIS eisdata = item.toStdString();
        m_aItemList.push_back(eisdata);
    }

    const QJsonArray& fieldOptions = jobj["outfieldOptions"].toArray();

    foreach (const QJsonValue& value, fieldOptions)
    {
        QString item = value.toString();
        m_aOutfieldOptions.push_back(item.toStdString());
    }
}
//----------------------------------------------------------------------------//
void MapSetWorldElementPropertiesMethodCall::vInsertToMapEIS(
    const std::string& tag, const std::string& value)
{
    m_aMapEIS.vAddTagValue(tag, value);
}

void MapSetWorldElementPropertiesMethodCall::vInsertToItemList(EIS eisTable)
{
    m_aItemList.push_back(eisTable);
}

void MapSetWorldElementPropertiesMethodCall::vSetWeFunction(int weFunc)
{
    m_eWeFunction = weFunc;
}

void MapSetWorldElementPropertiesMethodCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    jobj["mapEIS"] = QString(m_aMapEIS.GetData().c_str());
    jobj["weFunction"] = m_eWeFunction;

    QJsonArray itemList;

    for (std::vector<EIS>::iterator itr = m_aItemList.begin();
         itr != m_aItemList.end(); ++itr)
    {
        EIS eisData = *itr;
        QString item(eisData.GetData().c_str());
        itemList.append(item);
    }
    jobj["itemList"] = itemList;

    QString mapWorldParam = QJsonDocument(jobj).toJson();
    Service::Instance()->vInvoke(NAVIGATION, "MAP_SetWorldElementProperties",
                                 mapWorldParam, bIsSynchronous);
}

MapSetWorldElementPropertiesMethodCall::
    MapSetWorldElementPropertiesMethodCall() //!OCLINT (long line)
    : m_eWeFunction(0)
{
}
//----------------------------------------------------------------------------//
MapSetWorldElementPropertiesMethodResult::MapSetWorldElementPropertiesMethodResult(
    const QString& methodResult) //!OCLINT (long line)
{
    vDemarshall(methodResult);
}

const std::vector<EIS>& MapSetWorldElementPropertiesMethodResult::itemList()
{
    return m_aItemList;
}

EIS MapSetWorldElementPropertiesMethodResult::mapEIS()
{
    return m_aMapEIS;
}

int MapSetWorldElementPropertiesMethodResult::weFunctions()
{
    return m_eWeFunctions;
}

eNavValidState MapSetWorldElementPropertiesMethodResult::resultCode()
{
    return m_eResultCode;
}

void MapSetWorldElementPropertiesMethodResult::vDemarshall(
    const QString& methodResult)
{
    QJsonObject jobj = Utility::QStringToJobject(methodResult);
    m_eWeFunctions = jobj["weFunctions"].toInt();
    m_eResultCode = static_cast<eNavValidState>(jobj["resultCode"].toInt());
    QString mapEISTemp = jobj["mapEIS"].toString();
    m_aMapEIS = mapEISTemp.toStdString();
    const QJsonArray& itemListTemp = jobj["itemList"].toArray();

    foreach (const QJsonValue& value, itemListTemp)
    {
        QString item = value.toString();
        EIS eisdata = item.toStdString();
        m_aItemList.push_back(eisdata);
    }
}
//----------------------------------------------------------------------------//
void InfoGetDetailsMethodsCall::vInsertToInputEIS(const std::string& tag,
                                                  const std::string& value)
{
    m_aInputEIS.vAddTagValue(tag, value);
}

void InfoGetDetailsMethodsCall::vInsertToRequestedFields(
    const std::string& value)
{
    m_aRequestedFields.push_back(value);
}

void InfoGetDetailsMethodsCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    jobj["inputEIS"] = QString(m_aInputEIS.GetData().c_str());

    QJsonArray requestField;
    for (std::vector<std::string>::iterator itr = m_aRequestedFields.begin();
         itr != m_aRequestedFields.end(); ++itr)
    {
        QString request((*itr).c_str());
        requestField.append(request);
    }
    jobj["requestedFields"] = requestField;

    QString infoGetListParam = QJsonDocument(jobj).toJson();
    Service::Instance()->vInvoke(NAVIGATION, "Info_GetDetails",
                                 infoGetListParam, bIsSynchronous);
}
//----------------------------------------------------------------------------//
InfoGetDetailsMethodsResult::InfoGetDetailsMethodsResult(
    const QString& methodResult)
{
    vDemarshall(methodResult);
}

EIS InfoGetDetailsMethodsResult::outputEIS()
{
    return m_aOutputEIS;
}

EIS InfoGetDetailsMethodsResult::respEIS()
{
    return m_aRespEIS;
}

const std::vector<std::string> InfoGetDetailsMethodsResult::respFields()
{
    return m_aRespFields;
}

int InfoGetDetailsMethodsResult::resultCode()
{
    return m_iResultCode;
}

void InfoGetDetailsMethodsResult::vDemarshall(const QString& methodResult)
{
    QJsonObject jobj = Utility::QStringToJobject(methodResult);
    QString oeis = jobj["outputEIS"].toString();
    m_aOutputEIS = oeis.toStdString();

    QString resEis = jobj["respEIS"].toString();
    m_aRespEIS = resEis.toStdString();

    m_iResultCode = jobj["resultCode"].toInt();
    const QJsonArray& fields = jobj["respFields"].toArray();

    foreach (const QJsonValue& value, fields)
    {
        QString res = value.toString();
        m_aRespFields.push_back(res.toStdString());
    }
}
//----------------------------------------------------------------------------//
void InfoGetSpellMethodCall::vInsertToInputEIS(const std::string& tag,
                                               const std::string& value)
{
    m_aInputEIS.vAddTagValue(tag, value);
}

void InfoGetSpellMethodCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    jobj["inputEIS"] = QString(m_aInputEIS.GetData().c_str());

    QString infoGetSpellParam = QJsonDocument(jobj).toJson();

    Service::Instance()->vInvoke(NAVIGATION, "Info_GetSpell", infoGetSpellParam,
                                 bIsSynchronous);
}
//----------------------------------------------------------------------------//
PosPositionInformationSignalResponse::PosPositionInformationSignalResponse(
    const QString& signalResponse) //!OCLINT (long line)
{
    vDemarshall(signalResponse);
}

EIS PosPositionInformationSignalResponse::posInfo()
{
    return m_aPosInfo;
}

void PosPositionInformationSignalResponse::vDemarshall(
    const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    QString posInfoEIS = jobj["posInfo"].toString();
    m_aPosInfo = posInfoEIS.toStdString();
}
//----------------------------------------------------------------------------//
JsonGetPropertiesMethodCall::JsonGetPropertiesMethodCall(const QString& service)
{
    m_service = service;
}

void JsonGetPropertiesMethodCall::vAddToInputProperty(const std::string& inprop)
{
    m_aInputProperties.push_back(inprop);
}

void JsonGetPropertiesMethodCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    QJsonArray inputProperties;
    for (std::vector<std::string>::iterator iter = m_aInputProperties.begin();
         iter != m_aInputProperties.end(); ++iter)
    {
        std::string eachItem = *iter;
        QString item(eachItem.c_str());
        inputProperties.append(item);
    }
    jobj["inprop"] = inputProperties;

    QString jsonGetPropParam = QJsonDocument(jobj).toJson();

    const QString METHOD_NAME = "JSON_GetProperties";
    if (NDR == m_service)
    {
        Service::Instance()->vInvoke(NDR, METHOD_NAME, jsonGetPropParam,
                                     bIsSynchronous);
    }
    else if (NAVIGATION == m_service)
    {
        Service::Instance()->vInvoke(NAVIGATION, METHOD_NAME, jsonGetPropParam,
                                     bIsSynchronous);
    }
    else
    {
        qCritical("%s(%d):ERROR Service:'%s' is currently not supported",
                  __FILE__, __LINE__, m_service.toStdString().c_str());
    }
}
//----------------------------------------------------------------------------//
JsonGetPropertiesMethodResult::JsonGetPropertiesMethodResult(
    const QString& methodResult)
{
    vDemarshall(methodResult);
}

QJsonObject JsonGetPropertiesMethodResult::outprop() const
{
    return m_jOutprop;
}

void JsonGetPropertiesMethodResult::vDemarshall(const QString& methodResult)
{
    QJsonObject jobj = Utility::QStringToJobject(methodResult);
    m_jOutprop = jobj["outprop"].toObject();
}
//----------------------------------------------------------------------------//
JsonEnableSignalOutputMethodCall::JsonEnableSignalOutputMethodCall()
    : m_bInSendSignals(false)
{
}

void JsonEnableSignalOutputMethodCall::vSetInSendSignals(bool value)
{
    m_bInSendSignals = value;
}

void JsonEnableSignalOutputMethodCall::vSend(bool bIsSynchronous)
{
    QJsonObject jobj;
    jobj["inSendSignals"] = m_bInSendSignals;

    QString jsonEnableSigParam = QJsonDocument(jobj).toJson();
    Service::Instance()->vInvoke(NDR, "JSON_enableSignalOutput",
                                 jsonEnableSigParam, bIsSynchronous);
}
//----------------------------------------------------------------------------//
JsonEnableSignalOutputMethodResult::JsonEnableSignalOutputMethodResult(
    const QString& methodResult)
{
    vDemarshall(methodResult);
}

QString JsonEnableSignalOutputMethodResult::outprop()
{
    return m_outProp;
}

void JsonEnableSignalOutputMethodResult::vDemarshall(const QString& methodResult)
{
    QJsonObject jobj = Utility::QStringToJobject(methodResult);
    m_outProp = jobj["outprop"].toString();
}
//----------------------------------------------------------------------------//
SENGyroSumSignalResponse::SENGyroSumSignalResponse(const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int SENGyroSumSignalResponse::gyroSum() const
{
    return m_iGyroSum;
}

void SENGyroSumSignalResponse::vDemarshall(const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    m_iGyroSum = jobj["gyroSum"].toInt();
}
//----------------------------------------------------------------------------//
POSCalibrationLevelSignalResponse::POSCalibrationLevelSignalResponse(
    const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int POSCalibrationLevelSignalResponse::level() const
{
    return m_iLevel;
}

void POSCalibrationLevelSignalResponse::vDemarshall(
    const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    m_iLevel = jobj["level"].toInt();
}
//----------------------------------------------------------------------------//
SENReverseGearSignalResponse::SENReverseGearSignalResponse(
    const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int SENReverseGearSignalResponse::reverseValue() const
{
    return m_iReverseValue;
}

void SENReverseGearSignalResponse::vDemarshall(const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    m_iReverseValue = jobj["reverseValue"].toInt();
}
//----------------------------------------------------------------------------//
void POSResetCalibrationMethodCall::vSend(bool bIsSynchronous)
{
    Service::Instance()->vInvoke(NDR, "POS_resetCalibration", "{}",
                                 bIsSynchronous);
}
//----------------------------------------------------------------------------//
SENTemperatureSignalResponse::SENTemperatureSignalResponse(
    const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int SENTemperatureSignalResponse::temperature() const
{
    return m_iTemperature;
}

void SENTemperatureSignalResponse::vDemarshall(const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    m_iTemperature = jobj["temperature"].toInt();
}
//----------------------------------------------------------------------------//
POSPositionSignalResponse::POSPositionSignalResponse(
    const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int POSPositionSignalResponse::DR_distance() const
{
    return m_iDRDistance;
}

int POSPositionSignalResponse::DR_heading() const
{
    return m_iDRHeading;
}

int POSPositionSignalResponse::DR_latitude() const
{
    return m_iDRLatitude;
}

int POSPositionSignalResponse::DR_longitude() const
{
    return m_iDRLongitude;
}

int POSPositionSignalResponse::DR_speed() const
{
    return m_iDRSpeed;
}

void POSPositionSignalResponse::vDemarshall(const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    m_iDRDistance = jobj["DR_distance"].toInt();
    m_iDRHeading = jobj["DR_heading"].toInt();
    m_iDRLatitude = jobj["DR_latitude"].toInt();
    m_iDRLongitude = jobj["DR_longitude"].toInt();
    m_iDRSpeed = jobj["DR_speed"].toInt();
}
//----------------------------------------------------------------------------//
SENGPSInfoSignalResponse::SENGPSInfoSignalResponse(const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int SENGPSInfoSignalResponse::antennaState() const
{
    return m_iAntennaState;
}

int SENGPSInfoSignalResponse::day() const
{
    return m_iDay;
}

int SENGPSInfoSignalResponse::eastSpeed() const
{
    return m_iEastSpeed;
}

int SENGPSInfoSignalResponse::fix() const
{
    return m_iFix;
}

int SENGPSInfoSignalResponse::hdop() const
{
    return m_iHdop;
}

int SENGPSInfoSignalResponse::heading() const
{
    return m_iHeading;
}

int SENGPSInfoSignalResponse::height() const
{
    return m_iHeight;
}

int SENGPSInfoSignalResponse::horPosError() const
{
    return m_iHorPosError;
}

int SENGPSInfoSignalResponse::hrs() const
{
    return m_iHrs;
}

int SENGPSInfoSignalResponse::latitude() const
{
    return m_iLatitude;
}

int SENGPSInfoSignalResponse::longitude() const
{
    return m_iLongitude;
}

int SENGPSInfoSignalResponse::min() const
{
    return m_iMin;
}

int SENGPSInfoSignalResponse::month() const
{
    return m_iMonth;
}

int SENGPSInfoSignalResponse::northSpeed() const
{
    return m_iNorthSpeed;
}

int SENGPSInfoSignalResponse::pdop() const
{
    return m_iPdop;
}

int SENGPSInfoSignalResponse::satsUsed() const
{
    return m_iSatsUsed;
}

int SENGPSInfoSignalResponse::satsVisible() const
{
    return m_iSatsVisible;
}

int SENGPSInfoSignalResponse::sec() const
{
    return m_iSec;
}

int SENGPSInfoSignalResponse::signalQuality() const
{
    return m_iSignalQuality;
}

int SENGPSInfoSignalResponse::speed() const
{
    return m_iSpeed;
}

int SENGPSInfoSignalResponse::vdop() const
{
    return m_iVdop;
}

int SENGPSInfoSignalResponse::vertPosError() const
{
    return m_iVertPosError;
}

int SENGPSInfoSignalResponse::vertSpeed() const
{
    return m_iVertSpeed;
}

int SENGPSInfoSignalResponse::year() const
{
    return m_iYear;
}

void SENGPSInfoSignalResponse::vDemarshall(const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);

    m_iAntennaState = jobj["antennaState"].toInt();
    m_iDay = jobj["day"].toInt();
    m_iEastSpeed = jobj["eastSpeed"].toInt();
    m_iFix = jobj["fix"].toInt();
    m_iHdop = jobj["hdop"].toInt();
    m_iHeading = jobj["heading"].toInt();
    m_iHeight = jobj["height"].toInt();
    m_iHorPosError = jobj["horPosError"].toInt();
    m_iHrs = jobj["hrs"].toInt();
    m_iLatitude = jobj["latitude"].toInt();
    m_iLongitude = jobj["longitude"].toInt();
    m_iMin = jobj["min"].toInt();
    m_iMonth = jobj["month"].toInt();
    m_iNorthSpeed = jobj["northSpeed"].toInt();
    m_iPdop = jobj["pdop"].toInt();
    m_iSatsUsed = jobj["satsUsed"].toInt();
    m_iSatsVisible = jobj["satsVisible"].toInt();
    m_iSec = jobj["sec"].toInt();
    m_iSignalQuality = jobj["signalQuality"].toInt();
    m_iSpeed = jobj["speed"].toInt();
    m_iVdop = jobj["vdop"].toInt();
    m_iVertPosError = jobj["vertPosError"].toInt();
    m_iVertSpeed = jobj["vertSpeed"].toInt();
    m_iYear = jobj["year"].toInt();
}
//----------------------------------------------------------------------------//

SENGPSInfoProperty::SENGPSInfoProperty(const QString& property)
    : m_data(property)
{
}

const SENGPSInfoSignalResponse& SENGPSInfoProperty::GetData() const
{
    return m_data;
}

QString SENGPSInfoProperty::GetName() const
{
    return "SEN_GPSInfo";
}

SENGPSInfoProperty::~SENGPSInfoProperty()
{
}
//----------------------------------------------------------------------------//

POSPositionProperty::POSPositionProperty(const QString& property)
    : m_data(property)
{
}

const POSPositionSignalResponse& POSPositionProperty::GetData() const
{
    return m_data;
}

QString POSPositionProperty::GetName() const
{
    return "POS_Position";
}

POSPositionProperty::~POSPositionProperty()
{
}

//----------------------------------------------------------------------------//
POSCalibrationLevelProperty::POSCalibrationLevelProperty(const QString& property)
    : m_data(property)
{
}

const POSCalibrationLevelSignalResponse&
POSCalibrationLevelProperty::GetData() const
{
    return m_data;
}

QString POSCalibrationLevelProperty::GetName() const
{
    return "POS_CalibrationLevel";
}

POSCalibrationLevelProperty::~POSCalibrationLevelProperty()
{
}

//----------------------------------------------------------------------------//
SENReverseGearProperty::SENReverseGearProperty(const QString& property)
    : m_data(property)
{
}

const SENReverseGearSignalResponse& SENReverseGearProperty::GetData() const
{
    return m_data;
}

QString SENReverseGearProperty::GetName() const
{
    return "SEN_ReverseGear";
}

SENReverseGearProperty::~SENReverseGearProperty()
{
}
//----------------------------------------------------------------------------//
SENGyroSumProperty::SENGyroSumProperty(const QString& property)
    : m_data(property)
{
}

const SENGyroSumSignalResponse& SENGyroSumProperty::GetData() const
{
    return m_data;
}

QString SENGyroSumProperty::GetName() const
{
    return "SEN_GyroSum";
}

SENGyroSumProperty::~SENGyroSumProperty()
{
}
//----------------------------------------------------------------------------//

SENTemperatureProperty::SENTemperatureProperty(const QString& property)
    : m_data(property)
{
}

const SENTemperatureSignalResponse& SENTemperatureProperty::GetData() const
{
    return m_data;
}

QString SENTemperatureProperty::GetName() const
{
    return "SEN_Temperature";
}

SENTemperatureProperty::~SENTemperatureProperty()
{
}
//----------------------------------------------------------------------------//

InstSpeedValueSignalResponse::InstSpeedValueSignalResponse(
    const QString& signalResponse)
{
    vDemarshall(signalResponse);
}

int InstSpeedValueSignalResponse::speedValue() const
{
    return m_speedValue;
}

void InstSpeedValueSignalResponse::vDemarshall(const QString& signalResponse)
{
    QJsonObject jobj = Utility::QStringToJobject(signalResponse);
    m_speedValue = jobj["Value"].toInt();
}
