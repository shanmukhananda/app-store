#include "LocationInput.hpp"

#include "ui_LocationInput.h"

LocationInput::LocationInput(QWidget* parent)
    : QDialog(parent), ui(new Ui::LocationInput), m_keyboard(NULL)
{
    ui->setupUi(this);
}

LocationInput::~LocationInput()
{
    delete ui;
}

void LocationInput::on_pushButton_Country_clicked()
{
    if (NULL == m_keyboard)
    {
        m_keyboard = new KeyBoard(this);
    }

    Logic::Instance()->vHandleLIEnterCountry();
    m_keyboard->setWindowTitle("Enter Country");
    m_keyboard->show();
}
