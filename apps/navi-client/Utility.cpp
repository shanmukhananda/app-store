#include "Utility.hpp"

double Utility::decimalToHex(const double value)
{
    return value * DEC_TO_HEX;
}

double Utility::degreeToRadian(const double deg)
{
    return deg * PI / 180;
}

double Utility::radianToDegree(const double rad)
{
    return rad * 180 / PI;
}

double Utility::kilometerToMeter(const double lengthInKm)
{
    return lengthInKm * KM_TO_M;
}

double Utility::degreeToNauticalMile(const double deg)
{
    return deg * DEG_TO_NM;
}

double Utility::nauticalMileToStatuteMile(const double nautialMile)
{
    return nautialMile * NM_TO_SM;
}

double Utility::mileToKiloMeter(const double mile)
{
    return mile * MILE_TO_KM;
}

// Refer: Spherical Law of Cosines #Link 3
// #Link 4
//
// Online tool to test distance calculation
// #Link 5
// returns distance between two points in km
double
Utility::DistanceBetweenPlaces(const Location& PointA, const Location& PointB)
{
    double delta = 0;
    double dist = 0;

    delta = PointA.longitude - PointB.longitude;

    dist = sin(degreeToRadian(PointA.latitude)) *
               sin(degreeToRadian(PointB.latitude)) +
           cos(degreeToRadian(PointA.latitude)) *
               cos(degreeToRadian(PointB.latitude)) *
               cos(degreeToRadian(delta));

    dist = acos(dist);

    dist = radianToDegree(dist);            // convert to degree
    dist = degreeToNauticalMile(dist);      // convert to Nautical Mile
    dist = nauticalMileToStatuteMile(dist); // convert to Statute Mile
    dist = mileToKiloMeter(dist);           // convert to Kilometer
    // dist = kilometerToMeter(dist); // convert to meter

    assert(dist > 0);

    return dist;
}

bool Utility::bIsInsideBoundingBox(const BoundingBox& box_,
                                   const Location& point)
{
    return point.latitude > box_.lowerLeft.latitude &&
           point.latitude < box_.upperRight.latitude &&
           point.longitude > box_.lowerLeft.longitude &&
           point.longitude < box_.upperRight.longitude;
}

bool Utility::bIsInsideCirle(const Location& center, const double radiusKm,
                             const Location& point)
{
    return DistanceBetweenPlaces(center, point) < radiusKm;
}

// Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
double Utility::WGS84EarthRadius(const double lati)
{
    //#Link 6
    //(short variable from copied code)
    double An = WGS84_Semi_Major * WGS84_Semi_Major * //!OCLINT
                cos(lati);
    double Bn = WGS84_Semi_Minor * WGS84_Semi_Minor * //!OCLINT
                sin(lati);
    double Ad = WGS84_Semi_Major * cos(lati); //!OCLINT
    double Bd = WGS84_Semi_Minor * sin(lati); //!OCLINT
    return sqrt((An * An + Bn * Bn) / (Ad * Ad + Bd * Bd));
}

Utility::BoundingBox
Utility::GetBoundingBox(const Location& point, const double halfSideInKm)
{
    double latitude = degreeToRadian(point.latitude);
    double longitude = degreeToRadian(point.longitude);
    double halfSide = kilometerToMeter(halfSideInKm);

    // Radius of Earth at given latitude
    double radius = WGS84EarthRadius(latitude);
    // Radius of the parallel at given latitude
    double pradius = radius * cos(latitude);

    double latMin = latitude - halfSide / radius;
    double latMax = latitude + halfSide / radius;
    double lonMin = longitude - halfSide / pradius;
    double lonMax = longitude + halfSide / pradius;

    latMin = radianToDegree(latMin);
    latMax = radianToDegree(latMax);
    lonMin = radianToDegree(lonMin);
    lonMax = radianToDegree(lonMax);

    return Utility::BoundingBox(Location(latMin, lonMin),
                                Location(latMax, lonMax));
}

double
Utility::AngleBetweenTwoPoints(const Location& PointA, const Location& PointB)
{
    double startLat = degreeToRadian(PointA.latitude);
    double startLong = degreeToRadian(PointA.longitude);
    double endLat = degreeToRadian(PointB.latitude);
    double endLong = degreeToRadian(PointB.longitude);

    double deltaLong = endLong - startLong;

    double phi =
        log(tan(endLat / 2.0 + PI / 4.0) / tan(startLat / 2.0 + PI / 4.0));

    if (std::abs(deltaLong) > PI)
    {
        if (std::abs(deltaLong) > PI)
        {
            deltaLong = -(2.0 * PI - deltaLong);
        }
        else
        {
            deltaLong = (2.0 * PI + deltaLong);
        }
    }

    double InDeg = radianToDegree(atan2(deltaLong, phi));
    return fmod((InDeg + 360.0), 360.0);
}

/*
    *   0      -> 127
    *   63     -> 64
    *   127    -> 0
    *   191    -> 192
    *   255    -> 128
    *   MOST Directions: N = 0, W = 64, S = 128, E = 192
    */
double Utility::MOSTAngleToNormal(const double mostAngle)
{
    double normalHeading = (45.0 / 32.0) * mostAngle;
    normalHeading = 360 - normalHeading;

    return normalHeading;
}

// If the heading changes "direction" changes
double
Utility::DirectionBasedOnHeading(const double direction, const double heading)
{
    double dirBasedHeading = direction;

    dirBasedHeading = dirBasedHeading - heading;
    dirBasedHeading = dirBasedHeading + 360.0;
    dirBasedHeading = fmod(dirBasedHeading, 360.0);

    return dirBasedHeading;
}

std::string Utility::GetEightPointCompassDirection(const double angle)
{
    static std::vector<std::string> EightPointCompass;
    EightPointCompass.push_back("NORTH");
    EightPointCompass.push_back("NORTH-EAST");
    EightPointCompass.push_back("EAST");
    EightPointCompass.push_back("SOUTH-EAST");
    EightPointCompass.push_back("SOUTH");
    EightPointCompass.push_back("SOUTH-WEST");
    EightPointCompass.push_back("WEST");
    EightPointCompass.push_back("NORTH-WEST");

    double angleTemp = fmod(angle, 360.0); // Ensure angle is less than 360
    const int POINTS_REQUIRED = 8;
    double compassScaleFactor = 360.0 / POINTS_REQUIRED;

    double fIndex = floor(angleTemp / compassScaleFactor + 0.5);
    fIndex = fmod(fIndex, POINTS_REQUIRED);

    return EightPointCompass[static_cast<std::size_t>(fIndex)];
}

QJsonObject Utility::QStringToJobject(const QString& jsonString)
{
    QJsonParseError jerror;
    QJsonObject jobj;
    QJsonDocument jdoc =
        QJsonDocument::fromJson(jsonString.toStdString().c_str(), &jerror);

    if (QJsonParseError::NoError == jerror.error)
    {
        jobj = jdoc.object();
    }
    else
    {
        qCritical("ERROR in parsing JSON, ErrorName = %s Input String = %s",
                  jerror.errorString().toStdString().c_str(),
                  jsonString.toStdString().c_str());
    }

    return jobj;
}

std::string Utility::GetDateTimeStamp()
{
    const std::string seperator("_");
    time_t currentTime;
    struct tm* timeinfo;

    time(&currentTime);
    timeinfo = localtime(&currentTime);

    std::string timeStamp;
    timeStamp += NumberToString<int>(timeinfo->tm_mday) +
                 NumberToString<int>(timeinfo->tm_mon) +
                 NumberToString<int>(timeinfo->tm_year) + seperator +
                 NumberToString<int>(timeinfo->tm_hour) +
                 NumberToString<int>(timeinfo->tm_min) +
                 NumberToString<int>(timeinfo->tm_sec);

    return timeStamp;
}
