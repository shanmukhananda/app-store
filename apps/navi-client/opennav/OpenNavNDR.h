#ifndef OPENNAVNPL_OPENNAVNDR_H
#define OPENNAVNPL_OPENNAVNDR_H

/*******************************************************************************
 * Project         Harman Car Multimedia System
 * (c) copyright   2010
 * Company         Harman/Becker Automotive Systems
 *                 All rights reserved
 ******************************************************************************/
/**
 * @file           OpenNavNDR.h
 * @ingroup        opennavnpl
 * @author         RHaase
 * @brief          This file contains predefined data types for OpenNav.
 */

/*------------------------------------------------------------------------------
 *   INCLUDES
 *----------------------------------------------------------------------------*/
/// Header including OpenNav type definitions.
#include <opennav/OpenNavTypes.h>

struct GPS_All_hfs_t
{
    long timestamp;
    unsigned char state;
    /*
    typedef enum
    {
        SVI_IN_USE       = 0x01,   ///< SV used for navigation
        SVI_DIFFERENTIAL = 0x02,   ///< differential correction is available
        SVI_ORBIT_INFO   = 0x04,   ///< orbit informortion is available
    (ephemeris or almanach)
        SVI_EPHEMERESIS  = 0x08,   ///< orbit information is epemeresis
        SVI_UNHEALTHY    = 0x10,   ///< SV is unhealthy / shall not beused
        SVI_DUPOS        = 0x80    ///< don't use position information (azimuth
    and elevation)
    } SVInfoFlags;
    */
    unsigned char flags;
    /*
    /// antenna state
    typedef enum
    {
        ASTATE_UNKNOWN = 0,     ///< state is unknown
        ASTATE_OK,              ///< OK
        ASTATE_OPEN,            ///< annetenna is open or not exist
        ASTATE_SHORT_CIRCUIT,   ///< short circuit
        ASTATE_OFF              ///< antenna is off
    }  AntennaState;
    */
    unsigned char antennaState;
    /*
    typedef enum
    {
        GPSSIGQ_UNKNOWN,  ///< signal quality is unknown
        GPSSIGQ_NULL,     ///< no signal at all
        GPSSIGQ_BAD,      ///< signal is too bad, outside specification <
    45dbCN0
        GPSSIGQ_MINIMAL,  ///< signal has minimal needed quality 45-46dbCN0
        GPSSIGQ_GOOD      ///< signal has a good quality               >=47dbCN0
    } GpsSignalQuality;
    */
    unsigned char signalQuality;
    long latitude;
    long longitude;
    long height;
    unsigned long speed;
    unsigned short heading;
    unsigned short year;
    unsigned char month;
    unsigned char day;
    unsigned char hrs;
    unsigned char min;
    unsigned char sec;
    unsigned char fix;
    unsigned short hdop;
    unsigned short pdop;
    unsigned short vdop;
    unsigned short satsUsed;
    unsigned short satsVisible;
    unsigned short horPosError;
    unsigned short vertPosError;
    unsigned short northSpeed;
    unsigned short eastSpeed;
    unsigned short vertSpeed;
};

/**
    Some info flags for received satellite.
    Note: Note all info with each gps receiver are available
          Please use only SVI_IN_USE or SVI_DUPOS or you are a 'better-knower'.
*/
typedef enum {
    SVI_IN_USE = 0x01,       ///< SV used for navigation
    SVI_DIFFERENTIAL = 0x02, ///< differential correction is available
    SVI_ORBIT_INFO = 0x04,   ///< orbit informortion is available (ephemeris or
                             /// almanach)
    SVI_EPHEMERESIS = 0x08,  ///< orbit information is epemeresis
    SVI_UNHEALTHY = 0x10,    ///< SV is unhealthy / shall not beused
    SVI_DUPOS = 0x80 ///< don't use position information (azimuth and elevation)
} SVInfoFlags;

struct GPS_SatInfoEntry_hfs_t
{
    unsigned short infoFlags; // see SVInfoFlags
    unsigned char satNr;
    unsigned char sigNoise;
    unsigned short elevation;
    unsigned short azimuth;
    unsigned short residual;
};

struct GPS_SatInfo_hfs_t
{
    long timestamp;
    unsigned char state;
    unsigned char flags;
    GPS_SatInfoEntry_hfs_t satInfoEntry[16];
};

typedef struct
{
    int DR_latitude;         // latitude  in 1/100,000 of a degree
    int DR_longitude;        // longitude in 1/100,000 of a degree
    unsigned int DR_heading; // heading   in 1/100 of a degree, East 0, North 90
    unsigned int DR_speed;   // speed in kmph
    unsigned int DR_distance;  // Total distance travelled in meters
    int DR_deltaDist;          // Distance travelled since last DR pos in meters
    unsigned int DR_timestamp; // Dead reckoning position's time (internal
                               // resolution)
    int GPS_latitude;          // GPS latitude  in 1/100,000 of a degree
    int GPS_longitude;         // GPS longitude in 1/100,000 of a degree
    unsigned int GPS_heading;  // GPS heading   in 1/100 of a degree, North 0,
                               // East 90
    unsigned int GPS_state;    // GPS state, values defined in E_GPS_STATE
} dead_reckoning_t;

typedef enum {
    GPS_STATE_NO_FIX = 0,   // GPS state : No GPS fix available, no GPS signal
    GPS_STATE_TIME = 1,     // GPS state : 2D GPS fix available
    GPS_STATE_2D_FIX = 2,   // GPS state : 2D GPS fix available
    GPS_STATE_3D_FIX = 3,   // GPS state : 3D GPS fix available
    GPS_STATE_VALIDTIME = 4 // GPS state : No GPS fix available, tunnel etc
} E_GPS_STATE;

typedef struct
{
    int latitude;           // latitude  in 1/100,000 of a degree
    int longitude;          // longitude in 1/100,000 of a degree
    unsigned int heading;   // heading   in 1/100 of a degree, East 0, North 90
    float deltaHeading;     // change in heading since last DR position (in deg)
    unsigned int speed;     // speed in kmph
    unsigned int distance;  // Total distance travelled in meters
    int deltaDistance;      // Distance travelled since last DR pos in meters
    unsigned int sensorCnt; // Nr of sensor inputs used to generate DR pos
    unsigned int sequenceNr; // Sequence count of DR pos
    unsigned int timestamp;  // Dead reckoning position's time (internal
                             // resolution)
    float headingError;      // Heading error estimate in degrees
    float X_Error;  // Position sideways error estimate (or X) in meters
    float Y_Error;  // Position along car axis error estimate (or Y) in meters
    float turnRate; // Turn rate in degrees per second
    float inverseCurve; // Inverse circle radius in meters (= 1/radius)
    float lateralAccel; // Lateral acceleration in m/s�
    float acceleration; // Longitudinal (along car axis) acceleration in m/s�
    float pitchAngle;   // Pitch Angle
    unsigned int pitchAngleQual; // Pitch Angle Quality
} dead_reckoning_ext_t;

typedef struct
{
    unsigned int command;   // Dead reckoning command
    unsigned int timestamp; // Last DR position's time (internal resolution)
} dead_reckoning_command_t;

typedef enum {
    DR_CMD_RESET_KALMAN = 0, // DR command to reset the DR Kalman filter
    DR_CMD_MAP_MISSMATCH = 1 // DR command to indicate bad map miss-match
} E_DR_COMMAND;

typedef struct
{
    unsigned int operationMode; // Dead reckoning operation mode
    unsigned int timestamp;     // Last DR position's time (internal resolution)
} dead_reckoning_mode_t;

typedef enum {
    DR_MODE_NORMAL = 0,      // DR in normal mode (default) on road
    DR_MODE_FERRY_TRAIN = 1, // DR in ferry/car train mode (no wheel signal)
    DR_MODE_OFF_ROAD = 2,    // DR in off road mode, (map match not available)
    DR_MODE_PARKING = 3 // DR in underground parking model, (car is driving in
                        // underground parking lot).
} E_DR_MODE;

/// Enum to represent extra information about the map matching position.
typedef enum {
    /// The position is in a tunnel
    MAP_IN_TUNNEL = 1 << 0,
    /// The position is offroad
    MAP_IN_OFFROAD = 1 << 1,
    /// The position is on a ferry
    MAP_IN_FERRY = 1 << 2,
    /// The position is in a carpark
    MAP_IN_CARPARK = 1 << 3,

} E_MAP_FLAG;

typedef struct
{
    int MAP_latitude;         // Map Position latitude  in 1/100,000 of a degree
    int MAP_longitude;        // Map Position longitude in 1/100,000 of a degree
    unsigned int MAP_heading; // Map Position heading   in 1/100 of a degree,
                              // North 0, East 90
    unsigned int MAP_nrMatched; // The number of posible Map Match Positions
                                // found
    unsigned int DR_timestamp;  // Dead reckoning position's time (internal
                                // resolution)
    int DR_latitude;  // Dead reckoning latitude  in 1/100,000 of a degree
    int DR_longitude; // Dead reckoning longitude in 1/100,000 of a degree
    unsigned int DR_heading; // Dead reckoning heading   in 1/100 of a degree,
                             // North 0, East 90
    unsigned int MAP_flags;  // Bitmask, extra information about the current map
                             // matching position.
} map_position_t;

typedef enum {
    DR_direct = 0,   // Dead reckoning position for each new DR position
    DR_after_GPS = 1 // Dead reckoning Position after GPS received (max 1/sec)
} UpdateMode_t;

#endif // OPENNAVNDR_H
