#ifndef OPENNAV_OPENNAVTYPES_H
#define OPENNAV_OPENNAVTYPES_H

/*******************************************************************************
 * Project         Harman Car Multimedia System
 * (c) copyright   2008-2010
 * Company         Harman/Becker Automotive Systems GmbH
 *                 All rights reserved
 ******************************************************************************/
/**
 * @file           OpenNavTypes.h
 * @ingroup        opennav
 * @author         ROder, RHaase
 * @brief          This file contains predefined data types for OpenNav.
 */

#pragma pack(push, 4)

/*------------------------------------------------------------------------------
 *   INCLUDES
 *----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
 *   TYPEDEFS
 *----------------------------------------------------------------------------*/

/// 32 bit signed integer.
typedef int Int32;
/// 32 bit unsigned integer.
typedef unsigned int UInt32;

#if defined(WIN32)
/// 64 bit signed integer.
typedef signed __int64 Int64;
/// 64 bit unsigned integer.
typedef unsigned __int64 UInt64;
#else
/// 64 bit signed integer.
typedef signed long long int Int64;
/// 64 bit unsigned integer.
typedef unsigned long long int UInt64;
#endif

/// Type for a visual component ID.
typedef Int32 tVCID;

/*------------------------------------------------------------------------------
 *   ENUMS
 *----------------------------------------------------------------------------*/

/// Enum to represent available unit systems.
enum eUnitSystem
{
    undefined = 1 << 0,
    imperialyards = 1 << 1,
    imperialfeet = 1 << 2,
    metric = 1 << 3
};

/// Enum to represent available orientations.
enum eOrientation
{
    /// Type to identify north up orientation.
    northUp = 1 << 0,
    /// Type to identify heading up orientation.
    headingUp = 1 << 1,
    /// Type to identify destination up orientation.
    destinationUp = 1 << 2
};

/// Enum to represent available view types.
enum eViewType
{
    /// Type to identify a 2D view.
    view2D = 1 << 0,
    /// Type to identify a 3D view.
    view3D = 1 << 1,
    /// Type to identify a perspective view without height information.
    viewBird = 1 << 2,
    /// Type to identify a 3D perspective view that only displays the vehicle
    ///   models in a preview garage.
    viewGarage = 1 << 3,
    /// Type to identify route preview map
    viewPreview = 1 << 4,
};

/// Enum to represent available appearance style sets.
enum eAppearanceStyle
{
    /// Type to identify a day time colors.
    day = 1 << 0,
    /// Type to identify a night time colors.
    night = 1 << 1,
    /// Type to identify automatic settings.
    autoStyle = 1 << 2,
};

/// Enum to represent available route modes.
enum eRouteMode
{
    /// Type to identify route calculation in <i>fastest</i> mode.
    fastest = 1 << 0,
    /// Type to identify route calculation in <i>shortest</i> mode.
    shortest = 1 << 1,
    /// Type to identify route calculation with a requested <i>scenic route</i>.
    eScenicRoute = 1 << 2,
    /// Type to identify route calculation <i>avoiding motorways</i>.
    avoidMotorways = 1 << 3,
    /// Type to identify route calculation <i>avoiding ferries</i>.
    avoidFerries = 1 << 4,
    /// Type to identify route calculation <i>avoiding toll roads</i>.
    avoidTollroads = 1 << 5,
    /// Type to identify route calculation <i>avoiding IPD</i>.
    avoidIPD = 1 << 6,
    /// Type to identify route calculation <i>avoiding unpaved</i> streets.
    avoidUnpaved = 1 << 7,
    /// Type to identify route calculation <i>avoiding border</i> crossings.
    avoidBorder = 1 << 8,
    /// Type to identify route calculation with a requested <i>eco route<i>
    ecoRoute = 1 << 9,
    /// Type to identify route destinations shall be reordered to optimize
    ///   travel time. "Traveling salesman mode"
    optimizeDestOrder = 1 << 10,
    /// Type to identify route calculation <i>avoiding car trains</i>.
    avoidCarTrain = 1 << 11,
    /// Type to identify route calculation <i>avoiding time restricted</i>.
    avoidTimeRestricted = 1 << 12,
    /// Type to identify route calculation <i>avoiding seasonal restricted</i>.
    avoidSeasonalRestricted = 1 << 13,
    /// Type to identify route calculation <i>avoiding tunnels</i>.
    avoidTunnels = 1 << 14,
    /// Type to identify route destinations shall be reordered to optimize
    ///   travel time but do not modify order of the last destination.
    ///   "Traveling salesman mode"
    optimizeDestOrderKeepLastDest = 1 << 15,
    /// Ignore Traffic Patterns
    ignoreTrafficPatterns = 1 << 16,
    /// No IPD Guidance
    noIPDGuidance = 1 << 17,
    /// No Restricted Access
    avoidRestrictedAccess = 1 << 18,
    /// Type to identify route calculation with a requested <i>Sport route</i>
    /// characteristics.
    sportRoute = 1 << 19,
    /// Type to identify Trail guidance (off-road) guidance.
    trailGuidance = 1 << 20,
    /// Type to identify Trail guidance (off-road) guidance starting from the
    /// end of the route
    ///   and provide guidance backwards through the points.
    trailGuidanceFromEnd = 1 << 21,
    /// Type to identify route calculation should avoid car pool lanes.
    avoidCarPoolLanes = 1 << 22,
    /// Type to identify route calculation should prefer car pool lanes.
    preferCarPoolLanes = 1 << 23,
    /// Type to identify that the destination is on a closed road
    destOnClosedRoad = 1 << 24,
    /// Type to identify saved route to load.
    loadRoute = 1 << 25,
    /// Type to calculate the route faster using the precompiled route.
    hintUsePrecompiledRoute = 1 << 26,
    /// Type to identify route calculation should avoid 4WD roads.
    avoid4WD = 1 << 27,
    /// Type to indicate that alternative highway interface was chosen
    useAlternativeIC = 1 << 28,
    /// Type to indicate that using Past Traffic Information for route
    /// calculation was chosen
    usePastTrafficInfo = 1 << 29,
    /// Type to identify route calculation <i>avoiding small roads (road class
    /// 5-7)</i>.
    avoidSmallRoads = 1 << 30,
    /// Type to identify route calculation <i>avoiding urban areas</i>.
    avoidUrbanAreas = 1 << 31,
};

/// Enum to represent available route modes can be used in extended .
enum eRouteModeExtended
{
    /// Type to identify route calculation <i>avoiding bridges</i>.
    avoidBridges = 1 << 0,
    /// Type to indicate that truck info should be ignored
    ignoreTruckInfo = 1 << 1,
    /// Type to indicate that enable SmartIC on route calculation
    useSmartIC = 1 << 2,
};

/// Enum to represent available highway modes
enum eHighwayMode
{
    highwayModeNormal = 0,
    highwayModeRouteMonitor = 1
};

enum eHighwayBoundaryInterchangeType
{
    highwayEntranceInterchange = 1,
    highwayExitInterchange = 2
};

enum eHighwayInterchangeType
{
    interchangeTypeIC = 1,
    interchangeTypeSmartIC = 2,
    interchangeTypeJunction = 3
};

enum ePictogramTypes
{
    Airport = 1,
    Bus_station = 2,
    Fair = 3,
    Ferry = 4,
    FirstAid_post = 5,
    Harbour = 6,
    Hospital = 7,
    HotelMotel = 8,
    Industrial_Area = 9,
    Information_Centre = 10,
    Parking_Facility = 11,
    Petrol_Station = 12,
    Railway_station = 13,
    Rest_Area = 14,
    Restaurant = 15,
    Toilet = 16,
    Disabled_Toilet = 24,
    Shopping_Corner_Souvenir_shop = 25,
    Nap_Rest_Area = 26,
    Shower_Room = 27,
    Coin_Laundry = 28,
    Public_Bath = 29,
    FAX = 30,
    Post_Box = 31,
    ATM = 32,
    Snack_Corner = 33,
    Highway_Information_Terminal = 34,
    Highway_Oasis = 35,
    Chain_Store = 36,
    Dog_Run = 37
};

enum eAdditionalHighwayManeuverInfo
{
    ADDITIONAL_HIGHWAY_INFO_IC = 52,
    ADDITIONAL_HIGHWAY_INFO_PA = 54,
    ADDITIONAL_HIGHWAY_INFO_SA = 53,
    ADDITIONAL_HIGHWAY_INFO_JCT = 55,
    ADDITIONAL_HIGHWAY_INFO_SMART_IC = 56
};

/// Enum to represent available scroll modes.
enum eExtendedJunctionViewType
{
    eJvtUnknown = 0,
    eJvtIntersectionNotIllustration = 1,
    eJvtIntersectionIllustration = 2,
    eJvtHighwayEntrance = 3,
    eJvtHighwayJunction = 4,
    eJvtSAPA = 5
};

/// Enum to represent available scroll modes.
enum eScrollMode
{
    /// Type to identify the start of scrolling.
    start = 1 << 0,
    /// Type to identify the stop of scrolling.
    stop = 1 << 1,
    /// Type to identify the stop of scrolling, plus a jump back to the starting
    /// position.
    jumpBackToCarPosition = 1 << 2,
    /// Detach from car position.  The map will stop following the vehicle
    /// position.
    detachFromCarPosition = 1 << 3,
    /// Follow car position (without the jump back effect)
    followCarPosition = 1 << 4,
};

/// Used to specify if map items are visible.
enum eNavMapItemVisibility
{
    /// Signifies item(s) and sub items are not visible.
    NotVisible = 0,
    /// Signifies item(s) and sub items are fully visible.
    FullyVisible = 1,
    /// Signifies item(s) and sub items are partially visible.
    /// Meaning one or more sub categories are visible, but not all.
    PartiallyVisible = 2
};

/// Used to specify the Map lane recommendation mode
enum eMapLaneRecMode
{
    /// Signifies lane rec mode is off.
    laneRecModeOff = 0,
    /// Signifies lane rec mode is on.
    laneRecModeOn = 1,
    /// Signifies that if no signposts are shown, lane recommendations will be
    /// shown.
    showIfNoSignpost = 2
};

/// Enum to represent voice prompt.
enum ePrompts
{
    routeIsBeingCalculated = 1 << 0
};

/// Enum to represent nav guidance prompt style.
enum eNavGuidancePromptType
{
    // Enable street names to be spoken when phonemes are available for the
    // current country and language.  This style incorporates
    // TTS street names into the prompts when phonemes are available on a street
    // by street basis.
    // Street name phonemes provide the best speech quality.
    TTSStreetNamesForPhonemes = 1 << 0,

    // Enable street names to be spoken when haptic text is available for the
    // current country and the language are native.  This style incorporates
    // TTS street names into the prompts when native display text is available.
    // The expected quality of the TTS speech is lower than having phonemes,
    // but the quality is generally acceptable.
    TTSStreetNamesForNativeText = 1 << 1,

    // Enable street names to be spoken when haptic text is available even for
    // foreign languages.  This style incorporates TTS street names into the
    // prompts when only foreign display text is available. This is the worst
    // possible quality of TTS and is generally not recommended.
    TTSStreetNamesForForeignText = 1 << 2,

    // Enable natural guidance objects to be spoken.  This style incorporates
    // buildings, landmarks, etc. into the prompts when available.
    naturalPromptStyle = 1 << 3,

    // Enable beep only prompts. This will prevent all other guidance related
    // announcements.
    beepOnlyGuidanceNotification = 1 << 4,

    // Enable short prompts. If this style is active the guidance will be
    // shorter.
    shortGuidanceNotification = 1 << 5,

    // Enable VICS traffic alert guidance to be spoken. This style incorporates
    // VICS alert message with distance when data is defined in the VICS
    // messages.
    VICSTrafficinformationguidance = 1 << 6,

    // Enable Prefecture Border crossing guidance prompts. This prompt uses the
    // prefecture name
    // from current location by navi server.
    PrefectureBorderGuidance = 1 << 7,

    // Enable merging alert guidance prompts. This prompt uses the distance from
    // current position
    // to Merging point on the Motor way.
    MergingAlertGuidance = 1 << 8,

    // Enable Railroad Crossing alert. This prompt uses the distance from
    // current position to
    // Rail road crossing point.
    RailRoadCrossingAlertGuidance = 1 << 9,

    // Enable all prompts except the guidance related ones.
    noGuidancePrompts = 1 << 10,
};

/// Used to specify the style for street names on the map
enum eNavStreetNameType
{
    /// Street name style depend on map behave. At car follow mode streetBubble
    /// are used, when camera is detached from car streetSpline used
    streetAuto = 0,
    /// Street names are placed in bubbles with a small arrow touching the road.
    streetBubble = 1,
    /// Street names are placed into the map along the roads.
    streetSpline = 2,

};

/// Enum to represent available maneuver types
enum RouteGuideElementTypes
{
    NOSYMBOL = 0,
    NOINFO = 1,
    DIRECTIONTODESTINATION = 2,
    ARRIVED = 3,
    NEARDESTINATION = 4,
    ARRIVEDDESTINATIONOFFROAD = 5,
    OFFROAD = 6,
    OFFMAP = 7,
    NOROUTE = 8,
    CALCROUTE = 9,
    RECALCROUTE = 10,
    FOLLOWSTREET = 11,
    CHANGELANE = 12,
    TURN = 13,
    TURNONMAINROAD = 14,
    EXITRIGHT = 15,
    EXITLEFT = 16,
    SERVICEROADRIGHT = 17,
    SERVICEROADLEFT = 18,
    FORK2 = 19,
    FORK3 = 20,
    ROUNDABOUTTRSRIGHT = 21,
    ROUNDABOUTTRSLEFT = 22,
    SQUARETRSRIGHT = 23,
    SQUARETRSLEFT = 24,
    UTURN = 25,
    EXITROUNDABOUTTRSRIGHT = 26,
    EXITROUNDABOUTTRSLEFT = 27,
    PREPARETURN = 28,
    PREPAREROUNDABOUT = 29,
    PREPARESQUARE = 30,
    PREPAREUTURN = 31,
    EXITRIGHTRAMPUP = 32,
    EXITRIGHTRAMPDOWN = 33,
    TOLLBOOTH = 34,
    IPDAREA = 35,

    SIDESTREET = 129,
    PROHIBITEDSIDESTREET = 130
};

/// Enum to represent available update modes.
enum eUpdateMode
{
    /// Update disabled.
    disable = 0,
    /// Update once.
    once = 1 << 0,
    /// Update on change.
    onChange = 1 << 1,
    /// Defined frequency for updates.
    frequency = 1 << 2
};

/// Enum to represent available OpenNav binary states.
enum eBinaryState
{
    /// Type to identify <i>on</i> in a binary state.
    on = 1 << 0,
    /// Type to identify <i>off</i> in a binary state.
    off = 1 << 1
};

/// Enum to represent valid and invalid states.
enum eValidState
{
    /// Type to identify a <i>valid</i> state.
    valid = 1 << 0,
    /// Type to identify an <i>invalid</i> state.
    invalid = 1 << 1
};

/// Enum to represent location ambiguity.
enum eAmbiguityState
{
    /// Type to identify an <i>ambiguous</i> state.
    ambiguous = 1 << 0,
    /// Type to identify an <i>unambiguous</i> state.
    unambiguous = 1 << 1
};

/// Enum to represent sort options.
enum eSortOption
{
    /// Type to identify default sorting.
    defaultSort = 0,
    /// Type to identify ascending sorting.
    ascending = 1 << 0,
    /// Type to identify descending sorting.
    descending = 1 << 1,
    /// Type to identify nearest first sorting.
    nearFirst = 1 << 2,
    /// Type to identify farther first sorting.
    farFirst = 1 << 3,
    /// Type to identify higher hierarchy first sorting.
    hierarchicalSort = 1 << 4,
    /// Type to identify lower hierarchy first sorting.
    hierarchicalDescSort = 1 << 5,

    /// special poiData sorting for requestGetPickInfo (not used
    /// hierarchicalSort and hierarchicalDescSort)
    /// Type to icon priority ascending sorting.
    prioritySort = 1 << 6,
    /// Type to icon priority descending sorting.
    priorityDescSort = 1 << 7
};

/// Enum to represent the different world element types.
enum eWorldElement
{
    geoCoordinate = 1 << 0,
    locationDetails = 1 << 1,
    poiData = 1 << 2,
    tmcData = 1 << 3,
    weatherData = 1 << 4,
    speedCameraData = 1 << 5,
    vehicleModel = 1 << 6,
    routeSegments = 1 << 7,
    roadSegment = 1 << 8
};

/// Enum to represent the different world element operation.
enum eWorldElementFunction
{
    zoom = 1 << 0,
    highlight = 1 << 1,
    /// Used to set visibility of discrete world elements (a route handle,
    /// a TMC event, etc.)
    visible = 1 << 2,
    /// Used to set visibility for categories or types of items (POI Categories)
    setItemVisibilities = 1 << 3,
    /// Used to temporary items set or clear temporary POIs
    setTemporaryItems = 1 << 4,
    /// Used to isolated and center map display on the differences of two route
    /// handles
    zoomToRouteDifferences = 1 << 5,
    /// smooth use of autocamera on change views
    smooth = 1 << 6,
};

/// Enum for Route alternative reason.
enum eRouteAlternativeReason
{
    /// Route was generated based on an alternative maneuver
    alternativeRouteManeuver = 0,
    /// Route was generated based on user driving habits and traffic.
    userDrivingHabitsAndTraffic = 1,
};

/// Enum for route calculation states
enum eNavCalculationState
{
    /// Initial state. No planning or calculation has yet occurred.
    Idle = 0,
    /// Route is currently planning.
    Planning = 1,
    /// Route has been planned.
    Planned = 2,
    /// Route is calculating. It is possible that a route has already been
    /// calculated and is being re-calculated due to TMC or deviation.
    Calculating = 3,
    /// Route has been calculated. A route segment list is available.
    Calculated = 4,
    /// The route has been calculated and is currently the active route. A route
    /// segment list is available.
    Routing = 5,
    /// The route was calculated. The route was travelled. The travelled route
    /// segment is available.
    Routed = 6,
    /// Route calculation failed for the provided destinations.
    Failed = 7
};

/// Enum for route calculation error codes
enum eNavCalculationError
{
    MapsIsolated = -1,
    ProhibitedIsolated = -2,
    OutOfMemory = -3,
    LineGraphCorrupt = -4,
    SourceIsolated = -5,
    TargetIsolated = -6,
    RouteTooLong = -7,
    CreateWaypoint_NoRoadFound = -8,
    ViaPointUnreachable = -10,
    ViaPointUnreachableWithPreferences = -11,
    TargetUnreachable = -12,
    TargetUnreachable_Pedestrian = -13,
    TargetUnreachableWithPreferences = -14,
    TargetUnreachableWithPreferences_Unpaved = -15,
    TargetUnreachableWithPreferences_PermitNeeded = -16,
    TruckOnMinorRoad = -17,
    TruckUnreachableWithPreferences_Soft = -18,
    TruckUnreachableWithPreferences_Hard = -19,
    MapProviderChanged = -20,
    MissingNavigationLicence = -21,
    CantGetCoordinates = -22,
    TruckWarningsNearDestination = -101
};

/// Possible audio sources.
enum eNavAudioSource
{
    /// Default Nav audio source. No nav audio is currently playing.
    AudioIdle = 0,
    /// This is the navcore initiated audio source. IE. a turn recommendation
    /// triggered by the vehicle approaching the maneuver.
    AudioAutomatic = 1,
    /// This is the user initiated audio source. IE. user asks for a navigation
    /// prompt repeat.
    AudioManual = 2,
    /// This is the safety camera audio source. IE. beep while approaching
    /// safety camera
    AudioSafetyCamera = 3,
    /// DSRC interruption highest priority information [JEITA6004]
    AudioDSRCHighestPri = 4,
    /// ETC Guidance [Wave file]
    AudioETCGuidance = 5,
    /// DSRC interruption normal priority information [JEITA6004]
    AudioDSRCNormalPri = 6,
};

/// Enum of available languages.
enum eNavLanguage
{
    /// Undefined.
    LangUndef = 0,
    /// Danish.
    LangDansk = 1,
    /// German.
    LangGerman = 2,
    /// British English.
    LangEngGB = 3,
    /// US English.
    LangEngUS = 4,
    /// Spanish.
    LangSpanish = 5,
    /// US/NA Spanish.
    LangSpanishUS = 6,
    /// French.
    LangFrench = 7,
    /// Italian.
    LangItalian = 8,
    /// Nederland.
    LangNetherlands = 9,
    /// Portuguese.
    LangPortuguese = 10,
    /// Swedish.
    LangSwedish = 11,
    /// Turkish.
    LangTurkish = 12,
    /// Flemish.
    LangFlemish = 13,
    /// Arabic.
    LangArabic = 14,
    /// ChineseTraditional.
    LangChineseTraditional = 15,
    /// ChineseSimple.
    LangChineseSimple = 16,
    /// Korean.
    LangKorean = 17,
    /// Japanese.
    LangJapanese = 18,
    /// Brazilian Portuguese
    LangBrazilianPortuguese = 19,
    /// French Canadian
    LangFrenchCanadian = 20,
    /// Finnish
    LangFinnish = 21,
    /// Greek
    LangGreek = 22,
    /// Norwegian
    LangNorwegian = 23,
    /// Polish
    LangPolish = 24,
    /// Russian
    LangRussian = 25,
    /// Hindi
    LangHindi = 26,
    /// Czech
    LangCzech = 27,
    /// Hungarian
    LangHungarian = 28,
    /// Romanian
    LangRomanian = 29,
    /// Slovak
    LangSlovak = 30,
    /// Thai
    LangThai = 31,
    /// Ukranian
    LangUkranian = 32,
    /// Bulgarian
    LangBulgarian = 33,
    /// Australian English.
    LangEngAU = 34,
    /// Indian English.
    LangEngIN = 35,
};

/// Used to specify if segments should be added or removed.
enum eNavBlockFunction
{
    /// Signifies segments should be removed from consideration during route
    /// calculation.
    AddBlock = 0,
    /// Signifies segments should be added to consideration during route
    /// calculation.
    RemoveBlock = 1,
    /// Add avoid area
    AddAvoidArea = 2,
    /// Remove avoid area
    RemoveAvoidArea = 3,
};

/// Used to identify the type of the blocked element.
enum eBlockedElementType
{
    /// Signifies the element is a block by distance.
    Distance = 1,
    /// Signifies the route segment was blocked.
    RoadSegment = 2,
    /// Signifies the maneuver of the route segment was blocked.
    RoadSegmentManeuver = 3,
    /// avoid area
    AvoidArea = 4,
};

/// Enumerations for TMC Route Modes
enum eNavTMCRouteMode
{
    /// Signifies tmc messages will not be considered during route calculation
    tmcOff = 0,
    /// Signifies the tmc messages will be considered during route calculation,
    /// and the naviserver will accept the calculated route automatically.
    tmcAutomatic = 1,
    /// Signifies the tmc messages will be considered during route calculation,
    /// but the naviserver will not accept the calculated route automatically.
    /// The server creates the new route in the background, but user can accept
    /// or reject the calculated route.
    tmcManual = 2,
    /// Signifies the naviserver will only change the current route if the
    /// specified delay time is exceeded by the current route.
    tmcDelay = 4
};

/// Enumerations for TMC Tuning Modes
enum eNavTMCTuningMode
{
    /// Signifies the NaviServer will automatically tune to TMC stations.
    tmcTuningAutomatic = 1,
    /// Signifies the NaviServer will attempt to tune to the specified TMC
    /// station if possible.  If it is not available, other stations may be
    /// used.
    tmcTuningManual = 2,
};

/// Used to indicate the add or remove operation for TMC ignore and avoid
/// functions.
enum eNavTMCFunction
{
    /// Signifies events should be ignored or avoided depending upon requested
    /// function
    AddEvents = 0,
    /// Signifies events should no longer be ignored or avoided depending upon
    /// requested function
    RemoveEvents = 1
};

/// Enumerations for tmc info types.
enum eNavTMCInfo
{
    /// List of tmc messages on route
    tmcOnRoute = 1,
    /// List of significent tmc messages
    tmcSignificent = 2,
    /// List of non significent tmc messages
    tmcNonSignificent = 4,
    /// List of all tmc messages
    tmcAllMessages = 8,
    /// List of tmc avoided messages
    tmcAvoided = 16,
    /// List of tmc ignored messages
    tmcIgnoredMessages = 32,
};

/// Enumerations for tmc avoidances.
enum eNavTMCAvoidMode
{
    /// The navserver decides to avoid or ignore the TMC event.
    tmcAuto = 0,
    /// The navserver try to avoid the TMC event
    tmcAvoid = 1,
    /// The navserver ignores the TMC event.
    tmcIgnore = 2
};

/// Enumerations for IP Traffic update rate settings
enum eIPTrafficUpdateRate
{
    /// Off
    ipTrafficUpdateRateOff = 0,
    /// Manual
    ipTrafficUpdateRateManual = 1,
    /// Automatic update rate - low
    ipTrafficUpdateRateAutoLow = 2,
    /// Automatic update rate - high
    ipTrafficUpdateRateAutoHigh = 3
};

/// Enumerations for Data connection roaming settings
enum eDataConnectionRoaming
{
    /// Data connection is not in roaming mode
    dataRoamingOff = 0,
    /// Data connection is in roaming mode
    dataRoamingOn = 1
};

/// Enums for speed warning mode.
enum eNavSpeedWarningMode
{
    /// Off
    warningsOff = 0,
    /// Speed value warning mode.  uses a fixed km/h value.
    valueMode = 1,
};

/// Enums for map speed limit mode.  Used by <MSL> Map speed limit map control.
///   See OpenNav Map API Design for more details.
enum eMapSpeedLimitMode
{
    /// Off
    mapSpeedLimitOff = 0,
    /// Speed Limit will always be displayed
    mapSpeedLimitAlwaysOn = 1,
    /// Speed Limit will be displayed when the limit is exceeded
    mapSpeedLimitFollowWarningMode = 2,
};

/// Enums for safety camera warning mode.
enum eNavSafetyCameraMode
{
    /// all safety camera warnings Off
    safetyWarningsOff = 0,
    /// All Safety Camera warnings on. (both 3rd party and onboard are on)
    safetyWarningsOn = 1,
    /// Only Built In Safety Camera warnings on.
    safetyOnlyBuiltInWarningsOn = 2,
    /// Only 3rd party Safety Camera warnings on.
    safetyOnly3rdPartyWarningsOn = 3,
};

/// Enums for voice input mode bitfield.
enum eNavVoiceInputMode
{
    /// Full word input (SCL File).
    fullWordInput = 1 << 0,
    /// One Shot input (SCN File).
    oneShotInput = 1 << 1,
    /// Spelling input (SPB File).
    spellingInput = 1 << 2,
};

/// Enumerations for update result state
enum eUpdateResultState
{
    /// The update api completed with success.
    updSuccess = 0,
    /// The update package received is not valid for this device.
    updInvalidPackage = 1,
    /// The update package received is not activated.
    updActivationNeeded = 2,
    /// There is not enough space for the update package
    updNotEnoughSpace = 4,
    /// The update package is older than the current package on the device
    updOlderPackage = 8,
    /// No update package is found
    updNoPackage = 16,
    /// Already activated
    updAlreadyActivated = 32,
    /// The update package is the same as the current package on the device
    updSamePackage = 64
};

/// Enums for data connection type.
enum eNavDataConnectionType
{
    /// no connection
    noDataConnection = 0,
    /// direct connection, no proxy necessary.
    directDataConnection = 1,
    /// proxy is required to connect.
    proxyDataConnection = 2,
};

/// Enums for demo mode commands.
enum eNavDemoModeCommands
{
    /// demo mode pause
    demoPause = 0,
    /// demo mode play
    demoPlay = 1,
    /// demo mode skip to next route segment
    demoNextSegment = 2,
    /// demo mode skip to last route segment
    demoLastSegment = 3,
    demoPrevSegment = 3,
    /// demo mode skip to next route maneuver
    demoNextManeuver = 4,
    /// demo mode skip to last route maneuver
    demoLastManeuver = 5,
    demoPrevManeuver = 5,
    /// demo mode stop
    demoStop = 6
};

/// Enums for special destination attributes
enum eNavDestinationFlags
{
    /// Type to disable the announcement of which side of the street the
    /// destination is on.
    disableStreetSideAnnouncement = 1 << 0,
};

/// Enums for type of update package.
enum ePackageType
{
    /// unspecified data
    pkgTypeUnspecified = 0,
    /// voice data
    pkgTypeVoice = 1,
    /// phonemes for voice
    pkgTypeVoicePhoneme = 2,
    /// automatic speech recognition
    pkgTypeAsr = 3,
    /// map or general map related data
    pkgTypeMap = 4,
    /// truck info
    pkgTypeMapTruckInfo = 5,
    /// driver alert
    pkgTypeMapDriverAlert = 6,
    /// address point
    pkgTypeMapAddressPoint = 7,
    /// speed profile of roads
    pkgTypeMapSpeedProfile = 8,
    /// junction view
    pkgTypeMapJunctionView = 9,
    /// raster map
    pkgTypeMapRaster = 10,
    /// 4WD roads info
    pkgTypeMap4Wd = 11,
    /// extended postal code info (7PPC UK/6PPC NLD)
    pkgTypeMapExtendedPostalCode = 12,
    /// traffic lights
    pkgTypeMapTrafficLights = 13,
    /// point of interest
    pkgTypePoi = 14,
    /// speed cameras
    pkgTypeSpeedcam = 15,
    /// general GUI data
    pkgTypeGui = 16,
    /// language of GUI
    pkgTypeGuiLanguage = 17,
    /// sw tutorial
    pkgTypeGuiTutorial = 18,
    /// ui skin
    pkgTypeGuiSkin = 19,
    /// ui scheme
    pkgTypeGuiScheme = 20,
    /// general 3d data
    pkgType3d = 21,
    /// 3d elevation map
    pkgType3dDem = 22,
    /// landmarks
    pkgType3dLandmark = 23,
    /// city map
    pkgType3dBuilding = 24,
    /// car models
    pkgType3dCar = 25,
    /// TMC data
    pkgTypeTmc = 26,
    /// general routing related data
    pkgTypeRouting = 27,
    /// historical traffic information
    pkgTypeRoutingHistTraffic = 28,
    /// highway node routing (preprocessed road network)
    pkgTypeRoutingHnr = 29,
    /// dictionary data (abbreviations, similarities)
    pkgTypeDictionary = 30,
    /// NaviServer software
    pkgTypeNaviServer = 31,
    /// navigation related software component
    pkgTypeNaviSwComponent = 32,
    /// general navigation related data
    pkgTypeNaviData = 33,
    /// non navigation related data
    pkgTypeNonNavigationData = 34,
    /// OS image
    pkgTypeOsImage = 35
};

/// Enums for engine types.
enum eNavEngineType
{
    // Gasoline
    engGas = 1 << 0,
    // Diesel
    engDiesel = 1 << 1,
    // Gasoline hybrid
    engGasHybrid = 1 << 2,
    // Diesel hybrid
    engDieselHybrid = 1 << 3,
};

// Define lchange() action to take
enum eListChangeAction
{
    actionNop = 0,
    actionAdd = 1 << 0,
    actionRemove = 1 << 1,
    actionRename = 1 << 2
};

/// Enums for Japanese road types.
enum eJapanRoadType
{
    /// not map-matched, or off-road.
    jpnRoadTypeUnknown = 0,
    jpnRoadTypeHighway = 1,
    jpnRoadTypeMetropolitan = 2,
    jpnRoadTypePublic = 3,
    jpnRoadTypeOther = 4

};

/// Enums for Japanese Prefectures.
enum eJapanPrefecture
{
    /// Unknown Prefecture
    jpnPrefUnknown = 0,
    /// Hokkaido
    jpnPrefHokkaido = 1,
    /// Aomori
    jpnPrefAomori = 2,
    /// Iwate
    jpnPrefIwate = 3,
    /// Miyagi
    jpnPrefMiyagi = 4,
    /// Akita
    jpnPrefAkita = 5,
    /// Yamagata
    jpnPrefYamagata = 6,
    /// Fukushima
    jpnPrefFukushima = 7,
    /// Ibaraki
    jpnPrefIbaraki = 8,
    /// Tochigi
    jpnPrefTochigi = 9,
    /// Gunma
    jpnPrefGunma = 10,
    /// Saitama
    jpnPrefSaitama = 11,
    /// Chiba
    jpnPrefChiba = 12,
    /// Tokyo
    jpnPrefTokyo = 13,
    /// Kanagawa
    jpnPrefKanagawa = 14,
    /// Niigata
    jpnPrefNiigata = 15,
    /// Toyama
    jpnPrefToyama = 16,
    /// Ishikawa
    jpnPrefIshikawa = 17,
    /// Fukui
    jpnPrefFukui = 18,
    /// Yamanashi
    jpnPrefYamanashi = 19,
    /// Nagano
    jpnPrefNagano = 20,
    /// Gifu
    jpnPrefGifu = 21,
    /// Shizuoka
    jpnPrefShizuoka = 22,
    /// Aichi
    jpnPrefAichi = 23,
    /// Mie
    jpnPrefMie = 24,
    /// Shiga
    jpnPrefShiga = 25,
    /// Kyoto
    jpnPrefKyoto = 26,
    /// Osaka
    jpnPrefOsaka = 27,
    /// Hyogo
    jpnPrefHyogo = 28,
    /// Nara
    jpnPrefNara = 29,
    /// Wakayama
    jpnPrefWakayama = 30,
    /// Tottori
    jpnPrefTottori = 31,
    /// Shimane
    jpnPrefShimane = 32,
    /// Okayama
    jpnPrefOkayama = 33,
    /// Hiroshima
    jpnPrefHiroshima = 34,
    /// Yamaguchi
    jpnPrefYamaguchi = 35,
    /// Tokushima
    jpnPrefTokushima = 36,
    /// Kagawa
    jpnPrefKagawa = 37,
    /// Ehime
    jpnPrefEhime = 38,
    /// Kochi
    jpnPrefKochi = 39,
    /// Fukuoka
    jpnPrefFukuoka = 40,
    /// Saga
    jpnPrefSaga = 41,
    /// Nagasaki
    jpnPrefNagasaki = 42,
    /// Kumamoto
    jpnPrefKumamoto = 43,
    /// Oita
    jpnPrefOita = 44,
    /// Miyazaki
    jpnPrefMiyazaki = 45,
    /// Kagoshima
    jpnPrefKagoshima = 46,
    /// Okinawa
    jpnPrefOkinawa = 47
};

/// Functional Road class.  used in <FRC> EIS tag
enum eFunctionRoadClass
{
    /// invalid value
    eFRC_INVALID = 0,
    /// interstates, Autobahn
    eFRC_GLOBAL = 1,
    /// highways
    eFRC_MAIN = 2,
    /// regional mainroads
    eFRC_REGIONAL = 3,
    /// local mainroads
    eFRC_LOCAL = 4,
    /// residents roads
    eFRC_SMALL = 5,
    /// carpark roads
    eFRC_CARPARK = 6,
    /// roads on a company area
    eFRC_COMPANY = 7,
    /// opened for busses and cabs
    eFRC_BUSLANE = 8,
    /// private roads
    eFRC_PRIVATE = 9,
    /// limited e.g. bus lanes or private area, used if specified not clear
    eFRC_LIMITED = 10,
    /// not usable as roads
    eFRC_LOCKED
};

/// Sensor media type.
enum eSensorMediaType
{
    /// invalid value
    eEMT_UNKNOWN = 0,
    /// ETC
    eEMT_ETC = 1,
    /// VICS Optical
    eEMT_VICS_OPTICAL = 2,
    /// regional mainroads
    eEMT_VICS_RADIO = 3,
    /// local mainroads
    eEMT_DSRC = 4
};

/// ETC Gate Information
enum eETCGateInfo
{
    /// invalid value
    eETC_ROAD_UNKNOWN = 0,
    /// ETC Charging gate
    eETC_CHARGE = 0x00000001,
    /// ETC Charging gate (Refund)
    eETC_REFUND = 0x00000002,
    /// ETC PREGATE
    eETC_PREGATE = 0x00000010,
    /// ETC Road classification : Public road
    eETC_ROAD_PUBRIC = 0x00000100,
    /// ETC Road classification : Highway road
    eETC_ROAD_HIGHWAY = 0x00000200,

};

enum eRoadAttributesType
{
    /// invalid value
    eRATS_Unknow = 0,
    /// road default speed
    eRATS_DefaultSpeed = 1,
};

/// OverTheAir Update - enum to represent Update Task Status
enum eDownloadStatus
{
    // Download is not yet started
    eDownloadNone,
    // Download is in progress
    eDownloadOngoing,
    // Download is completed
    eDownloadComplete,
    // Retrying, transient error
    eDownloadRetrying,
    // Permanent error, server side e.g. invalid URL
    eDownloadFailed,
    // Permanent error, client side e.g. no disk space
    eDownloadErrorOnClient
};

enum eLaneCategory
{
    LANE_CATEGORY_NOINFO = 0,
    LANE_CATEGORY_ONE_LANE = 1,
    LANE_CATEGORY_TWO_OR_THREE_LANES = 2,
    LANE_CATEGORY_FOUR_OR_MORE_LANES = 3
};

enum eRoadSpeedCategoryType
{
    ROAD_SPEED_CATEGORY_GT_130_KMH = 0,
    ROAD_SPEED_CATEGORY_81_TO_130_KMH = 1,
    ROAD_SPEED_CATEGORY_51_TO_80_KMH = 2,
    ROAD_SPEED_CATEGORY_0_TO_50_KMH = 3
};

enum eDigitalMapEffectiveSpeedLimitType
{
    DIG_MAP_SPD_LIM_TYPE_IMPLICIT = 0,
    DIG_MAP_SPD_LIM_TYPE_EXPLICIT_TRAFFIC_SIGNS = 1,
    DIG_MAP_SPD_LIM_TYPE_EXPLICIT_ONLY_BY_NIGHT = 2,
    DIG_MAP_SPD_LIM_TYPE_EXPLICIT_ONLY_BY_DAY = 3,
    DIG_MAP_SPD_LIM_TYPE_EXPLICIT_TIME_OF_DAY = 4,
    DIG_MAP_SPD_LIM_TYPE_EXPLICIT_ONLY_BY_RAIN = 5,
    DIG_MAP_SPD_LIM_TYPE_EXPLICIT_ONLY_BY_SNOW = 6,
    DIG_MAP_SPD_LIM_TYPE_UNKNOWN = 7
};

enum eDigitalMapDrivingSide
{
    DIG_MAP_DRIV_SIDE_LEFT = 0,
    DIG_MAP_DRIV_SIDE_RIGHT = 1
};

/// lane border line types
enum eLaneInfoBorder
{
    /// no information about border line
    libUnknown = 0,
    /// border line is normal dash = :
    libNormalDash = 1,
    /// border line is double continuous = ||
    libDoubleContinuous = 2,
    /// border line is single continuous = |
    libSingleContinuous = 3,
    /// border line is open from right = |:
    libOpenFromRight = 4,
    /// border line is open from left = :|
    libOpenFromLeft = 5,
    /// border line is rapid dash
    libRapidDash = 6,
};

/// lane type
enum eLaneInfoType
{
    /// standard lane
    litNormal = 0,
    /// closed lane for some reason
    litClosed = 1,
    /// lane used by bus or taxi
    litBusTaxi = 2,
    /// lane with gate
    litGate = 3,
};

/// lane arrow type information
enum eLaneInfoArrow
{
    liaUnknown = 0,
    // no highlight, single arrow
    liaStraight = 1,
    liaSlightRight = 2,
    liaRight = 3,
    liaSharpRight = 4,
    liaSlightLeft = 5,
    liaLeft = 6,
    liaSharpLeft = 7,
    // no highlight, straight+arrows
    liaStraight_SlightRight = 8,
    liaStraight_SlightLeft = 9,
    liaStraight_SlightLeft_SlightRight = 10,
    liaStraight_Right = 11,
    liaStraight_Left = 12,
    liaStraight_Left_Right = 13,
    // no highlight, dual arrows
    liaSlightRight_SharpRight = 14,
    liaSlightLeft_SharpLeft = 15,
    liaSlightLeft_SlightRight = 16,
    liaSlightRight_Right = 17,
    liaSlightLeft_Left = 18,
    liaLeft_Right = 19,
    // snigle highlight, straight+arrows
    liaStraightHL_SlightRight = 20,
    liaStraight_SlightRightHL = 21,
    liaStraightHL_SlightLeft = 22,
    liaStraight_SlightLeftHL = 23,
    liaStraightHL_SlightLeft_SlightRight = 24,
    liaStraight_SlightLeftHL_SlightRight = 25,
    liaStraight_SlightLeft_SlightRightHL = 26,
    liaStraightHL_Right = 27,
    liaStraight_RightHL = 28,
    liaStraightHL_Left = 29,
    liaStraight_LeftHL = 30,
    liaStraightHL_Left_Right = 31,
    liaStraight_LeftHL_Right = 32,
    liaStraight_Left_RightHL = 33,
    // single highlight, dual arrows
    liaSlightRightHL_SharpRight = 34,
    liaSlightRight_SharpRightHL = 35,
    liaSlightLeftHL_SharpLeft = 36,
    liaSlightLeft_SharpLeftHL = 37,
    liaSlightLeftHL_SlightRight = 38,
    liaSlightLeft_SlightRightHL = 39,
    liaSlightRightHL_Right = 40,
    liaSlightRight_RightHL = 41,
    liaSlightLeftHL_Left = 42,
    liaSlightLeft_LeftHL = 43,
    liaLeftHL_Right = 44,
    liaLeft_RightHL = 45,
    // all highlight
    liaStraightHL = 46,
    liaSlightRightHL = 47,
    liaRightHL = 48,
    liaSharpRightHL = 49,
    liaSlightLeftHL = 50,
    liaLeftHL = 51,
    liaSharpLeftHL = 52,
    // all highlight, straight+arrows
    liaStraightHL_SlightRightHL = 53,
    liaStraightHL_SlightLeftHL = 54,
    liaStraightHL_SlightLeftHL_SlightRightHL = 55,
    liaStraightHL_RightHL = 56,
    liaStraightHL_LeftHL = 57,
    liaStraightHL_LeftHL_RightHL = 58,
    // all highlight, dual arrows
    liaSlightRightHL_SharpRightHL = 59,
    liaSlightLeftHL_SharpLeftHL = 60,
    liaSlightLeftHL_SlightRightHL = 61,
    liaSlightRightHL_RightHL = 62,
    liaSlightLeftHL_LeftHL = 63,
    liaLeftHL_RightHL = 64,
};

/// Enum to represent vehicle types.
enum eVehicleType
{
    /// Type to identify a car. Currently not used on the opennav interface.
    Car = 0,
    /// Type to identify a taxi. Currently not used on the opennav interface.
    Taxi = 1,
    /// Type to identify a coach.
    Coach = 2,
    /// Type to identify a truck.
    Truck = 3,
    /// Type to identify a emergency vehicle (e.g. ambulance). Currently not
    /// used on the opennav interface.
    Emergency = 4,
    /// Type to identify a motorcycle. Currently not used on the opennav
    /// interface.
    MotorCycle = 9, // values 5..8 are left out intentionally to correspond to
                    // the internal enum of NaviServer
};

/// Enum to represent hazardous load categories.
/// http://en.wikipedia.org/wiki/ADR_(treaty)
enum eHazardousLoadCategories
{
    /// Type to identify not hazardous materials.
    noHazard = 0,
    /// Type to identify class 1 materials (Explosive substances and articles).
    hazardClass_1 = 1 << 0,
    /// Type to identify class 2.1 materials (Flammable gases)
    hazardClass_2_1 = 1 << 1,
    /// Type to identify class 2.2  materials (Non-flammable and non-toxic
    /// gasses, likely to cause asphyxiation)
    hazardClass_2_2 = 1 << 2,
    /// Type to identify class 2.3  materials (Toxic gases)
    hazardClass_2_3 = 1 << 3,
    /// Type to identify class 2 materials (Gases, including compressed,
    /// liquified and dissolved under pressure gases and vapors)
    hazardClass_2 = hazardClass_2_1 | hazardClass_2_2 | hazardClass_2_3,
    /// Type to identify class 3 materials (Flammable liquids)
    hazardClass_3 = 1 << 4,
    /// Type to identify class 4.1 materials (Flammable solids, self-reactive
    /// substances and solid desensitized explosives)
    hazardClass_4_1 = 1 << 5,
    /// Type to identify class 4.2 materials (Substances liable to spontaneous
    /// combustion)
    hazardClass_4_2 = 1 << 6,
    /// Type to identify class 4.3 materials (Substances which, in contact with
    /// water, emit flammable gases)
    hazardClass_4_3 = 1 << 7,
    /// Type to identify class 4 materials
    hazardClass_4 = hazardClass_4_1 | hazardClass_4_2 | hazardClass_4_3,
    /// Type to identify class 5.1 materials (Oxidizing substances)
    hazardClass_5_1 = 1 << 8,
    /// Type to identify class 5.2 materials (Organic peroxides)
    hazardClass_5_2 = 1 << 9,
    /// Type to identify class 5 materials
    hazardClass_5 = hazardClass_5_1 | hazardClass_5_2,
    /// Type to identify class 6.1 materials (Toxic substances)
    hazardClass_6_1 = 1 << 10,
    /// Type to identify class 6.2 materials (Infectious substances)
    hazardClass_6_2 = 1 << 11,
    /// Type to identify class 6 materials
    hazardClass_6 = hazardClass_6_1 | hazardClass_6_2,
    /// Type to identify class 7 materials (Radioactive material)
    hazardClass_7 = 1 << 12,
    /// Type to identify class 8 materials (Corrosive substances)
    hazardClass_8 = 1 << 13,
    /// Type to identify class 9 materials (Miscellaneous dangerous substances
    /// and articles)
    hazardClass_9 = 1 << 14,
};

/// Enum value to represent, which regulation is violated.
enum eRegulationIdentifier
{
    /// Identifies that *height* regulation is violated. (Hard limit)
    heightRegulationIsViolated = 1,
    /// Identifies that *current weight* regulation is violated. (Hard limit)
    currentWeightRegulationIsViolated = 2,
    /// Identifies that *width* regulation is violated. (Hard limit)
    widthRegulationIsViolated = 3,
    /// Identifies that *length* regulation is violated. (Hard limit)
    lengthRegulationIsViolated = 4,
    /// Identifies that traffic is not allowed with the vehicle selected in
    /// profile (legal restriction, e.g. no entry for trucks)
    trafficWithSelectedVehicleIsNotAllowed = 5,
    /// Identifies that *maximum weight* regulation is violated. (Soft limit)
    maximumWeightRegulationIsViolated = 6,
    /// Identifies that *maximum weight for one axis* regulation is violated.
    /// (Soft limit)
    maximumWeightForOneAxisRegulationIsViolated = 7,
    ///// Identifies that *maximum weight for double axis* regulation is
    /// violated. (Soft limit)
    // maximumWeightForDoubleAxisRegulationIsViolated = 8, // Not supported,
    // reserved for future use.
    /// Identifies that *hazardous load* regulation is violated. (Soft limit)
    hazardousLoadRegulationIsViolated = 9,
    /// Identifies that *truck/coach regulation data is unavailable* for the map
    /// partition (country). (Soft limit)
    unavailableTruckCoachRegulationData = 10,
    /// Identifies that the current *vehicle type* is not allowed on the current
    /// road segment. (e.g. unpaved road or cycle path for trucks) (Soft limit)
    vehicleTypeRegulationIsViolated = 11,
    /// Identifies that *special permit* is required for the current road
    /// segment (Soft limit)
    specialPermitIsRequired = 12,
    /// Identifies that a *user avoid* restriction (Route Settings) is violated
    /// (Soft limit)
    userAvoidRestrictionIsViolated = 13,
    /// Identifies that a *time domain* restriction is violated (Soft limit)
    timeDomainRestrictionIsViolated = 14,
};

/// enum value to identify, that the applicability of a restriction is altered
/// by a run-time evaluated condition
enum eRegulationAttribute
{
    /// Identifies that the regulation is unconditional. This value is the
    /// default, so can be omitted.
    RegulationAttributeNone = 0,
    /// Identifies that the regulation is not to be applied when delivering to
    /// the restricted area.
    RegulationAttributeExceptDelivery = 1 << 0,
};

/// enum values for vehicle regulation violation report flags
/// @see informVehicleRegulationViolations
enum eViolationsReportFlags
{
    /// Identifies that only the distance information has been changed since the
    /// last call of informVehicleRegulationViolations.
    changedDistanceOnly = 1 << 0,
};

/// enum value to indicate whether the car (truck) can leave the area without
/// violating regulations
enum eTruckDeadendState
{
    /// Identifies that truck deadend detection is inactive. We report this
    /// value when we have a planned route.
    TruckDeadendStateDetectionIsInactive = 0,
    /// Identifies that the car position is not in deadend.
    TruckDeadendStateCanLeaveAreaWithoutViolatingRegulations = 1,
    /// Identifies that the car can leave the area only with violating
    /// regulations
    TruckDeadendStateUnavoidableRegulationsAhead = 2,
    /// Compare the detected truck deadend state to this value to have boolean
    /// information:
    /// if (Value >= TruckDeadendStateThreshold) {/* show deadend warning */}
    TruckDeadendStateThreshold = TruckDeadendStateUnavoidableRegulationsAhead,
    /// Identifies that the car is just violating regulations
    TruckDeadendStateRegulationsCurrentlyViolated = 3,
};

/// Enum to represent the level of details of truck warnings on the map.
enum eMapTruckWarning
{
    /// Enum to represent the case when no truck warnings are shown on the map.
    NoTruckWarningsOnMap = 0,
    /// Enum to represent the case when important truck warnings are shown on
    /// the map.
    /// Tables are shown on visible routes or for the next 2-3 road segments.
    ImportantTruckWarningsOnMap = 1,
    ///  Enum to represent the case when all truck warnings are shown on the
    ///  map.
    ///	All restricted road segments are colored. In high zoom levels tables are
    /// shown as well.
    AllTruckWarningsOnMap = 2,
};

/*------------------------------------------------------------------------------
 *   STRUCTS
 *----------------------------------------------------------------------------*/

/// Structure to represent a vector of Strings.
typedef struct _SStringArray
{
    const char** pStrings;
    Int32 count;
} SStringArray;

/// Structure to represent a vector of Strings.
typedef struct _SRouteHandleList
{
    /// Array of route handles. A unique ID identifying a calculated route
    /// defined by specific eRouteModes.
    const Int32** pIntegers;
    /// Number of available route handles.
    Int32 count;
} SRouteHandleList;

typedef struct _SDestinationElement
{
    /// EIS encoded hard destination.  Must contain <GEO> eis element, which is
    /// the coordinate for the destination.  A hard destination is "arrived at".
    /// Route guidance will end upon reaching the hard destination of the
    /// active  route.
    /// note: must point to a valid string pointer, NULL is not valid.
    const char* pHardDestination;

    /// EIS encoded soft destinations. Each soft destination must contain a
    /// <GEO> eis element. Soft destinations are only used to shape the route
    /// and are it is not necessary to arrive at or pass through soft
    /// destinations.
    const SStringArray* pSoftDestinations;

} SDestinationElement;

/// Structure to represent a vector of Destination Elements.
typedef struct _SDestinationElementList
{
    /// The destination elements, e.g. contains both hard and soft destinations.
    SDestinationElement** pElements;

    /// Number of elements in the list.
    Int32 count;

} SDestinationElementList;

/// Describes the active route handle, route element index, and route segment
/// index.
typedef struct _SActiveRouteDescription
{
    /// Route handle of the active route. A value is greater than 0 is valid.
    Int32 handle;
    /// Route element index of the active route. A value is greater than -1 is
    /// valid.
    Int32 elementIndex;
    /// Route segment index of the active route. A value is greater than -1 is
    /// valid.
    Int32 segmentIndex;

} SActiveRouteDescription;

/// Structure to represent a vector of route modes.
typedef struct _SRouteModeList
{
    /// List of route mode bitfields (enums eRouteMode).
    const UInt32** pModes;
    /// Number of elements in the list.
    Int32 count;

} SRouteModeList;

/// Structure to represent a vector of sort options.
typedef struct _SSortOptionsList
{
    /// List of sort options (enums eSortOption).
    const eSortOption** pOptions;
    /// Number of elements in the list.
    Int32 count;

} SSortOptionList;

/// Structure to hold the language and the speaker id.
typedef struct _SNavLangInfo
{
    /// nav language.
    enum eNavLanguage lang;

    /// speaker id.
    Int32 speaker;

} SNavLangInfo;

/// Contains the time and distance remaining to the next destination.
typedef struct _STimeAndDistance
{
    /// Time in minutes. -1 represents an invalid value.
    Int32 time;
    /// Distance in meters. -1 represents an invalid value.
    Int32 distance;

} STimeAndDistance;

/// Structure to represent a vector of time and distance.
typedef struct _STimeAndDistanceList
{
    /// List of time and distances
    const STimeAndDistance** pTimeAndDistances;
    /// Number of elements in the list.
    Int32 count;

} STimeAndDistanceList;

/// Contains the next maneuver information.
typedef struct _SNextManeuverInfo
{
    /// Signal for client to notify user that maneuver is approaching.
    bool approachingManeuver;
    /// Bargraph percentage, -1 if no bargraph should be shown
    Int32 bargraph;
    /// Guidance arrow direction, -1 if no arrow should be shown.  A direction
    ///  value relative to the vehicle direction at 0 degrees and increasing
    ///  counter-clockwise.  Range of the signal is 0-255.
    Int32 direction;
} SNextManeuverInfo;

/// Enum to represent if a phone is registered to the NavFusion service.
enum eNavFusionState
{
    /// Type to identify that the phone is new and not connected to the
    /// NavFusion service.
    newPhone = 1 << 0,
    /// Type to identify that the phone is already connected to the NavFusion
    /// service.
    boundPhone = 1 << 1
};

/// Enum to represent if Synchronization is in progress.
enum eSyncEvent
{
    /// Synchronization in progress (sent at start; at map synchronization it is
    /// also sent periodically with percentage filled)
    syncEventUpdating = 0,
    /// Synchronization finished
    syncEventUpdated = 1,
    /// Synchronization failed
    syncEventFailed = 2
};

/// Enumerations for update type
enum eUpdateType
{
    /// static (dealer) update. not specific to any concrete device.
    updTypeStatic = 1 << 0,
    /// specialized (toolbox) update. specific to a concrete device.
    updTypeSpecial = 1 << 1
};

/// Enumerations for file locality type
enum eFileLocalityType
{
    /// content file specific to a location (country, part of country or region)
    fileLocTypeLocalizedContent = 0,
    /// content file not related to any location
    fileLocTypeNotLocalizedContent = 1,
    /// non content file (license or temporary files)
    fileLocTypeNonContent = 2
};

/// Enum to represent user POI type
enum eUserPOIType
{
    euptHome = 0,
    euptFavourite = 1,
    euptPinIcon = 2,
};

/// Enumerations of VICS Action Type
enum eVICSActionType
{
    /// NaviServer should start read out TTS
    eVATStart = 0,
    /// NaviServer should abort pllaying TTS (In this case, other tags should
    /// not be provided)
    eVATAbort = 1,
};

/// enumerations of condition of delete driving path
enum eDeleteConditionDrivingPath
{
    /// no condition
    eDCDPNoCondition = 0,
    /// delete driving path when ccp is around home
    eDCDPAroundHome = 1,
    /// delete driving path when it is one week before.
    eDCDPOneWeekBefore = 2,
};

/// traffic information.
enum eTrafficInformation
{
    /// traffic jam
    eTrafficInfoJam = 0,
    /// traffic control
    eTrafficInfoControl = 1,
    /// traffic disturbance
    eTrafficInfoDisturbance = 2,
    /// traffic smooth
    eTrafficInfoSmooth = 4,
    /// unknow
    eTrafficUnknow = 5,
};
/**
 *  Shanmukha: Enum copied from OpenNavTypes.hpp
 *
 * Valid state, valid or not valid.
 */
enum eNavValidState
{
    /**
     * The results are valid.
     */
    eValid = 1,
    /**
     * The results are not valid.
     */
    eInvalid = 2,
};

#pragma pack(pop)

#endif // OPENNAV_OPENNAVTYPES_H
