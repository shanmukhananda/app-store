#include "Logger.hpp"

const std::string SPEED_FILE = "speed.log";

Logger* Logger::m_pInstance = NULL; //!OCLINT (old code using singleton)

Logger* Logger::Instance()
{
    if (NULL == m_pInstance)
    {
        m_pInstance = new Logger();
    }

    return m_pInstance;
}

void Logger::DR_Speed(int speed)
{
    m_dr_speedMutex.lock();
    m_dr_speed = speed;
    m_dr_speedMutex.unlock();
}

void Logger::Inst_Speed(int speed)
{
    m_inst_speedMutex.lock();
    m_inst_speed = speed;
    m_inst_speedMutex.unlock();
}

void Logger::vSetLogState(bool state)
{
    m_bIsLogEnabled = state;
}

void Logger::vLogSpeed()
{
    int dr_speed = 0;
    m_dr_speedMutex.lock();
    dr_speed = m_dr_speed;
    m_dr_speedMutex.unlock();

    int inst_Speed = 0;
    m_inst_speedMutex.lock();
    inst_Speed = m_inst_speed;
    m_inst_speedMutex.unlock();

    QString outString = "D:" + QString::number(dr_speed) + "\t" +
                        "I:" + QString::number(inst_Speed);

    if (m_bIsLogEnabled)
    {
        oFile << outString.toStdString() << std::endl;
    }
}

Logger::Logger(QObject* parent)
    : QObject(parent), m_dr_speed(0), m_inst_speed(0), m_bIsLogEnabled(false)
{
    oFile.open(SPEED_FILE.c_str(), std::ios_base::trunc);
    assert(oFile.good());
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(vLogSpeed()));
    m_timer.start(1000);
}
