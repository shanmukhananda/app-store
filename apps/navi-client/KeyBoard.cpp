#include "KeyBoard.hpp"
#include "Common.hpp"

#include "ui_KeyBoard.h"

KeyBoard::KeyBoard(QWidget* parent) : QDialog(parent), ui(new Ui::KeyBoard)
{
    ui->setupUi(this);
    ui->listView_LocationList->setModel(
        Logic::Instance()->pLocationListModel());
    ui->listView_SpellCharacters->setModel(
        Logic::Instance()->pSpellCharListModel());
}

KeyBoard::~KeyBoard()
{
    delete ui;
}

void KeyBoard::on_lineEdit_locationInput_cursorPositionChanged(int /*arg1*/,
                                                               int /*arg2*/)
{
    QString str = ui->lineEdit_locationInput->text();

    int strLen = str.length();

    if (strLen > 0)
    {
        QChar enteredLastChar = str[strLen - 1];

        if (false == Logic::Instance()->bIsCharInSpellList(enteredLastChar))
        {
            qDebug() << "Keyboard entered character: " << enteredLastChar
                     << "is not present in spell list";
            QString newStr = str.remove(strLen - 1, 1);
            ui->lineEdit_locationInput->setText(newStr);
            Logic::Instance()->vHandleLIGetSpell(newStr);
        }
    }
}

void KeyBoard::on_listView_LocationList_doubleClicked(
    const QModelIndex& /*index*/)
{
    qDebug() << __FUNCTION__;

    close();
}

void KeyBoard::on_listView_SpellCharacters_doubleClicked(
    const QModelIndex& index)
{
    qDebug() << __FUNCTION__;

    // whenever the spell character's list view is clicked
    // append the string to line edit.

    QString currentText = ui->lineEdit_locationInput->text();
    currentText += index.data().toString();
    ui->lineEdit_locationInput->setText(currentText);
}
