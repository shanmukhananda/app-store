#pragma once

#include <QDialog>

namespace Ui
{
class RouteCalculation;
}

class RouteCalculation : public QDialog
{
    Q_OBJECT

public:
    explicit RouteCalculation(QWidget* parent = 0);
    ~RouteCalculation();

private slots:
    void on_pushButton_regression_clicked();

private:
    Ui::RouteCalculation* ui;
};
