#include "NaviClient.hpp"

#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication appl(argc, argv);

    Logic::Instance();
    Service::Instance();
    NaviClient nav_client;
    nav_client.show();
    nav_client.showIpAddressDialog();

    return appl.exec();
}
