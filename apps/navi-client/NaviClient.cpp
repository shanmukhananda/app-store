#include "NaviClient.hpp"
#include "DialogIP.hpp"

#include "ui_NaviClient.h"

NaviClient::NaviClient(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::NaviClient)
    , m_pMapPage(NULL)
    , m_LocationInput(NULL)
    , m_pPositioning(NULL)
    , m_pRouteCalc(NULL)
{
    ui->setupUi(this);
}

NaviClient::~NaviClient()
{
    delete ui;
}

void NaviClient::showIpAddressDialog()
{
    DialogIP* diag = new DialogIP();
    diag->show();
}

void NaviClient::on_pushButton_Map_clicked()
{
    if (NULL == m_pMapPage)
    {
        m_pMapPage = new Map(this);
    }
    m_pMapPage->show();
}

void NaviClient::on_pushButton_LI_clicked()
{
    if (NULL == m_LocationInput)
    {
        m_LocationInput = new LocationInput(this);
    }
    m_LocationInput->show();
}

void NaviClient::on_pushButton_Positioning_clicked()
{
    if (NULL == m_pPositioning)
    {
        m_pPositioning = new Poistioning(this);
    }
    m_pPositioning->show();
    Logic::Instance()->vHandlePositioningEntry();
}

void NaviClient::on_pushButton_Route_clicked()
{
    if (NULL == m_pRouteCalc)
    {
        m_pRouteCalc = new RouteCalculation(this);
    }
    m_pRouteCalc->show();
    Logic::Instance()->vHandlePositioningEntry();
}
