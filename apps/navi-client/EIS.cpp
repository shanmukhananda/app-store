#include "EIS.hpp"

std::string EIS::sGetTagData(const std::string& tag)
{
    StringPOS ValWithPercentS = getTagDataUnformated(m_EISData, tag);
    const std::string FORMAT_SPECIFIER = "%s";
    const std::string TXT_TAG = "<TXT>";

    size_t formatSpecifierPos = 0;
    size_t startIndex = 0;
    size_t position = 0;

    std::string strWithoutFormatSpec = ValWithPercentS.data;
    while ((formatSpecifierPos = ValWithPercentS.data.find(
                FORMAT_SPECIFIER, startIndex)) != std::string::npos)
    {
        StringPOS txtData = getTagDataUnformated(m_EISData, "TXT", position);
        strWithoutFormatSpec.replace(formatSpecifierPos,
                                     FORMAT_SPECIFIER.length(), txtData.data);
        ValWithPercentS.data = strWithoutFormatSpec;
        position += (txtData.pos + TXT_TAG.length() + txtData.data.length());
    }

    return strWithoutFormatSpec;
}

void EIS::vAddTagValue(const std::string& tag, const std::string& value)
{
    m_EISData +=
        std::string("<") + tag + std::string(">") + value + std::string("<*>");
}

std::string EIS::GetData()
{
    return m_EISData;
}

EIS::EIS(const std::string& data) : m_EISData(data)
{
}

EIS::EIS()
{
}

EIS::~EIS()
{
}

EIS::StringPOS
EIS::getTagDataUnformated(const std::string& sEIS, const std::string& sTag,
                          size_t startIndex)
{
    std::string sStartPattern = std::string("<") + sTag + std::string(">");
    std::string sEndPattern = "<*>";
    StringPOS sRetVal;

    size_t firstPos = sEIS.find(sStartPattern, startIndex);

    if (firstPos == std::string::npos)
        return sRetVal;

    size_t lastPos = sEIS.find(sEndPattern, firstPos);

    if (lastPos == std::string::npos)
        return sRetVal;

    size_t lengthOfData = lastPos - (firstPos + sStartPattern.length());
    sRetVal.data = sEIS.substr(firstPos + sStartPattern.length(), lengthOfData);
    sRetVal.pos = firstPos;

    // if (firstPos != std::string::npos)
    // {
    //     size_t lastPos = sEIS.find(sEndPattern, firstPos);
    //     if (lastPos != std::string::npos)
    //     {
    //         size_t lengthOfData = lastPos - (firstPos +
    //         sStartPattern.length());
    //         sRetVal.data = sEIS.substr(firstPos + sStartPattern.length(),
    //         lengthOfData);
    //         sRetVal.pos = firstPos;
    //     }
    //     else
    //     {
    //         std::cerr << "ERROR: End pattern " << sEndPattern << " is not
    //         found"
    //                   << std::endl;
    //     }
    // }
    // else
    // {
    //     std::cerr << "ERROR: Start pattern " << sStartPattern << " is not
    //     found"
    //               << std::endl;
    // }

    return sRetVal;
}

void EIS::vAppend(const std::string& str)
{
    m_EISData.append(str);
}