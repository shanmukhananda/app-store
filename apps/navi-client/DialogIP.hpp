#pragma once

#include "NaviClient.hpp"

#include <QDialog>

namespace Ui
{
class DialogIP;
}

class DialogIP : public QDialog
{
    Q_OBJECT

public:
    explicit DialogIP(QWidget* parent = 0);
    ~DialogIP();

private slots:
    void on_pushButton_Ok_clicked();

    void on_pushButton_Cancel_clicked();

private:
    Ui::DialogIP* ui;
};
