#pragma once

#include "Common.hpp"
#include "UILogicData.hpp"

class Logic : public QObject
{
    Q_OBJECT

public:
    static Logic* Instance();

    void vHandleMapVisibility(const QString& args);

    void vHandleMapViewType(const QString& args);

    void vHandleMapHeading(const QString& args);

    void vHandleLIEnterCountry();

    void vHandleLIGetSpell(QString str);

    QStringListModel* pLocationListModel();

    QStringListModel* pSpellCharListModel();

    bool bIsCharInSpellList(QChar spellChar);

    void vHandlePositioningEntry();

    void vRegisterLogicEventHandlers();

signals:
    void UISENGPSInfo(UISenGPSData&);
    void UIPOSPositionInfo(UIPosPositionData&);
    void UICalibrationLevelInfo(UICalibrationLevel&);
    void UIReverseGearInfo(UIReverseGear&);
    void UIGyroSumInfo(UIGyroSum&);
    void UITemperatureInfo(UITemperature&);
    void UIInstSpeedInfo(UIInstSpeed&);

public slots:

    void vSlotSenGpsInfoProperty(const SENGPSInfoProperty&);
    void vSlotSenGpsInfoSignal(const SENGPSInfoSignalResponse&);
    void vSlotPOSPositionProperty(const POSPositionProperty&);
    void vSlotPOSPositionSignal(const POSPositionSignalResponse&);
    void vSlotPOSCalibirationSignal(const POSCalibrationLevelSignalResponse&);
    void vSlotPOSCalibrationProperty(const POSCalibrationLevelProperty&);
    void vSlotSENTemperatureSignal(const SENTemperatureSignalResponse&);
    void vSlotSENTemperatureProperty(const SENTemperatureProperty&);
    void vSlotSENReverseGearSignal(const SENReverseGearSignalResponse&);
    void vSlotSENReverseGearProperty(const SENReverseGearProperty&);
    void vSlotSENGyroSumSignal(const SENGyroSumSignalResponse&);
    void vSlotSENGyroSumProperty(const SENGyroSumProperty&);
    void vSlotInstSpeedValue(const InstSpeedValueSignalResponse&);

private:
    explicit Logic(QObject* parent = 0);

    Logic(const Logic&);

    Logic& operator=(const Logic&);

    static Logic* m_pInstance;

    QStringListModel* m_pLocationListModel;

    QStringListModel* m_pSpellCharListModel;

    QString m_currentLIData;
};
