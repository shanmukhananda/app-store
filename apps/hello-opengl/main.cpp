#include <GL/glut.h>

void displayMe()
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glPointSize(5.0);
    glBegin(GL_POLYGON);
    {
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(0.0, 0.0, 0.0);

        glColor3f(0.0, 1.0, 0.0);
        glVertex3f(0.5, 0.0, 0.0);

        glColor3f(0.0, 0.0, 1.0);
        glVertex3f(0.5, 0.5, 0.0);

        glColor3f(1.0, 1.0, 0.0);
        glVertex3f(0.0, 0.5, 0.0);
    }
    glEnd();

    glFlush();
    glutSwapBuffers();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(300, 300);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Hello world");
    glutDisplayFunc(displayMe);
    glutMainLoop();

    return 0;
}
