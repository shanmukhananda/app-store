#include "ping-pong.hpp"

CPingPong::CPingPong(int count_)
{
    flag = true;
    count = 2 * count_;
}

void CPingPong::start()
{
    std::thread pingThread(&CPingPong::ping, this);
    std::thread pongThread(&CPingPong::pong, this);

    pingThread.join();
    pongThread.join();
}

void CPingPong::ping()
{
    while (true)
    {
        std::unique_lock<std::mutex> locker(m);
        while (false == flag)
        {
            cv.wait(locker);
        }

        if (count <= 0)
            break;
        std::cout << "ping\n";
        --count;
        flag = false;

        cv.notify_all();
    }
}

void CPingPong::pong()
{
    while (true)
    {
        std::unique_lock<std::mutex> locker(m);
        while (true == flag)
        {
            cv.wait(locker);
        }

        if (count <= 0)
            break;
        std::cout << "pong\n";
        --count;
        flag = true;

        cv.notify_all();
    }
}
