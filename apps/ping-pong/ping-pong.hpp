#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

class CPingPong
{
public:
    explicit CPingPong(int count_);
    void start();

private:
    void ping();
    void pong();

    std::mutex m;
    std::condition_variable cv;
    bool flag;
    int count;
};
