#include "CommandParser.hpp"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <utility>

using namespace std;

CommandParser::CommandParser(const std::string& file)
    : MAX_X(800), MAX_Y(480), MAX_SIZE(size_t(-1))
{
    m_lineNumber = 0;
    m_bIsParsingSuccess = true;
    m_bIsBlockStarted = false;

    m_blockCmdStart = MAX_SIZE;
    m_blockCmdEnd = MAX_SIZE;

    vCreateCommandToCANHKMap();
    vCreatePossibleCommandList();
    vCreatePossibleHKButtonList();
    vCreatePossibleHKPressList();
    vCreateHKPressStringToValueMap();
    vCreatePossibleDirectives();

    m_CommandFile.open(file.c_str());
    if (m_CommandFile.good())
    {
        vParseCommandFile();
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(("ERROR: Cannot open the file %s", file.c_str()));
    }
}

CommandParser::~CommandParser()
{
    for (map<string, tCommandList>::iterator it =
             m_BlockNameAndCmdlistMap.begin();
         it != m_BlockNameAndCmdlistMap.end(); ++it)
    {
        tCommandList& commandList = it->second;
        vDestroyCommandList(commandList);
    }

    vDestroyCommandList(m_instructionList);
}

void CommandParser::vCreatePossibleCommandList()
{
    m_possibleCommands.insert("touch");
    m_possibleCommands.insert("hardkey");
    m_possibleCommands.insert("screenshot");
    m_possibleCommands.insert("wait");
}

void CommandParser::vCreatePossibleHKButtonList()
{
    m_possibleHKButtons.insert("up");
    m_possibleHKButtons.insert("down");
    m_possibleHKButtons.insert("left");
    m_possibleHKButtons.insert("right");
    m_possibleHKButtons.insert("select");
    m_possibleHKButtons.insert("volume_up_rear");
    m_possibleHKButtons.insert("volume_down_rear");
    m_possibleHKButtons.insert("volume_up_front");
    m_possibleHKButtons.insert("volume_up_front");
    m_possibleHKButtons.insert("source");
    m_possibleHKButtons.insert("seek_up");
    m_possibleHKButtons.insert("seek_down");
    m_possibleHKButtons.insert("ptt_front");
    m_possibleHKButtons.insert("ptt_rear");
    m_possibleHKButtons.insert("home");
}

void CommandParser::vCreatePossibleHKPressList()
{
    m_possibleHKPress.insert("shortpress");
    m_possibleHKPress.insert("longpress");
}

void CommandParser::vCreateCommandToCANHKMap()
{
    vAddToCommandToHKMap("up", "Up");
    vAddToCommandToHKMap("down", "Down");
    vAddToCommandToHKMap("left", "Right");
    vAddToCommandToHKMap("right", "Left");
    vAddToCommandToHKMap("select", "Select");
    vAddToCommandToHKMap("home", "Home");
    vAddToCommandToHKMap("volume_up_front", "Vol+(F)");
    vAddToCommandToHKMap("volume_down_front", "Vol-(F)");
    vAddToCommandToHKMap("ptt_front", "PTT(F)");
    vAddToCommandToHKMap("seek_up", "Seek+");
    vAddToCommandToHKMap("seek_down", "Seek-");
    vAddToCommandToHKMap("volume_up_rear", "Vol+(R)");
    vAddToCommandToHKMap("volume_down_rear", "Vol-(R)");
    vAddToCommandToHKMap("source", "Source");
    vAddToCommandToHKMap("ptt_rear", "PTT(R)");
}

void CommandParser::vCreateHKPressStringToValueMap()
{
    m_HKPressStringToValue.insert(pair<string, int>("shortpress", 1));
    m_HKPressStringToValue.insert(pair<string, int>("longpress", 2));
}

void CommandParser::vCreatePossibleDirectives()
{
    m_possibleDirectives.insert("DEFINE");
    m_possibleDirectives.insert("CALL");
    m_possibleDirectives.insert("BLOCK_START");
    m_possibleDirectives.insert("BLOCK_END");
}

bool CommandParser::bStartsWithCommand(const std::string& line,
                                       std::string& commandStr)
{
    bool bRetVal = false;
    for (std::set<std::string>::iterator iter = m_possibleCommands.begin();
         iter != m_possibleCommands.end(); ++iter)
    {
        string command = *iter;
        command += "("; // check for command + opening bracket
        size_t pos = line.find(*iter);

        if (pos == string::npos)
            continue;
        // command found, check if its in beginning of line
        if (0 == pos)
        {
            // -1 to compensate added '('
            commandStr = line.substr(0, command.length() - 1);
            bRetVal = true;
            break; // one line, one command
        }
        else
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Command '%s' is not at "
                 "the beginning of the line",
                 m_lineNumber, command.c_str()));
        }
    }
    return bRetVal;
}

std::vector<std::string>
CommandParser::GetParameters(const std::string& commandStr, char DELIMITER)
{
    const std::string PARAM_START = "(";
    const std::string PARAM_END = ");";

    std::vector<std::string> retValue;

    size_t firstPos = commandStr.find(PARAM_START);
    if (firstPos == string::npos)
    {
        size_t lastPos = commandStr.find(PARAM_END);
        if (lastPos == string::npos)
        {
            size_t lenOfParams = lastPos - firstPos - 1;
            string params = commandStr.substr(firstPos + 1, lenOfParams);
            std::stringstream sstream_(params);
            string eachParam;
            while (getline(sstream_, eachParam, DELIMITER))
            {
                // parses both multiple parameter & single parameter
                vRemoveSpaces(eachParam);
                if (bIsPartOfDefine(eachParam))
                {
                    eachParam = m_defineKeyValue[eachParam];
                }
                retValue.push_back(eachParam);
            }
        }
        else
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Command '%s' Closing bracket "
                 "')' is not found",
                 m_lineNumber, commandStr.c_str()));
        }
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Command '%s' Opening bracket '(' "
             "is not found",
             m_lineNumber, commandStr.c_str()));
    }

    return retValue;
}

void CommandParser::vAddToCommandToHKMap(const std::string& command,
                                         const std::string& canHK)
{
    m_CommandToHK.insert(pair<string, string>(command, canHK));
    // todo handle return value
}

void CommandParser::vExtractTouchData( //!OCLINT
    const std::string& touchLine)
{
    const std::string COMMAND = "touch";
    const int TOUCH_PRESSED = 1;

    vector<string> touchParams = GetParameters(touchLine);
    size_t touchParamsSize = touchParams.size();
    if (2 == touchParamsSize)
    {
        std::shared_ptr<TouchData> pTouchData(new TouchData());
        pTouchData->m_x = StringToNumber<int>(touchParams[0]);
        pTouchData->m_y = StringToNumber<int>(touchParams[1]);
        pTouchData->m_eventType = TOUCH_PRESSED;

        if (bIsValidCoordinate(pTouchData->m_x, pTouchData->m_y))
        {
            tCommand command_;
            command_.first = COMMAND;
            command_.second = pTouchData;

            m_instructionList.push_back(command_);
        }
        else if (bIsXCoordinateValid(pTouchData->m_x))
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Command '%s' Touch "
                 "coordinate X(%d)",
                 m_lineNumber, COMMAND.c_str(), pTouchData->m_x));
        }
        else if (bIsYCoordinateValid(pTouchData->m_y))
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Command '%s' Touch "
                 "coordinate Y(%d)",
                 m_lineNumber, COMMAND.c_str(), pTouchData->m_y));
        }
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Command '%s' Wrong number of "
             "parameters",
             m_lineNumber, COMMAND.c_str()));
    }
}

//(long name, high ncss, old code)
void CommandParser::vExtractHardKeyData( //!OCLINT
    const std::string& hardkeyLine)
{
    const std::string COMMAND = "hardkey";

    vector<string> hkParams = GetParameters(hardkeyLine);
    size_t hkParamsSize = hkParams.size();
    if (2 == hkParamsSize)
    {
        std::shared_ptr<HardKeyData> pHKData(new HardKeyData());
        const std::string& hkey = hkParams[0];

        pHKData->m_hkey = m_CommandToHK[hkey];

        if (pHKData->m_hkey.empty())
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Command '%s' Hardkey %s is "
                 "not defined",
                 m_lineNumber, COMMAND.c_str(), hkey.c_str()));
        }

        if (bIsPressTypeDefined(hkParams[1]))
        {
            pHKData->m_pressType = m_HKPressStringToValue[hkParams[1]];

            tCommand command_;
            command_.first = COMMAND;
            command_.second = pHKData;

            m_instructionList.push_back(command_); // push the press/long press
                                                   // command

            std::shared_ptr<HardKeyData> pHKDataRelease(new HardKeyData());
            pHKDataRelease->m_pressType = 0;
            pHKDataRelease->m_hkey = pHKData->m_hkey;

            tCommand pRel;
            pRel.first = COMMAND;
            pRel.second = pHKDataRelease;

            m_instructionList.push_back(pRel); // push the release command. 2
                                               // release events are still fine
        }
        else
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Command '%s' Hardkey Press "
                 "type %s is not defined",
                 m_lineNumber, COMMAND.c_str(), hkParams[1].c_str()));
        }
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Command '%s' Wrong number of "
             "parameters",
             m_lineNumber, COMMAND.c_str()));
    }
}

void CommandParser::vExtractScreeenShotData(const std::string& ssLine)
{
    const std::string COMMAND = "screenshot";

    string resolution = NumberToString(MAX_X) + "x" + NumberToString(MAX_Y);

    const std::string SCREEN_SHOT =
        "screenshot -size=" + resolution + " -filename=";

    vector<string> ssParams = GetParameters(ssLine);
    size_t ssParamsSize = ssParams.size();
    if (1 == ssParamsSize)
    {
        std::shared_ptr<ScreenShotData> pSSData(new ScreenShotData());
        pSSData->m_path = ssParams[0];

        pSSData->m_command = SCREEN_SHOT + pSSData->m_path;

        tCommand command_;
        command_.first = COMMAND;
        command_.second = pSSData;

        m_instructionList.push_back(command_);
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Command '%s' Wrong number of "
             "parameters",
             m_lineNumber, COMMAND.c_str()));
    }
}

void CommandParser::vExtractWaitData(const std::string& waitLine)
{
    const std::string COMMAND = "wait";
    vector<string> waitParams = GetParameters(waitLine);
    size_t waitParamsSize = waitParams.size();
    if (1 == waitParamsSize)
    {
        std::shared_ptr<WaitData> pWaitData(new WaitData());
        pWaitData->m_timeMs = StringToNumber<unsigned int>(waitParams[0]);

        tCommand command_;
        command_.first = COMMAND;
        command_.second = pWaitData;

        m_instructionList.push_back(command_);
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Command '%s' Wrong number of "
             "parameters",
             m_lineNumber, COMMAND.c_str()));
    }
}

void CommandParser::vParseCommandFile() //!OCLINT(long name, high CC, old code)
{
    string eachLine;
    while (getline(m_CommandFile, eachLine))
    {
        ++m_lineNumber;

        if (bIsCommentLine(eachLine))
            continue;

        string commandStr;
        string directiveStr;
        if (bStartsWithCommand(eachLine, commandStr))
        {
            if ("touch" == commandStr)
            {
                vExtractTouchData(eachLine);
            }
            else if ("hardkey" == commandStr)
            {
                vExtractHardKeyData(eachLine);
            }
            else if ("screenshot" == commandStr)
            {
                vExtractScreeenShotData(eachLine);
            }
            else if ("wait" == commandStr)
            {
                vExtractWaitData(eachLine);
            }
            else
            {
                m_bIsParsingSuccess = false;
                DBG_MSG(("ERROR in line %lu: Command %s not found/handled",
                         m_lineNumber, commandStr.c_str()));
            }
        }
        else if (bStartsWithDirective(eachLine, directiveStr))
        {
            if ("DEFINE" == directiveStr)
            {
                vProcessDefineData(eachLine);
            }
            else if ("BLOCK_START" == directiveStr)
            {
                vProcessBlockStart(eachLine);
            }
            else if ("BLOCK_END" == directiveStr)
            {
                vProcessBlockEnd(eachLine);
            }
            else if ("CALL" == directiveStr)
            {
                vProcessCall(eachLine);
            }
            else
            {
                m_bIsParsingSuccess = false;
                DBG_MSG(("ERROR in line %lu: Directive %s not found/handled",
                         m_lineNumber, directiveStr.c_str()));
            }
        }
    }
}

bool CommandParser::bIsCommentLine(const std::string& line)
{
    const std::string COMMENT_INDICATOR = "//";
    return line.find(COMMENT_INDICATOR) == 0;
}

tCommandList CommandParser::GetInstructionList()
{
    return m_instructionList;
}

void CommandParser::vRemoveSpaces(std::string& str)
{
    str.erase(std::remove(str.begin(), str.end(), ' '), str.end());
}

bool CommandParser::bIsParsingSuccess()
{
    return m_bIsParsingSuccess;
}

bool CommandParser::bIsPressTypeDefined(const std::string& press)
{
    return m_HKPressStringToValue.find(press) != m_HKPressStringToValue.end();
}

bool CommandParser::bIsXCoordinateValid(int x_value)
{
    return x_value >= 0 && x_value <= MAX_X;
}

bool CommandParser::bIsYCoordinateValid(int y_value)
{
    return y_value >= 0 && y_value <= MAX_Y;
}

bool CommandParser::bIsValidCoordinate(int x_value, int y_value)
{
    return bIsXCoordinateValid(x_value) && bIsYCoordinateValid(y_value);
}

bool CommandParser::bStartsWithDirective(const std::string& line,
                                         std::string& directiveStr)
{
    bool bRetVal = false;
    for (std::set<std::string>::iterator iter = m_possibleDirectives.begin();
         iter != m_possibleDirectives.end(); ++iter)
    {
        string directive = *iter;
        directive += "("; // check for command + opening bracket
        size_t pos = line.find(*iter);

        if (pos == string::npos)
            continue;

        // command found, check if its in beginning of line
        if (0 == pos)
        {
            // -1 to compensate added '('
            directiveStr = line.substr(0, directive.length() - 1);
            bRetVal = true;
            break; // one line, one command
        }
        else
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Directive '%s' is not at "
                 "the beginning of the line",
                 m_lineNumber, directive.c_str()));
        }
    }

    return bRetVal;
}

void CommandParser::vProcessDefineData(const std::string& defineLine)
{
    const std::string DIRECTIVE = "DEFINE";
    vector<string> defineParams = GetParameters(defineLine, '=');
    size_t defineParamsSize = defineParams.size();

    if (2 == defineParamsSize)
    {
        m_defineKeyValue[defineParams[0]] = defineParams[1];
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Directive '%s' Wrong number of "
             "parameters",
             m_lineNumber, DIRECTIVE.c_str()));
    }
}

bool CommandParser::bIsPartOfDefine(const std::string& key)
{
    return m_defineKeyValue.find(key) != m_defineKeyValue.end();
}

bool CommandParser::bIsBlockAlreadyPresent(const std::string& blockName)
{
    return m_BlockNameAndCmdlistMap.find(blockName) !=
           m_BlockNameAndCmdlistMap.end();
}

void CommandParser::vProcessBlockStart(const std::string& directiveStr)
{
    const std::string DIRECTIVE = "BLOCK_START";

    vector<string> blockParams = GetParameters(directiveStr);
    size_t blockParamsSize = blockParams.size();
    if (1 == blockParamsSize)
    {
        m_bIsBlockStarted = true;
        m_currentBlockName = blockParams[0];
        m_blockCmdStart = m_instructionList.size();
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Directive '%s' Wrong number of "
             "parameters",
             m_lineNumber, DIRECTIVE.c_str()));
    }
}

//(long name, high ncss, old code)
//(deep nested block, old code)
void CommandParser::vProcessBlockEnd( //!OCLINT
    const std::string& directiveStr)
{ //!OCLINT
    const std::string DIRECTIVE = "BLOCK_END";

    if (m_bIsBlockStarted)
    { //!OCLINT(deep nested block, old code)
        vector<string> blockEndParams = GetParameters(directiveStr);
        size_t blockEndParamsSize = blockEndParams.size();

        if (1 == blockEndParamsSize)
        {
            if (m_currentBlockName == blockEndParams[0])
            {
                m_bIsBlockStarted = false;
                m_blockCmdEnd = m_instructionList.size();

                if (m_blockCmdEnd > m_blockCmdStart)
                {
                    if (false == bIsBlockAlreadyPresent(m_currentBlockName))
                    {
                        m_BlockNameAndCmdlistMap[m_currentBlockName] =
                            SliceCommandList(m_blockCmdStart, m_blockCmdEnd);

                        m_currentBlockName = "";
                        m_blockCmdStart = (size_t)-1;
                        m_blockCmdEnd = (size_t)-1;
                    }
                    else
                    {
                        m_bIsParsingSuccess = false;
                        DBG_MSG(
                            ("ERROR in input file, line: %lu, Re-definition of "
                             "the block %s",
                             m_lineNumber, m_currentBlockName.c_str()));
                    }
                }
            }
            else
            {
                m_bIsParsingSuccess = false;
                DBG_MSG(
                    ("ERROR in input file, line: %lu, Directive '%s'"
                     "BLOCK_END received without BLOCK_START"
                     "current started block = %s, block end received for %s",
                     m_lineNumber, DIRECTIVE.c_str(),
                     m_currentBlockName.c_str(), blockEndParams[0].c_str()));
            }
        }
        else
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Directive '%s' Wrong number "
                 "of parameters",
                 m_lineNumber, DIRECTIVE.c_str()));
        }
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(("ERROR in line %lu: BLOCK_END received without BLOCK_START",
                 m_lineNumber));
    }
}

tCommandList CommandParser::SliceCommandList(size_t startIdx, size_t endPos)
{
    tCommandList retVec;

    if (bIsValidCommandIndices(startIdx, endPos))
    {
        for (size_t i = startIdx; i < endPos; ++i)
        {
            retVec.push_back(m_instructionList[i]);
        }

        tCommandList::iterator startIter = m_instructionList.begin();
        advance(startIter, startIdx);
        tCommandList::iterator endIter = startIter;
        advance(endIter, endPos - startIdx);

        m_instructionList.erase(startIter, endIter);
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(("ERROR in line %lu: Invalid index for slicing the vector",
                 m_lineNumber));
    }

    return retVec;
}

bool CommandParser::bIsValidCommandIndices(size_t startIdx, size_t endPos)
{
    return startIdx != MAX_SIZE && startIdx < m_instructionList.size() &&
           endPos != MAX_SIZE && endPos <= m_instructionList.size() &&
           startIdx <= endPos;
}

void CommandParser::vProcessCall( //!OCLINT
    const std::string& directiveStr)
{
    const std::string DIRECTIVE = "CALL";

    vector<string> callParams = GetParameters(directiveStr);
    size_t callParamsSize = callParams.size();
    if (2 == callParamsSize)
    {
        if (false == m_bIsBlockStarted)
        {
            if (true == bIsBlockAlreadyPresent(callParams[0]))
            {
                std::shared_ptr<CallData> pCallData(new CallData());
                pCallData->m_count = StringToNumber<int>(callParams[1]);
                pCallData->m_callCommandList =
                    m_BlockNameAndCmdlistMap[callParams[0]];

                tCommand command_;
                command_.first = DIRECTIVE;
                command_.second = pCallData;

                m_instructionList.push_back(command_);
            }
            else
            {
                m_bIsParsingSuccess = false;
                DBG_MSG(
                    ("ERROR in input file, line: %lu, Directive '%s' CALL on "
                     "empty/un-declared block is not allowed",
                     m_lineNumber, DIRECTIVE.c_str()));
            }
        }
        else
        {
            m_bIsParsingSuccess = false;
            DBG_MSG(
                ("ERROR in input file, line: %lu, Directive '%s' CALL is used "
                 "within the BLOCK",
                 m_lineNumber, DIRECTIVE.c_str()));
        }
    }
    else
    {
        m_bIsParsingSuccess = false;
        DBG_MSG(
            ("ERROR in input file, line: %lu, Directive '%s' Wrong number of "
             "parameters",
             m_lineNumber, DIRECTIVE.c_str()));
    }
}

void CommandParser::vDestroyCommand(tCommand&)
{
    // shared_ptr is automatically deleted
}

void CommandParser::vDestroyCommandList(tCommandList& commandList)
{
    for (tCommandList::iterator itCmd = commandList.begin();
         itCmd != commandList.end(); ++itCmd)
    {
        vDestroyCommand(*itCmd);
    }
}
