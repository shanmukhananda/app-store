
/*
* NiceTouch commands:
* . touch(x, y); e.g. touch(10, 20); sends touch command at screen co-ordinate
* x=10, y=20
* . wait(time in milliseconds); e.g. wait(2000); waits for 2 second before
* executing next command
* . hardkey(key, presstype); e.g. hardkey(up, longpress); sends long press
* command on 'up' key
* . screenshot(path to file); e.g. screenshot(/fs/usb0/1.bmp); creates and
* stores the screenshot in the specified path
* . DEFINE(TOP=123) to give names to values
* . BLOCK_START(NAME), BLOCK_END(NAME); e.g Do define a named block of commands
* . CALL(NAME, COUNT); e.g. CALL(NAME, 10); Calls the named block with the
* specified count. Its a looping construct
* . (proposal)demo(lat, long); e.g. demo(12.34, 45.76); enables the demo mode,
* car will be placed at the specifed geo
*
* Description:
* One line one command
* */

#include <fstream>
#include <iostream>
#include <set>
#include <string>

#include "CommandParser.hpp"

using namespace std;

int main(int argc, char** argv)
{
    if (argc == 2)
    {
        CommandParser parser_(argv[1]);
        parser_.GetInstructionList();
        if (parser_.bIsParsingSuccess())
        {
            DBG_MSG(("Parsing is Successfull!"));
        }
        else
        {
            DBG_MSG(("ERROR: File contains no NiceTouch commands"));
        }
    }
    else
    {
        DBG_MSG(
            ("ERROR: Please enter \"NiceTouchParser.exe <filename>\" in "
             "command line"));
    }

    return 0;
}