project(nice-touch)

set(source_list
    CommandParser.hpp
    CommandParser.cpp
    main.cpp)

add_executable(${PROJECT_NAME} ${source_list})
