#pragma once

#include <assert.h>
#include <stdio.h>

#include <fstream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifdef _MSC_VER
#pragma warning(disable : 4127) // C4127 : conditional expression is constant //
                                // Note: warning disabled for whole compilation
                                // unit
#endif                          //_MSC_VER

#undef DBG_MSG
#define DBG_MSG(args) \
    do                \
    {                 \
        printf args;  \
        printf("\n"); \
    } while (false) // C4127 : conditional expression is constant

template <typename T>
std::string NumberToString(T Number)
{
    std::ostringstream ss;
    ss << Number;
    return ss.str();
}

template <typename T>
T StringToNumber(const std::string& Text)
{
    std::istringstream ss(Text);
    T result;
    ss >> result;
    if (ss.fail())
    {
        result = 0;
        assert(0);
    }

    return result;
}

class IReplayData
{
public:
    virtual ~IReplayData()
    {
    }
};

class TouchData : public IReplayData
{
public:
    TouchData() : m_eventType(0), m_x(0), m_y(0)
    {
    }
    ~TouchData()
    {
    }
    unsigned int m_eventType;
    int m_x;
    int m_y;
};

class HardKeyData : public IReplayData
{
public:
    HardKeyData() : m_pressType(0)
    {
    }
    ~HardKeyData()
    {
    }
    std::string m_hkey;
    int m_pressType;
};

class ScreenShotData : public IReplayData
{
public:
    ~ScreenShotData()
    {
    }
    std::string m_command;
    std::string m_path;
};

class WaitData : public IReplayData
{
public:
    WaitData() : m_timeMs(0)
    {
    }
    ~WaitData()
    {
    }
    unsigned int m_timeMs;
};

typedef std::pair<std::string, std::shared_ptr<IReplayData>> tCommand;
typedef std::vector<tCommand> tCommandList;

class CallData : public IReplayData
{
public:
    CallData() : m_count(0)
    {
    }
    ~CallData()
    {
    }
    tCommandList m_callCommandList;
    int m_count;
};

class CommandParser
{
public:
    explicit CommandParser(const std::string&);
    ~CommandParser();
    tCommandList GetInstructionList();
    bool bIsParsingSuccess();

private:
    void vCreatePossibleCommandList();
    void vCreatePossibleHKButtonList();
    void vCreatePossibleHKPressList();
    void vCreateCommandToCANHKMap();
    void vCreateHKPressStringToValueMap();
    void vCreatePossibleDirectives();
    bool bStartsWithCommand(const std::string&, std::string&);
    bool bStartsWithDirective(const std::string&, std::string&);
    std::vector<std::string>
    GetParameters(const std::string&, char DELIMITER = ',');
    void vExtractTouchData(const std::string&);
    void vExtractHardKeyData(const std::string&);
    void vExtractScreeenShotData(const std::string&);
    void vExtractWaitData(const std::string&);
    void vAddToCommandToHKMap(const std::string&, const std::string&);
    void vParseCommandFile();
    bool bIsCommentLine(const std::string&);
    void vRemoveSpaces(std::string&);
    bool bIsPressTypeDefined(const std::string&);
    bool bIsXCoordinateValid(int x);
    bool bIsYCoordinateValid(int y);
    bool bIsValidCoordinate(int x, int y);
    void vProcessDefineData(const std::string&);
    void vProcessBlockStart(const std::string&);
    void vProcessBlockEnd(const std::string&);
    void vProcessCall(const std::string&);
    bool bIsPartOfDefine(const std::string&);
    bool bIsBlockAlreadyPresent(const std::string&);
    tCommandList SliceCommandList(size_t, size_t);
    bool bIsValidCommandIndices(size_t, size_t);
    void vDestroyCommand(tCommand&);
    void vDestroyCommandList(tCommandList&);

    std::set<std::string> m_possibleCommands;
    std::set<std::string> m_possibleHKButtons;
    std::set<std::string> m_possibleHKPress;
    std::map<std::string, std::string> m_CommandToHK;
    std::map<std::string, int> m_HKPressStringToValue;
    tCommandList m_instructionList;
    std::set<std::string> m_possibleDirectives;
    std::map<std::string, std::string> m_defineKeyValue;
    std::map<std::string, tCommandList> m_BlockNameAndCmdlistMap;
    bool m_bIsBlockStarted;
    size_t m_blockCmdStart;
    size_t m_blockCmdEnd;
    std::string m_currentBlockName;
    const int MAX_X;
    const int MAX_Y;
    const size_t MAX_SIZE;
    std::ifstream m_CommandFile;
    unsigned long m_lineNumber;
    bool m_bIsParsingSuccess;
};
