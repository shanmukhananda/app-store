#include <jsoncpp/json/json.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/writer.h>

#include <fstream>
#include <iostream>

void output(const Json::Value& value)
{
    std::cout << value["hello"] << '\n';
    std::cout << value["number"] << '\n';
    std::cout << value["array"][0] << value["array"][1] << '\n';
    std::cout << value["object"]["hello"] << '\n';
}

int main()
{
    Json::Value fromScratch;
    Json::Value array;

    array.append("hello");
    array.append("world");

    fromScratch["hello"] = "world";
    fromScratch["number"] = 2;
    fromScratch["array"] = array;
    fromScratch["object"]["hello"] = "world";

    output(fromScratch);
    return 0;
}
