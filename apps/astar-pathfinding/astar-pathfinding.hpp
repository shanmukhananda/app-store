﻿#include <functional>
#include <iostream>
#include <queue>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

/*
A--------12----------B

Instance of CNode will store values
B and 12 for the node A
*/
template <typename TVertex, typename TWeight>
class CNode
{
public:
    CNode() : mVertex(), mWeight()
    {
    }
    CNode(const TVertex& v_, const TWeight& w_) : mVertex(v_), mWeight(w_)
    {
    }

    TVertex vertex() const
    {
        return mVertex;
    }

    TWeight weight() const
    {
        return mWeight;
    }

    bool operator<(const CNode& rhs) const
    {
        return (this->mVertex < rhs.vertex());
    }

    bool operator==(const CNode& rhs) const
    {
        return (this->mVertex == rhs.vertex());
    }

private:
    TVertex mVertex;
    TWeight mWeight;
};

namespace std
{
template <>
struct hash<pair<int, int>>
{
    size_t operator()(const pair<int, int>& k) const
    {
        return (hash<int>()(k.first) ^ hash<int>()(k.second));
    }
};

template <typename TVertex, typename TWeight>
struct hash<CNode<TVertex, TWeight>>
{
    size_t operator()(const CNode<TVertex, TWeight>& k) const
    {
        return (hash<int>()(k.vertex().first) ^ hash<int>()(k.vertex().second));
    }
};
}

struct WeightSorter
{
    template <typename TVertex, typename TWeight>
    bool operator()(const CNode<TVertex, TWeight>& n1,
                    const CNode<TVertex, TWeight>& n2) const
    {
        return (n1.weight() < n2.weight());
    }
};

template <typename TVertex, typename TWeight>
class CGraph
{
public:
    void
    addEdge(const TVertex& start, const TVertex& end, const TWeight& weight)
    {
        auto& adjList = mAdjList[start];
        adjList.insert({end, weight});
    }

    const auto& adjacentNodes(const TVertex& v)
    {
        return (mAdjList[v]);
    }

    TWeight cost(const TVertex& from, const TVertex& to)
    {
        TWeight ret = TWeight();
        const auto& adjNodes = mAdjList[from];

        for (const auto& item : adjNodes)
        {
            if (to == item.vertex())
            {
                ret = item.weight();
                break;
            }
        }

        return ret;
    }

private:
    /*
    A--------12----------B
    |
    |
    15
    |
    |
    C

    Map's Key item will store "A"
    Instance of CAdjacentEdge will store values
    "B" and 12
    "C" and 15
    for node "A"
    */

    std::unordered_map<TVertex, std::unordered_set<CNode<TVertex, TWeight>>>
        mAdjList;
};

/*
   @brief: Calculates path using Astar
   @input:
            Graph
            Start
            End
            Heuristic function
   @return: pair(path, cost>
*/
template <typename TVertex, typename TWeight>
std::pair<std::unordered_map<TVertex, TVertex>, TWeight>
AStar_Path(CGraph<TVertex, TWeight> graph, const TVertex& start,
           const TVertex& end,
           std::function<TWeight(const TVertex&, const TVertex&)> heuristic)
{
    std::priority_queue<CNode<TVertex, TWeight>,
                        std::vector<CNode<TVertex, TWeight>>, WeightSorter>
        open_items; // open set
    CNode<TVertex, TWeight> start_(start, TWeight());
    open_items.push(start_);

    std::unordered_map<TVertex, TVertex> cameFrom; // path info
    cameFrom[start] = start;
    std::unordered_map<TVertex, TWeight> closed_items; // closed set
    closed_items[start] = TWeight();

    while (false == open_items.empty())
    {
        const auto current = open_items.top();
        open_items.pop();

        if (start != end)
        {
            const auto& adjNodes = graph.adjacentNodes(current.vertex());
            for (const auto& next : adjNodes)
            {
                TWeight newCost = closed_items[current.vertex()] +
                                  graph.cost(current.vertex(), next.vertex());

                if ( // new neighbour is not previously evaluated
                    !closed_items.count(next.vertex()) ||
                    // new cost is less than previously calculated cost
                    newCost < closed_items[next.vertex()])
                {
                    closed_items[next.vertex()] = newCost;
                    TWeight newWeight = newCost + heuristic(next.vertex(), end);
                    CNode<TVertex, TWeight> updatedNext(next.vertex(),
                                                        newWeight);
                    open_items.push(updatedNext);
                    cameFrom[next.vertex()] = current.vertex();
                }
            }
        }
        else
        {
            break;
        }
    }

    return {cameFrom, closed_items[end]};
}

template <typename TVertex, typename TWeight>
void printPath(
    const std::pair<std::unordered_map<TVertex, TVertex>, TWeight> result,
    const TVertex& start, const TVertex& end)
{
    std::stack<TVertex> pathTrace;
    pathTrace.push(end);
    const auto& cameFrom = result.first;
    auto p = cameFrom.at(end);
    while (p != start)
    {
        pathTrace.push(p);
        p = cameFrom.at(p);
    }

    pathTrace.push(start);

    while (false == pathTrace.empty())
    {
        const auto item = pathTrace.top();
        pathTrace.pop();

        std::cout << "(" << item.first << "," << item.second << ")";

        if (false == pathTrace.empty())
        {
            std::cout << "\n=>\n";
        }
    }

    std::cout << "\nCost = " << result.second << "\n";
}
