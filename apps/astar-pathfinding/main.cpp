#include <cmath>
#include "astar-pathfinding.hpp"

void testAstar_1()
{
    CGraph<std::pair<int, int>, double> graph;

    graph.addEdge(std::make_pair(0, 0), std::make_pair(1, 0), 10);
    graph.addEdge(std::make_pair(0, 0), std::make_pair(0, 1), 10);
    graph.addEdge(std::make_pair(0, 0), std::make_pair(1, 1), 14);

    graph.addEdge(std::make_pair(0, 1), std::make_pair(1, 1), 10);
    graph.addEdge(std::make_pair(0, 1), std::make_pair(0, 2), 10);

    graph.addEdge(std::make_pair(0, 2), std::make_pair(1, 2), 10);

    graph.addEdge(std::make_pair(1, 0), std::make_pair(1, 1), 10);

    graph.addEdge(std::make_pair(1, 1), std::make_pair(1, 2), 10);

    const auto start = std::make_pair(0, 0);
    const auto end = std::make_pair(1, 2);

    auto heuristic = [](const std::pair<int, int>& point1,
                        const std::pair<int, int>& point2) -> double {
        const auto delta_x = point1.first - point2.first;
        const auto delta_y = point1.second - point2.second;

        return sqrt((delta_x * delta_x) + (delta_y * delta_y));
    };

    const auto result =
        AStar_Path<std::pair<int, int>, double>(graph, start, end, heuristic);

    printPath<std::pair<int, int>, double>(result, start, end);
}

int main()
{
    testAstar_1();
    return 0;
}
