#include "ta/CTraceConfig.hpp"
#include "ta/CTraceParser.hpp"

#include <chrono>
#include <exception>

void terminator()
{
    std::cerr << "Unhandled exception\n";
}

int main(int argc, char** argv)
{
    std::set_terminate(terminator);

    std::cout << "Starting " << argv[0] << "\n";

    if (argc == 2)
    {
        NTraceAnalyser::CTraceConfig cfg_(argc, argv);
        NTraceAnalyser::CTraceParser parser(cfg_);
        std::chrono::steady_clock::time_point begin =
            std::chrono::steady_clock::now();
        std::cout << "\nParsing...\n";
        parser.parse();
        std::cout << "\nGenerating Report...\n";
        parser.generateReport();
        std::chrono::steady_clock::time_point end =
            std::chrono::steady_clock::now();
        auto timetaken_min =
            std::chrono::duration_cast<std::chrono::seconds>(end - begin)
                .count();

        std::cout << "Time taken = " << timetaken_min << " seconds\n";
    }
    else
    {
        std::cout << "Invalid Parameters\n";
    }

    std::cout << "Exiting " << argv[0] << "\n";

    return 0;
}