#include "CTraceParser.hpp"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <fstream>
#include <sstream>

namespace NTraceAnalyser
{
CTraceParser::CTraceParser(const ITraceConfig& cfg)
    : m_cfg(cfg), m_scopeBoundaryCount(0), m_reporter(cfg)
{
}

void CTraceParser::parse()
{
    const auto& files = m_cfg.input().files();

    for (const auto& item : files)
    {
        std::cout << "\r\n"
                  << "Current File = " << item << std::endl;
        parseFile(item);
    }
}

void CTraceParser::parseFile(const std::string& filename)
{
    ParseOption fileExt = getFileExtension(filename);
    if (ParseOption::INVALID != fileExt)
    {
        std::ifstream iFile(filename.c_str(), std::ios::in);

        if (iFile.good())
        {
            std::string currentLine;
            while (std::getline(iFile, currentLine))
            {
                if (ParseOption::TRC == fileExt)
                {
                    processTRCLine(currentLine);
                }
                else
                {
                    parseTXTLine(currentLine);
                }
            }
        }
        else
        {
            std::cerr << "Cannot open file " << filename << "\n";
        }
    }
}

void CTraceParser::processTRCLine(const std::string& currentLine)
{
    if (m_cfg.advanced().identifier().scopeIdBoundary() == currentLine)
    {
        ++m_scopeBoundaryCount;
    }
    if (1 == m_scopeBoundaryCount &&
        m_cfg.advanced().identifier().scopeIdBoundary() != currentLine)
    {
        parseTRCScopeInfo(currentLine);
    }
    else if (m_scopeBoundaryCount == 2 &&
             m_cfg.advanced().identifier().scopeIdBoundary() != currentLine)
    {
        parseTRCLine(currentLine);
    }
}

void CTraceParser::parseTRCScopeInfo(const std::string& currLine)
{
    std::stringstream sstream_(currLine);

    const auto& processStartMarker =
        m_cfg.advanced().identifier().processListStartMarker();
    const char PROCESS_START = processStartMarker[0];

    const auto& trcDelimiter = m_cfg.advanced().identifier().trcDelimiter();
    const char TRC_DELIMITER = trcDelimiter[0];

    if (0 == currLine.find_first_of(PROCESS_START))
    {
        // process name info
        std::string processName;
        std::getline(sstream_, processName, TRC_DELIMITER);
        processName.erase(0, 1); // remove '['
        std::string processNameID;
        std::getline(sstream_, processNameID, TRC_DELIMITER);
        int pnid = stoi(processNameID);
        m_processname_id[pnid] = processName;
    }
    else
    {
        // scope name info
        std::string scopeName;
        std::getline(sstream_, scopeName, TRC_DELIMITER);
        std::string scopeNameID;
        std::getline(sstream_, scopeNameID, TRC_DELIMITER);
        int snid = stoi(scopeNameID);
        m_scopename_id[snid] = scopeName;
    }
}

ParseOption CTraceParser::getFileExtension(const std::string& filename)
{
    const std::string TRC_EXT = ".trc";
    const std::string TXT_EXT = ".txt";
    // both TRC and TXT file extension size is same.
    std::string parseOption =
        filename.substr(filename.length() - TRC_EXT.length());
    ParseOption option;

    if (parseOption == TRC_EXT)
    {
        option = ParseOption::TRC;
    }
    else if (parseOption == TXT_EXT)
    {
        option = ParseOption::TXT;
    }
    else
    {
        option = ParseOption::INVALID;
        std::cerr << "Invalid File extension encountred " << filename << "\n";
    }

    return option;
}

void CTraceParser::parseTRCLine(const std::string& currentLine)
{
    const auto& trcDelim = m_cfg.advanced().identifier().trcDelimiter();
    std::vector<std::string> parsed(9);
    split(currentLine, trcDelim[0], parsed);

    if (parsed.size() == 9)
    {
        /* .trc file
            0 = Some number
            1 = Process ID Process Name ID
            2 = Thread ID
            3 = Scope ID
            4 = Timestamp
            5 = Log Receiver Timestamp {Time   Date}
            6 = Console
            7 = Log type
            8 = Message
        */

        CTraceMessage trc_msg;
        trc_msg.m_process = getTRCProcessName(parsed[1]);

        if (!parsed[2].empty())
        {
            trc_msg.m_thread = static_cast<unsigned int>(stoi(parsed[2]));
        }

        trc_msg.m_scope = getTRCScopeName(parsed[3]);

        if (!parsed[4].empty())
        {
            trc_msg.m_timestamp = stod(parsed[4]);
        }

        trc_msg.m_receiverTime = getLogReceiverTime(parsed[5]);
        trc_msg.m_console = parsed[6];
        trc_msg.m_logtype = parsed[7];
        trc_msg.m_message = parsed[8];

        if (false == isIgnored(trc_msg))
        {
            m_reporter.updateReport(trc_msg);
        }
    }
    else
    {
        std::cout << "Ignored Line : " << currentLine << "\n";
    }
}

void CTraceParser::parseTXTLine(const std::string& currentLine)
{
    const auto& trcDelim = m_cfg.advanced().identifier().trcDelimiter();
    std::vector<std::string> parsed;
    parsed.reserve(8);
    split(currentLine, trcDelim[0], parsed);

    if (parsed.size() == 8)
    {
        /* .txt File
            0 = Log type
            1 = Timestamp
            2 = Log Receiver Timestamp {Time   Date}
            3 = Thread ID
            4 = Process Name/ID
            5 = Scope Name/ID
            6 = Console
            7 = Message
        */

        CTraceMessage trc_msg;
        trc_msg.m_logtype = parsed[0];
        if (!parsed[1].empty())
        {
            trc_msg.m_timestamp = stod(parsed[1]);
        }

        trc_msg.m_receiverTime = getLogReceiverTime(parsed[2]);
        if (!parsed[3].empty())
        {
            trc_msg.m_thread = static_cast<unsigned int>(
                strtoul(parsed[3].c_str(), nullptr, 16));
        }

        trc_msg.m_process = parsed[4];
        trc_msg.m_scope = parsed[5];
        trc_msg.m_console = parsed[6];
        trc_msg.m_message = parsed[7];

        if (false == isIgnored(trc_msg))
        {
            m_reporter.updateReport(trc_msg);
        }
    }
    else
    {
        std::cout << "Ignored Line : " << currentLine << "\n";
    }
}

CLoggerReceiverTime //!OCLINT
    CTraceParser::getLogReceiverTime(const std::string& /*time*/)
{
    return CLoggerReceiverTime();

    // // premature return as below code takes time
    // // currently CLoggerReceiverTime is not used by other parts of the code
    // const auto& dateDelim = m_cfg.advanced().identifier().dateDelimiter();
    // char DATE_DELIM = dateDelim[0];

    // const auto& timeDelim = m_cfg.advanced().identifier().timeDelimiter();
    // char TIME_DELIM = timeDelim[0];

    // const auto& DATE_TIME_SEP =
    // m_cfg.advanced().identifier().dateTimeSeperator();

    // /* Time Date
    // 0 = Time
    // 1 = Date
    // */
    // std::vector<std::string> time_date;
    // time_date.reserve(2);
    // split(time, DATE_TIME_SEP, time_date);
    // assert(time_date.size() == 2);

    // /* Time
    // 0 = Hour
    // 1 = Minute
    // 2 = Second
    // 3 = Millisecond
    // */
    // std::vector<std::string> time_vec;
    // time_vec.reserve(4);
    // split(time_date[0], TIME_DELIM, time_vec);
    // assert(time_vec.size() == 4);

    // /* Date
    // 0 = Year
    // 1 = Month
    // 2 = Date
    // */
    // std::vector<std::string> date_vec;
    // date_vec.reserve(3);
    // split(time_date[1], DATE_DELIM, date_vec);
    // assert(time_vec.size() == 3);

    // CTime logtime;
    // logtime.m_hour = stoi(time_vec[0]);
    // logtime.m_minute = stoi(time_vec[1]);
    // logtime.m_second = stoi(time_vec[2]);
    // logtime.m_millisecond = stoi(time_vec[3]);

    // CDate logdate;
    // logdate.m_year = stoi(date_vec[0]);
    // logdate.m_month = stoi(date_vec[1]);
    // logdate.m_date = stoi(date_vec[2]);

    // return CLoggerReceiverTime(logtime, logdate);
}

std::string CTraceParser::getTRCProcessName(const std::string& pname)
{
    const auto& pname_id_delim =
        m_cfg.advanced().identifier().processIdSeperator();
    char PID_DELIM = pname_id_delim[0];

    /* ProcessName ID
    0 = Process ID
    1 = Process Name ID
    */
    std::vector<std::string> pname_id;
    pname_id.reserve(2);
    split(pname, PID_DELIM, pname_id);
    assert(pname_id.size() == 2);

    const auto& pid = pname_id[0];
    // const auto& pnid = pname_id[1];
    int pid_i = stoi(pid);
    const auto& processName = m_processname_id[pid_i];

    std::string ret{pid};
    if (!processName.empty())
    {
        ret = processName;
    }

    return ret;
}

std::string CTraceParser::getTRCScopeName(const std::string& scopeid)
{
    std::string ret{scopeid};
    int sid_i = stoi(scopeid);
    const auto& scopename = m_scopename_id[sid_i];

    if (!scopename.empty())
    {
        ret = scopename;
    }

    return ret;
}

void CTraceParser::generateReport()
{
    m_reporter.generateReport();
}

void CTraceParser::split(const std::string& str_, const char delim,
                         std::vector<std::string>& output)
{
    std::stringstream sstream_(str_);
    std::string item;
    while (std::getline(sstream_, item, delim))
    {
        output.push_back(item);
    }
}

void CTraceParser::split(const std::string& str_, const std::string& delim,
                         std::vector<std::string>& output)
{
    std::size_t startidx = 0;

    while (startidx < str_.size()) //!OCLINT(inverted logic)
    {
        std::size_t findpos = str_.find(delim, startidx);
        if (findpos == std::string::npos)
        {
            output.push_back(str_.substr(startidx));
            startidx = str_.size();
        }
        else
        {
            std::size_t len = findpos - startidx;
            output.push_back(str_.substr(startidx, len));
            startidx += delim.size() + findpos;
        }
    }
}

bool CTraceParser::isIgnored(const CTraceMessage& trace)
{
    return isProcessIgnored(trace.m_process) && isScopeIgnored(trace.m_scope) &&
           isConsoleIgnored(trace.m_console) &&
           isLogTypeIgnored(trace.m_logtype) &&
           isMessageIgnored(trace.m_message) &&
           isTimeStampIgnored(trace.m_timestamp);
}

bool CTraceParser::isProcessIgnored(const std::string& process)
{
    const auto& processIgnores = m_cfg.input().ignores().process();

    return (processIgnores.find(process) != processIgnores.end());
}

bool CTraceParser::isScopeIgnored(const std::string& scope)
{
    const auto& scopeIgnores = m_cfg.input().ignores().scope();

    return (scopeIgnores.find(scope) != scopeIgnores.end());
}

bool CTraceParser::isConsoleIgnored(const std::string& console)
{
    const auto& consoleIgnores = m_cfg.input().ignores().console();

    return (consoleIgnores.find(console) != consoleIgnores.end());
}

bool CTraceParser::isLogTypeIgnored(const std::string& logtype)
{
    const auto& logtypeIgnores = m_cfg.input().ignores().logtype();

    return (logtypeIgnores.find(logtype) != logtypeIgnores.end());
}

bool CTraceParser::isMessageIgnored(const std::string& message)
{
    return isPartOfBeginsWithList(message) && isPartOfContainsList(message) &&
           isPartOfEndsWithList(message) && isPartOfContainsAtList(message);
}

bool CTraceParser::isTimeStampIgnored(const double& timestamp)
{
    bool ret{false};

    const auto& timeIgnores = m_cfg.input().ignores().timestamp();

    for (const auto& item : timeIgnores)
    {
        const auto& start = item.first;
        const auto& end = item.second;
        if (timestamp >= start && timestamp <= end)
        {
            ret = true;
            break;
        }
    }

    return ret;
}

bool CTraceParser::isBeginsWith(const std::string& mainstr,
                                const std::string& substr)
{
    return mainstr.find(substr) == 0;
}

bool CTraceParser::isEndsWith(const std::string& mainstr,
                              const std::string& substr)
{
    return (mainstr.size() >= substr.size()) &&
           (mainstr.compare(mainstr.size() - substr.size(), substr.size(),
                            substr) == 0);
}

bool CTraceParser::isContains(const std::string& mainstr,
                              const std::string& substr)
{
    return mainstr.find(substr) != std::string::npos;
}

bool CTraceParser::isContainsAt(const std::string& mainstr,
                                const std::string& substr, std::size_t pos)
{
    return mainstr.find(substr) == pos;
}

bool CTraceParser::isPartOfBeginsWithList(const std::string& message)
{
    bool ret{false};

    const auto& beginsWithList = m_cfg.input().ignores().message().beginsWith();

    for (const auto& item : beginsWithList)
    {
        if (isBeginsWith(message, item))
        {
            ret = true;
            break;
        }
    }

    return ret;
}

bool CTraceParser::isPartOfContainsList(const std::string& message)
{
    bool ret{false};

    const auto& containsWithList = m_cfg.input().ignores().message().contains();

    for (const auto& item : containsWithList)
    {
        if (isContains(message, item))
        {
            ret = true;
            break;
        }
    }

    return ret;
}

bool CTraceParser::isPartOfEndsWithList(const std::string& message)
{
    bool ret{false};

    const auto& endsWithList = m_cfg.input().ignores().message().endsWith();

    for (const auto& item : endsWithList)
    {
        if (isEndsWith(message, item))
        {
            ret = true;
            break;
        }
    }

    return ret;
}

bool CTraceParser::isPartOfContainsAtList(const std::string& message)
{
    bool ret{false};

    const auto& containsAtList = m_cfg.input().ignores().message().containsAt();
    for (const auto& item : containsAtList)
    {
        if (isContainsAt(message, item.first,
                         static_cast<std::size_t>(item.second)))
        {
            ret = true;
            break;
        }
    }

    return ret;
}
} /*namespace NTraceAnalyser*/
