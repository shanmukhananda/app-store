#pragma once

#include <string>

namespace NTraceAnalyser
{
class CDate
{
public:
    CDate();
    CDate(int date, int month, int year);

    int m_date;
    int m_month;
    int m_year;
};

class CTime
{
public:
    CTime();
    CTime(int hour, int minute, int second, int millisecond);

    int m_hour;
    int m_minute;
    int m_second;
    int m_millisecond;
};

class CLoggerReceiverTime
{
public:
    CLoggerReceiverTime();
    CLoggerReceiverTime(const CTime& time, const CDate& date);

    CTime m_logtime;
    CDate m_logdate;
};

class CTraceMessage
{
public:
    CTraceMessage();
    CTraceMessage(const std::string& logtype, const double& timestamp,
                  const CLoggerReceiverTime& receiverTime,
                  const unsigned int thread, const std::string& process,
                  const std::string& scope, const std::string& console,
                  const std::string& message);

    std::string m_logtype;
    double m_timestamp;
    CLoggerReceiverTime m_receiverTime;
    unsigned int m_thread;
    std::string m_process;
    std::string m_scope;
    std::string m_console;
    std::string m_message;
};
} // namespace NTraceAnalyser
