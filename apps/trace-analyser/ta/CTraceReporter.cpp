#include "CTraceReporter.hpp"

#include <assert.h>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <sstream>

namespace NTraceAnalyser
{
CTraceReporter::CTraceReporter(const ITraceConfig& cfg)
    : m_cfg(cfg)
    , m_totalSize(0)
    , m_totalLines(0)
    , m_lostTraceCount(0)
    , m_firstLogTime(0.0)
    , m_lastLogTime(0.0)
{
}

void CTraceReporter::updateReport(const CTraceMessage& msg)
{
    ++m_totalLines;
    const auto size = msg.m_message.size();
    m_totalSize += size;
    m_processTbl[msg.m_process] += size;
    m_scopeTbl[msg.m_scope] += size;
    m_consoleTbl[msg.m_console] += size;
    m_logtypeTbl[msg.m_logtype] += size;

    auto& topscopetbl = m_topperTbl[msg.m_process];
    topscopetbl[msg.m_scope] += size;

    auto groupname = getProcessGroup(msg.m_process);
    m_groupTbl[groupname] += size;

    processMessage(msg.m_message);
    processTimeStamp(msg);
}

void CTraceReporter::generateReport()
{
    generateProcessReport();
    generateScopeReport();
    generateConsoleReport();
    generateLogTypeReport();
    generateToppersReport();
    generateSummary();
}

void CTraceReporter::processMessage(const std::string& msg)
{
    const auto& LOST_TRACE =
        m_cfg.advanced().identifier().lostTraceIdentifier();
    std::size_t pos = msg.rfind(LOST_TRACE);

    if (std::string::npos != pos)
    {
        std::string lostCountStr = msg.substr(0, pos);
        uint64_t lostCount(0);
        lostCount = stoul(lostCountStr);
        m_lostTraceCount += lostCount;
    }
}

void CTraceReporter::processTimeStamp(const CTraceMessage& data)
{
    if (m_firstLogTime <= 0.0000001f && isValidTimeConsole(data.m_console))
    {
        m_firstLogTime = data.m_timestamp;
    }

    m_lastLogTime = data.m_timestamp;
}

std::string CTraceReporter::getProcessGroup(const std::string& process)
{
    std::string retval;
    const auto& pgroup = m_cfg.advanced().processGroup();

    for (const auto& item : pgroup)
    {
        if (item.second.find(process) != item.second.end())
        {
            retval = item.first;
            break;
        }
    }

    return retval;
}

void CTraceReporter::logtable(const std::string& filename,
                              const std::map<std::string, uint64_t>& tbl,
                              const std::string& header)
{
    std::ofstream oFile;
    if (openFile(filename, oFile))
    {
        oFile << header << "\n";

        std::vector<std::pair<std::string, uint64_t>> vec_;
        vec_.reserve(tbl.size());

        for (const auto& item : tbl)
        {
            vec_.push_back(item);
        }

        std::sort(vec_.begin(), vec_.end(),
                  [](auto& lhs, auto& rhs) { return lhs.second > rhs.second; });

        for (const auto& item : vec_)
        {
            auto key = item.first;

            if (key.empty())
            {
                key = m_cfg.advanced().identifier().unknown();
            }

            auto percent_ = percent(item.second, m_totalSize);

            const auto& DELIM = m_cfg.advanced().identifier().columnDelimiter();
            oFile << key << DELIM << percent_ << "\n";
        }
    }
}

void CTraceReporter::logTopTable(const std::string& filename,
                                 const std::string& header, int topcount)
{
    std::ofstream oFile;
    if (openFile(filename, oFile))
    {
        oFile << header << "\n";

        std::string lastProcessName;

        for (const auto& item : m_topperTbl)
        {
            const auto& currentProcessName = item.first;
            std::vector<std::pair<std::string, uint64_t>> vec_;
            vec_.reserve(item.second.size());

            for (const auto& scope : item.second)
            {
                vec_.push_back(scope);
            }

            std::sort(vec_.begin(), vec_.end(), [](auto& lhs_, auto& rhs_) {
                return lhs_.second > rhs_.second;
            });

            assert(topcount >= 0);
            std::size_t u_topcount = static_cast<std::size_t>(topcount);
            for (std::size_t i = 0; i < u_topcount && i < vec_.size(); ++i)
            {
                const auto& scopename = vec_[i].first;
                const auto& scopesize = vec_[i].second;
                const auto& total_process_size =
                    m_processTbl[currentProcessName];
                auto percent_ = percent(scopesize, total_process_size);
                const auto& delim =
                    m_cfg.advanced().identifier().columnDelimiter();

                std::string pNameToPrint;
                if (lastProcessName != currentProcessName)
                {
                    pNameToPrint = currentProcessName;
                }

                oFile << pNameToPrint << delim << scopename << delim << percent_
                      << "\n";
                lastProcessName = currentProcessName;
            }
        }
    }
}

void CTraceReporter::generateProcessReport()
{
    const auto& processFile = m_cfg.advanced().processFile().first;
    const auto& processHeader = m_cfg.advanced().processFile().second;

    std::string pheader;
    for (const auto& item : processHeader)
    {
        pheader.append(item);
        pheader.append(m_cfg.advanced().identifier().columnDelimiter());
    }

    logtable(processFile, m_processTbl, pheader);
}

void CTraceReporter::generateScopeReport()
{
    const auto& scopeFile = m_cfg.advanced().scopeFile().first;
    const auto& scopeHeader = m_cfg.advanced().scopeFile().second;

    std::string sheader;
    for (const auto& item : scopeHeader)
    {
        sheader.append(item);
        sheader.append(m_cfg.advanced().identifier().columnDelimiter());
    }
    logtable(scopeFile, m_scopeTbl, sheader);
}

void CTraceReporter::generateConsoleReport()
{
    const auto& consoleFile = m_cfg.advanced().consoleFile().first;
    const auto& consoleHeader = m_cfg.advanced().consoleFile().second;

    std::string cheader;
    for (const auto& item : consoleHeader)
    {
        cheader.append(item);
        cheader.append(m_cfg.advanced().identifier().columnDelimiter());
    }
    logtable(consoleFile, m_consoleTbl, cheader);
}

void CTraceReporter::generateLogTypeReport()
{
    const auto& logtypeFile = m_cfg.advanced().logTypeFile().first;
    const auto& logtypeHeader = m_cfg.advanced().logTypeFile().second;

    std::string lheader;
    for (const auto& item : logtypeHeader)
    {
        lheader.append(item);
        lheader.append(m_cfg.advanced().identifier().columnDelimiter());
    }
    logtable(logtypeFile, m_logtypeTbl, lheader);
}

void CTraceReporter::generateToppersReport()
{
    const auto& topFile = m_cfg.advanced().toppersFile().first;
    const auto& topFileHeader = m_cfg.advanced().toppersFile().second;

    std::string theader;
    for (const auto& item : topFileHeader)
    {
        theader.append(item);
        theader.append(m_cfg.advanced().identifier().columnDelimiter());
    }

    int topcount = m_cfg.advanced().numbersOfToppers();

    logTopTable(topFile, theader, topcount);
}

void CTraceReporter::generateSummary()
{
    const auto& filename = m_cfg.advanced().summaryFile();
    const auto& delim = m_cfg.advanced().identifier().columnDelimiter();

    std::string groupSummay{"\nGroupSummay\n"};
    std::ofstream oFile;
    if (openFile(filename, oFile))
    {
        for (const auto& item : m_groupTbl)
        {
            if (item.first.empty())
            {
                groupSummay += m_cfg.advanced().identifier().unknown();
            }
            else
            {
                groupSummay += item.first;
            }
            groupSummay += delim;
            groupSummay += percent(item.second, m_totalSize);
            groupSummay += "\n";
        }

        oFile << "Total Lines" << delim << m_totalLines << "\n";
        oFile << "Total Size" << delim << m_totalSize << "\n";
        oFile << "Total Lost Traces" << delim << m_lostTraceCount << "\n";
        oFile << "Log data rate" << delim << getDataRateInBytesPerSecond()
              << " bps" << delim << getDataRateInBytesPerSecond() / 1024.0
              << " kbps" << delim
              << getDataRateInBytesPerSecond() / (1024.0 * 1024.0) << " mbps\n";
        oFile << groupSummay;
    }
}

bool CTraceReporter::openFile(const std::string& file, std::ofstream& oFile)
{
    bool ret{false};
    auto filename = m_cfg.input().resultDirectory() + "/" + file;
    oFile.open(filename.c_str(), std::ios::out);
    oFile << std::fixed << std::showpoint;
    oFile << std::setprecision(2);

    if (oFile.good())
    {
        ret = true;
    }
    else
    {
        std::cerr << "Cannot open file " << filename << "\n";
    }

    return ret;
}

std::string
CTraceReporter::percent(const uint64_t& numerator, const uint64_t& denominator)
{
    auto percent_ = static_cast<long double>(numerator) / denominator;
    percent_ *= 100.0f;
    std::ostringstream out;
    out << std::fixed << std::setprecision(2) << percent_;
    return out.str();
}

bool CTraceReporter::isValidTimeConsole(const std::string& console)
{
    const auto& validTimeConsoles =
        m_cfg.advanced().identifier().firstValidTimeConsoles();

    return (validTimeConsoles.find(console) != validTimeConsoles.end());
}

long double CTraceReporter::getDataRateInBytesPerSecond()
{
    long double ret = 0;
    double time = m_lastLogTime - m_firstLogTime;
    if (time >= 0.0000001)
    {
        ret = static_cast<long double>(m_totalSize) / time;
    }

    return ret;
}
} /*namespace NTraceAnalyser*/
