#pragma once

#include "CTraceConfig.hpp"
#include "CTraceTypes.hpp"

#include <map>
#include <string>

namespace NTraceAnalyser
{
class CTraceReporter
{
public:
    explicit CTraceReporter(const ITraceConfig& cfg);
    void updateReport(const CTraceMessage&);
    void generateReport();

private:
    void processMessage(const std::string& msg);
    void processTimeStamp(const CTraceMessage&);
    std::string getProcessGroup(const std::string& process);
    void logtable(const std::string& filename,
                  const std::map<std::string, uint64_t>& tbl,
                  const std::string& header);
    void logTopTable(const std::string& filename, const std::string& header,
                     int topcount);
    void generateProcessReport();
    void generateScopeReport();
    void generateConsoleReport();
    void generateLogTypeReport();
    void generateToppersReport();
    void generateSummary();
    bool openFile(const std::string& file, std::ofstream& oFile);
    std::string percent(const uint64_t& numerator, const uint64_t& denominator);
    bool isValidTimeConsole(const std::string& console);
    long double getDataRateInBytesPerSecond();

    const ITraceConfig& m_cfg;
    uint64_t m_totalSize;
    uint64_t m_totalLines;
    uint64_t m_lostTraceCount;
    double m_firstLogTime;
    double m_lastLogTime;
    std::map<std::string, uint64_t> m_processTbl; // Map(ProcessName, Size)
    std::map<std::string, uint64_t> m_scopeTbl;   // Map(ScopeName, Size)
    std::map<std::string, uint64_t> m_consoleTbl; // Map(Console, Size)
    std::map<std::string, uint64_t> m_logtypeTbl; // Map(LogType, Size)
    std::map<std::string, uint64_t> m_groupTbl;   // Map(ProcessGroup, Size)
    std::map<std::string, std::map<std::string, uint64_t>>
        m_topperTbl; // Map(ProcessName, Map(ScopeName, Size))

#ifdef UNIT_TEST
    friend class CTraceReporterTest;
#endif // UNIT_TEST
};
} // namespace NTraceAnalyser
