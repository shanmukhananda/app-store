#pragma once

#include <jsoncpp/json/json.h>

#include <iostream>
#include <map>
#include <set>
#include <string>

namespace NTraceAnalyser
{
/******************************************************************************/
class IConfig
{
public:
    virtual ~IConfig()
    {
    }
    virtual void initialize() = 0;
};

class CInputConfig;
class CAdvancedConfig;

class ITraceConfig : public IConfig
{
public:
    virtual ~ITraceConfig()
    {
    }
    virtual const CInputConfig& input() const = 0;
    virtual const CAdvancedConfig& advanced() const = 0;
};
/******************************************************************************/
class CIgnoreMessage : public IConfig
{
public:
    CIgnoreMessage();
    CIgnoreMessage(const std::set<std::string>& beginsWith,
                   const std::set<std::string>& contains,
                   const std::set<std::string>& endsWith,
                   const std::set<std::pair<std::string, int>>& containsAt);

    const std::set<std::string>& beginsWith() const;
    const std::set<std::string>& contains() const;
    const std::set<std::string>& endsWith() const;
    const std::set<std::pair<std::string, int>>& containsAt() const;
    void initialize() override;

    void beginsWith(const std::set<std::string>&);
    void contains(const std::set<std::string>&);
    void endsWith(const std::set<std::string>&);
    void containsAt(const std::set<std::pair<std::string, int>>&);

private:
    std::set<std::string> m_beginsWith;
    std::set<std::string> m_contains;
    std::set<std::string> m_endsWith;
    std::set<std::pair<std::string, int>> m_containsAt; // Pair(Message,
                                                        // Position)
};

class CInputIgnores : public IConfig
{
public:
    CInputIgnores();
    CInputIgnores(const std::set<std::string>& scope,
                  const std::set<std::string>& console,
                  const std::set<std::string>& process,
                  const std::set<std::string>& logtype,
                  const CIgnoreMessage& message,
                  const std::set<std::pair<double, double>>& timestamp);

    const std::set<std::string>& scope() const;
    const std::set<std::string>& console() const;
    const std::set<std::string>& process() const;
    const std::set<std::string>& logtype() const;
    const CIgnoreMessage& message() const;
    const std::set<std::pair<double, double>>& timestamp() const;
    void initialize() override;

    void scope(const std::set<std::string>&);
    void console(const std::set<std::string>&);
    void process(const std::set<std::string>&);
    void logtype(const std::set<std::string>&);
    void message(const CIgnoreMessage&);
    void timestamp(const std::set<std::pair<double, double>>&);

private:
    std::set<std::string> m_scope;
    std::set<std::string> m_console;
    std::set<std::string> m_process;
    std::set<std::string> m_logtype;
    CIgnoreMessage m_message;
    std::set<std::pair<double, double>> m_timestamp; // Pair(Starttime, Endtime)
};
/******************************************************************************/
class CInputConfig : public IConfig
{
public:
    CInputConfig();
    CInputConfig(const std::set<std::string>& files,
                 const std::string& resultDirectory,
                 const CInputIgnores& ignores);

    const std::set<std::string>& files() const;
    const std::string& resultDirectory() const;
    const CInputIgnores& ignores() const;
    void initialize() override;

    void files(const std::set<std::string>&);
    void resultDirectory(const std::string&);
    void ignores(const CInputIgnores&);

private:
    std::set<std::string> m_files;
    std::string m_resultDirectory;
    CInputIgnores m_ignores;
};
/******************************************************************************/
class CIdentifier : public IConfig
{
public:
    CIdentifier();
    CIdentifier(const std::string& columnDelimiter,
                const std::string& trcDelimiter,
                const std::string& processListStartMarker,
                const std::string& dateDelimiter,
                const std::string& timeDelimiter,
                const std::string& dateTimeSeperator,
                const std::string& processIdSeperator,
                const std::string& scopeIdBoundary,
                const std::set<std::string>& firstValidTimeConsoles,
                const std::string& lostTraceIdentifier,
                const std::string& unknown);

    const std::string& columnDelimiter() const;
    const std::string& trcDelimiter() const;
    const std::string& processListStartMarker() const;
    const std::string& dateDelimiter() const;
    const std::string& timeDelimiter() const;
    const std::string& dateTimeSeperator() const;
    const std::string& processIdSeperator() const;
    const std::string& scopeIdBoundary() const;
    const std::set<std::string>& firstValidTimeConsoles() const;
    const std::string& lostTraceIdentifier() const;
    const std::string& unknown() const;
    void initialize() override;

    void columnDelimiter(const std::string&);
    void trcDelimiter(const std::string&);
    void processListStartMarker(const std::string&);
    void dateDelimiter(const std::string&);
    void timeDelimiter(const std::string&);
    void dateTimeSeperator(const std::string&);
    void processIdSeperator(const std::string&);
    void scopeIdBoundary(const std::string&);
    void firstValidTimeConsoles(const std::set<std::string>&);
    void lostTraceIdentifier(const std::string&);
    void unknown(const std::string&);

private:
    std::string m_columnDelimiter;
    std::string m_trcDelimiter;
    std::string m_processListStartMarker;
    std::string m_dateDelimiter;
    std::string m_timeDelimiter;
    std::string m_dateTimeSeperator;
    std::string m_processIdSeperator;
    std::string m_scopeIdBoundary;
    std::set<std::string> m_firstValidTimeConsoles;
    std::string m_lostTraceIdentifier;
    std::string m_unknown;
};
class CAdvancedConfig : public IConfig
{
public:
    CAdvancedConfig();
    CAdvancedConfig(
        const CIdentifier& identifier, const int& numbersOfToppers,
        const std::map<std::string, std::set<std::string>>& processGroup,
        const std::pair<std::string, std::vector<std::string>>& processFile,
        const std::pair<std::string, std::vector<std::string>>& scopeFile,
        const std::pair<std::string, std::vector<std::string>>& consoleFile,
        const std::pair<std::string, std::vector<std::string>>& logTypeFile,
        const std::pair<std::string, std::vector<std::string>>& toppersFile,
        const std::string& summaryFile);

    const CIdentifier& identifier() const;
    const int& numbersOfToppers() const;
    const std::map<std::string, std::set<std::string>>& processGroup() const;
    const std::pair<std::string, std::vector<std::string>>& processFile() const;
    const std::pair<std::string, std::vector<std::string>>& scopeFile() const;
    const std::pair<std::string, std::vector<std::string>>& consoleFile() const;
    const std::pair<std::string, std::vector<std::string>>& logTypeFile() const;
    const std::pair<std::string, std::vector<std::string>>& toppersFile() const;
    const std::string& summaryFile() const;
    void initialize() override;

    void identifier(const CIdentifier&);
    void numbersOfToppers(const int&);
    void processGroup(const std::map<std::string, std::set<std::string>>&);
    void processFile(const std::pair<std::string, std::vector<std::string>>&);
    void scopeFile(const std::pair<std::string, std::vector<std::string>>&);
    void consoleFile(const std::pair<std::string, std::vector<std::string>>&);
    void logTypeFile(const std::pair<std::string, std::vector<std::string>>&);
    void toppersFile(const std::pair<std::string, std::vector<std::string>>&);
    void summaryFile(const std::string&);

private:
    CIdentifier m_identifier;
    int m_numbersOfToppers;
    std::map<std::string, std::set<std::string>>
        m_processGroup; // Map(GroupName, List(Group Items))
    std::pair<std::string, std::vector<std::string>>
        m_processFile; // Pair(filename, fileheaders)
    std::pair<std::string, std::vector<std::string>> m_scopeFile;
    std::pair<std::string, std::vector<std::string>> m_consoleFile;
    std::pair<std::string, std::vector<std::string>> m_logTypeFile;
    std::pair<std::string, std::vector<std::string>> m_toppersFile;
    std::string m_summaryFile;
};
/******************************************************************************/
class CTraceConfig : public ITraceConfig
{
public:
    CTraceConfig();
    CTraceConfig(int argc, char** argv);
    ~CTraceConfig();
    const CInputConfig& input() const override;
    const CAdvancedConfig& advanced() const override;
    void initialize() override;

private:
    void parse(int argc, char** argv);
    void parseJSONConfigFile();
    void readInputConfig(const Json::Value&);
    CInputIgnores readInputIgnores(const Json::Value&);
    CIgnoreMessage readIgnoredMessages(const Json::Value&);
    void readAdvancedConfig(const Json::Value&);
    CIdentifier readIdentifies(const Json::Value&);
    std::pair<std::string, std::vector<std::string>>
    readFilenameAndHeader(const Json::Value&);
    std::map<std::string, std::set<std::string>>
    readProcessGroup(const Json::Value&);

    CInputConfig m_input;
    CAdvancedConfig m_advanced;
    std::string m_configFile;
};
/******************************************************************************/
} // namespace NTraceAnalyser
