#include "CTraceTypes.hpp"

namespace NTraceAnalyser
{
CLoggerReceiverTime::CLoggerReceiverTime()
{
}
CLoggerReceiverTime::CLoggerReceiverTime(const CTime& time, const CDate& date)
    : m_logtime(time), m_logdate(date)
{
}

CTraceMessage::CTraceMessage()
{
    m_thread = 0;
    m_timestamp = 0.0;
}

CTraceMessage::CTraceMessage(
    const std::string& logtype, const double& timestamp,
    const CLoggerReceiverTime& receiverTime, const unsigned int thread,
    const std::string& process, const std::string& scope,
    const std::string& console, const std::string& message)
    : m_logtype(logtype)
    , m_timestamp(timestamp)
    , m_receiverTime(receiverTime)
    , m_thread(thread)
    , m_process(process)
    , m_scope(scope)
    , m_console(console)
    , m_message(message)
{
}

CDate::CDate() : m_date(0), m_month(0), m_year(0)
{
}
CDate::CDate(int date, int month, int year)
    : m_date(date), m_month(month), m_year(year)
{
}
CTime::CTime() : m_hour(0), m_minute(0), m_second(0), m_millisecond(0)
{
}
CTime::CTime(int hour, int minute, int second, int millisecond)
    : m_hour(hour)
    , m_minute(minute)
    , m_second(second)
    , m_millisecond(millisecond)
{
}
} /*namespace NTraceAnalyser*/
