#include "CTraceConfig.hpp"

#include <fstream>

namespace NTraceAnalyser
{
CTraceConfig::CTraceConfig()
{
    initialize();
}

CTraceConfig::CTraceConfig(int argc, char** argv)
{
    parse(argc, argv);
    initialize();
}

CTraceConfig::~CTraceConfig()
{
}

const CInputConfig& CTraceConfig::input() const
{
    return m_input;
}

const CAdvancedConfig& CTraceConfig::advanced() const
{
    return m_advanced;
}

void CTraceConfig::parse(int argc, char** argv)
{
    const std::string CONFIG = "--config=";

    for (int i = 1; i < argc; ++i)
    {
        std::string currentArgument(argv[i]);

        if (std::string::npos != currentArgument.find(CONFIG))
        {
            m_configFile = currentArgument.substr(CONFIG.length());
            std::cout << "Config file = " << m_configFile << std::endl;
        }
    }

    try
    {
        parseJSONConfigFile();
    }
    catch (const std::exception& e)
    {
        std::cerr << "Caught Exception while Json parsing e = " << e.what()
                  << std::endl;
        throw;
    }
}

void CTraceConfig::initialize()
{
    m_input.initialize();
    m_advanced.initialize();
}

void CTraceConfig::parseJSONConfigFile()
{
    Json::Value root;
    Json::Reader reader;
    std::ifstream configFile(m_configFile.c_str(), std::ifstream::binary);

    bool isParseSuccess = reader.parse(configFile, root, false);

    if (isParseSuccess)
    {
        const auto& inputConfig = root["input"];
        readInputConfig(inputConfig);

        const auto& advancedConfig = root["advanced"];
        readAdvancedConfig(advancedConfig);
    }
    else
    {
        std::cerr << "JSON parsing error in config file " << m_configFile
                  << " ERROR = " << reader.getFormattedErrorMessages()
                  << std::endl;
    }
}

void CTraceConfig::readInputConfig(const Json::Value& config)
{
    const auto& files = config["files"];

    std::set<std::string> filesList;
    for (const auto& item : files)
    {
        filesList.insert(item.asString());
    }

    auto resultDirectory = config["resultDirectory"].asString();

    const auto& ignores = config["ignores"];
    auto inputIgnores = readInputIgnores(ignores);

    CInputConfig input_cfg(filesList, resultDirectory, inputIgnores);
    m_input = input_cfg;
}

CInputIgnores //!OCLINT
    CTraceConfig::readInputIgnores(const Json::Value& config)
{
    const auto& scope = config["scope"];

    std::set<std::string> scopeIgnoreList;
    for (const auto& item : scope)
    {
        scopeIgnoreList.insert(item.asString());
    }

    const auto& console = config["console"];
    std::set<std::string> consoleIgnoreList;

    for (const auto& item : console)
    {
        consoleIgnoreList.insert(item.asString());
    }

    const auto& process = config["process"];
    std::set<std::string> processIgnoreList;

    for (const auto& item : process)
    {
        processIgnoreList.insert(item.asString());
    }

    const auto& logtype = config["logtype"];
    std::set<std::string> logtypeIgnoreList;

    for (const auto& item : logtype)
    {
        logtypeIgnoreList.insert(item.asString());
    }

    const auto& message = config["message"];

    auto messageIgnoreList = readIgnoredMessages(message);

    const auto& timestamp = config["timestamp"];

    std::set<std::pair<double, double>> timeStampList;
    for (const auto& item : timestamp)
    {
        auto start = item["start"].asDouble();
        auto end = item["end"].asDouble();

        std::pair<double, double> timestamp_pair(start, end);
        timeStampList.insert(timestamp_pair);
    }

    CInputIgnores input_ignores(scopeIgnoreList, consoleIgnoreList,
                                processIgnoreList, logtypeIgnoreList,
                                messageIgnoreList, timeStampList);

    return input_ignores;
}

CIgnoreMessage CTraceConfig::readIgnoredMessages(const Json::Value& config)
{
    const auto& beginsWith = config["beginsWith"];

    std::set<std::string> beginsWithList;
    for (const auto& item : beginsWith)
    {
        beginsWithList.insert(item.asString());
    }

    const auto& contains = config["contains"];

    std::set<std::string> containsWithList;
    for (const auto& item : contains)
    {
        containsWithList.insert(item.asString());
    }

    const auto& endsWith = config["endsWith"];
    std::set<std::string> endsWithList;

    for (const auto& item : endsWith)
    {
        endsWithList.insert(item.asString());
    }

    const auto& containsAt = config["containsAt"];

    std::set<std::pair<std::string, int>> containsAtList;
    for (const Json::Value& item : containsAt)
    {
        auto messageToIgnore = item["messageToIgnore"].asString();
        auto position = item["position"].asUInt();
        std::pair<std::string, int> msg_ignore(messageToIgnore, position);
        containsAtList.insert(msg_ignore);
    }

    CIgnoreMessage ignore_msg(beginsWithList, containsWithList, endsWithList,
                              containsAtList);

    return ignore_msg;
}

void CTraceConfig::readAdvancedConfig(const Json::Value& config)
{
    const auto& j_identifier = config["identifier"];
    auto identifier = readIdentifies(j_identifier);

    auto numbersOfToppers = config["numbersOfToppers"].asInt();

    const auto& processGroup = config["processGroup"];
    auto pgroup = readProcessGroup(processGroup);

    const auto& processFile = config["processFile"];
    auto pfile = readFilenameAndHeader(processFile);

    const auto& scopeFile = config["scopeFile"];
    auto sfile = readFilenameAndHeader(scopeFile);

    const auto& consoleFile = config["consoleFile"];
    auto cfile = readFilenameAndHeader(consoleFile);

    const auto& logTypeFile = config["logTypeFile"];
    auto lfile = readFilenameAndHeader(logTypeFile);

    const auto& toppersFile = config["toppersFile"];
    auto tfile = readFilenameAndHeader(toppersFile);

    const auto& summaryFile = config["summaryFile"];
    auto sufile = summaryFile["filename"].asString();

    CAdvancedConfig advance_cfg(identifier, numbersOfToppers, pgroup, pfile,
                                sfile, cfile, lfile, tfile, sufile);
    m_advanced = advance_cfg;
}

CIdentifier CTraceConfig::readIdentifies(const Json::Value& identifier)
{
    auto columnDelimiter = identifier["columnDelimiter"].asString();
    auto trcDelimiter = identifier["trcDelimiter"].asString();
    auto processListStartMarker = //!OCLINT
        identifier["processListStartMarker"].asString();
    auto dateDelimiter = identifier["dateDelimiter"].asString();
    auto timeDelimiter = identifier["timeDelimiter"].asString();
    auto dateTimeSeperator = identifier["dateTimeSeperator"].asString();
    auto processIdSeperator = identifier["processIdSeperator"].asString();
    auto scopeIdBoundary = identifier["scopeIdBoundary"].asString();

    const auto& validTimeConsoles = identifier["firstValidTimeConsoles"];
    std::set<std::string> firstValidTimeConsoles; //!OCLINT (Long var name)
    for (const auto& item : validTimeConsoles)
    {
        firstValidTimeConsoles.insert(item.asString());
    }

    auto lostTraceIdentifier = identifier["lostTraceIdentifier"].asString();
    auto unknown = identifier["unknown"].asString();

    CIdentifier identif(columnDelimiter, trcDelimiter, processListStartMarker,
                        dateDelimiter, timeDelimiter, dateTimeSeperator,
                        processIdSeperator, scopeIdBoundary,
                        firstValidTimeConsoles, lostTraceIdentifier, unknown);

    return identif;
}

std::pair<std::string, std::vector<std::string>>
CTraceConfig::readFilenameAndHeader(const Json::Value& config)
{
    auto filename = config["filename"].asString();

    const auto& j_headerList = config["headerList"];
    std::vector<std::string> headerList;

    for (const auto& item : j_headerList)
    {
        headerList.push_back(item.asString());
    }

    return std::make_pair(filename, headerList);
}

std::map<std::string, std::set<std::string>>
CTraceConfig::readProcessGroup(const Json::Value& processGroup)
{
    std::map<std::string, std::set<std::string>> pgroup;
    for (Json::ValueConstIterator jiter = processGroup.begin();
         jiter != processGroup.end(); ++jiter)
    {
        const auto& key = jiter.key();
        const auto& value = processGroup[key.asString()];
        std::set<std::string> groupcontents;
        for (const auto& item : value)
        {
            groupcontents.insert(item.asString());
        }
        pgroup.insert(std::make_pair(key.asString(), groupcontents));
    }

    return pgroup;
}

CIgnoreMessage::CIgnoreMessage(
    const std::set<std::string>& beginsWith,
    const std::set<std::string>& contains,
    const std::set<std::string>& endsWith,
    const std::set<std::pair<std::string, int>>& containsAt)
    : m_beginsWith(beginsWith)
    , m_contains(contains)
    , m_endsWith(endsWith)
    , m_containsAt(containsAt)
{
}

const std::set<std::string>& CIgnoreMessage::beginsWith() const
{
    return m_beginsWith;
}

const std::set<std::string>& CIgnoreMessage::contains() const
{
    return m_contains;
}

const std::set<std::string>& CIgnoreMessage::endsWith() const
{
    return m_endsWith;
}

const std::set<std::pair<std::string, int>>& CIgnoreMessage::containsAt() const
{
    return m_containsAt;
}

const std::set<std::pair<double, double>>& CInputIgnores::timestamp() const
{
    return m_timestamp;
}

CIgnoreMessage::CIgnoreMessage()
{
    initialize();
}

void CIgnoreMessage::initialize()
{
}

CInputIgnores::CInputIgnores(
    const std::set<std::string>& scope, const std::set<std::string>& console,
    const std::set<std::string>& process, const std::set<std::string>& logtype,
    const CIgnoreMessage& message,
    const std::set<std::pair<double, double>>& timestamp)
    : m_scope(scope)
    , m_console(console)
    , m_process(process)
    , m_logtype(logtype)
    , m_message(message)
    , m_timestamp(timestamp)
{
}

const std::set<std::string>& CInputIgnores::scope() const
{
    return m_scope;
}

const std::set<std::string>& CInputIgnores::console() const
{
    return m_console;
}

const std::set<std::string>& CInputIgnores::process() const
{
    return m_process;
}

const std::set<std::string>& CInputIgnores::logtype() const
{
    return m_logtype;
}

const CIgnoreMessage& CInputIgnores::message() const
{
    return m_message;
}

CInputIgnores::CInputIgnores()
{
    initialize();
}

void CInputIgnores::initialize()
{
}

CInputConfig::CInputConfig(const std::set<std::string>& files,
                           const std::string& resultDirectory,
                           const CInputIgnores& ignores)
    : m_files(files), m_resultDirectory(resultDirectory), m_ignores(ignores)
{
}

const std::set<std::string>& CInputConfig::files() const
{
    return m_files;
}

const std::string& CInputConfig::resultDirectory() const
{
    return m_resultDirectory;
}

const CInputIgnores& CInputConfig::ignores() const
{
    return m_ignores;
}

CInputConfig::CInputConfig()
{
    initialize();
}

void CInputConfig::initialize()
{
}

const std::string& CIdentifier::columnDelimiter() const
{
    return m_columnDelimiter;
}

const std::string& CIdentifier::trcDelimiter() const
{
    return m_trcDelimiter;
}

const std::string& CIdentifier::processListStartMarker() const
{
    return m_processListStartMarker;
}

const std::string& CIdentifier::dateDelimiter() const
{
    return m_dateDelimiter;
}

const std::string& CIdentifier::timeDelimiter() const
{
    return m_timeDelimiter;
}

const std::string& CIdentifier::dateTimeSeperator() const
{
    return m_dateTimeSeperator;
}

const std::string& CIdentifier::processIdSeperator() const
{
    return m_processIdSeperator;
}

const std::string& CIdentifier::scopeIdBoundary() const
{
    return m_scopeIdBoundary;
}

const std::set<std::string>& CIdentifier::firstValidTimeConsoles() const
{
    return m_firstValidTimeConsoles;
}

const std::string& CIdentifier::lostTraceIdentifier() const
{
    return m_lostTraceIdentifier;
}

const std::string& CIdentifier::unknown() const
{
    return m_unknown;
}

void CIdentifier::initialize()
{
}

CAdvancedConfig::CAdvancedConfig(
    const CIdentifier& identifier, const int& numbersOfToppers,
    const std::map<std::string, std::set<std::string>>& processGroup,
    const std::pair<std::string, std::vector<std::string>>& processFile,
    const std::pair<std::string, std::vector<std::string>>& scopeFile,
    const std::pair<std::string, std::vector<std::string>>& consoleFile,
    const std::pair<std::string, std::vector<std::string>>& logTypeFile,
    const std::pair<std::string, std::vector<std::string>>& toppersFile,
    const std::string& summaryFile)
    : m_identifier(identifier)
    , m_numbersOfToppers(numbersOfToppers)
    , m_processGroup(processGroup)
    , m_processFile(processFile)
    , m_scopeFile(scopeFile)
    , m_consoleFile(consoleFile)
    , m_logTypeFile(logTypeFile)
    , m_toppersFile(toppersFile)
    , m_summaryFile(summaryFile)
{
}

const CIdentifier& CAdvancedConfig::identifier() const
{
    return m_identifier;
}

const int& CAdvancedConfig::numbersOfToppers() const
{
    return m_numbersOfToppers;
}

const std::map<std::string, std::set<std::string>>&
CAdvancedConfig::processGroup() const
{
    return m_processGroup;
}

const std::pair<std::string, std::vector<std::string>>&
CAdvancedConfig::processFile() const
{
    return m_processFile;
}

const std::pair<std::string, std::vector<std::string>>&
CAdvancedConfig::scopeFile() const
{
    return m_scopeFile;
}

const std::pair<std::string, std::vector<std::string>>&
CAdvancedConfig::consoleFile() const
{
    return m_consoleFile;
}

const std::pair<std::string, std::vector<std::string>>&
CAdvancedConfig::logTypeFile() const
{
    return m_logTypeFile;
}

const std::pair<std::string, std::vector<std::string>>&
CAdvancedConfig::toppersFile() const
{
    return m_toppersFile;
}

CAdvancedConfig::CAdvancedConfig()
{
    initialize();
}

const std::string& CAdvancedConfig::summaryFile() const
{
    return m_summaryFile;
}

void CAdvancedConfig::initialize()
{
    m_numbersOfToppers = 10;
}

CIdentifier::CIdentifier()
{
    initialize();
}

CIdentifier::CIdentifier( //!OCLINT (too manay parameters)
    const std::string& columnDelimiter, const std::string& trcDelimiter,
    const std::string& processListStartMarker, //!OCLINT
    const std::string& dateDelimiter,          //!OCLINT (Long var name)
    const std::string& timeDelimiter,
    const std::string& dateTimeSeperator, //!OCLINT (Long var name)
    const std::string& processIdSeperator, const std::string& scopeIdBoundary,
    const std::set<std::string>& firstValidTimeConsoles, //!OCLINT (Long var
                                                         //! name)
    const std::string& lostTraceIdentifier, const std::string& unknown)
    : m_columnDelimiter(columnDelimiter)
    , m_trcDelimiter(trcDelimiter)
    , m_processListStartMarker(processListStartMarker)
    , m_dateDelimiter(dateDelimiter)
    , m_timeDelimiter(timeDelimiter)
    , m_dateTimeSeperator(dateTimeSeperator)
    , m_processIdSeperator(processIdSeperator)
    , m_scopeIdBoundary(scopeIdBoundary)
    , m_firstValidTimeConsoles(firstValidTimeConsoles)
    , m_lostTraceIdentifier(lostTraceIdentifier)
    , m_unknown(unknown)
{
}

void CIgnoreMessage::beginsWith(const std::set<std::string>& beginsWith)
{
    m_beginsWith = beginsWith;
}

void CIgnoreMessage::contains(const std::set<std::string>& contains)
{
    m_contains = contains;
}

void CIgnoreMessage::endsWith(const std::set<std::string>& endsWith)
{
    m_endsWith = endsWith;
}

void CIgnoreMessage::containsAt(
    const std::set<std::pair<std::string, int>>& containsAt)
{
    m_containsAt = containsAt;
}

void CInputIgnores::scope(const std::set<std::string>& scope)
{
    m_scope = scope;
}

void CInputIgnores::console(const std::set<std::string>& console)
{
    m_console = console;
}

void CInputIgnores::process(const std::set<std::string>& process)
{
    m_process = process;
}

void CInputIgnores::logtype(const std::set<std::string>& logtype)
{
    m_logtype = logtype;
}

void CInputIgnores::message(const CIgnoreMessage& message)
{
    m_message = message;
}

void CInputIgnores::timestamp(
    const std::set<std::pair<double, double>>& timestamp)
{
    m_timestamp = timestamp;
}

void CInputConfig::files(const std::set<std::string>& files)
{
    m_files = files;
}

void CInputConfig::resultDirectory(const std::string& resultDirectory)
{
    m_resultDirectory = resultDirectory;
}

void CInputConfig::ignores(const CInputIgnores& ignores)
{
    m_ignores = ignores;
}

void CIdentifier::columnDelimiter(const std::string& columnDelimiter)
{
    m_columnDelimiter = columnDelimiter;
}

void CIdentifier::trcDelimiter(const std::string& trcDelimiter)
{
    m_trcDelimiter = trcDelimiter;
}

void CIdentifier::processListStartMarker(
    const std::string& processListStartMarker) //!OCLINT(long var name)
{
    m_processListStartMarker = processListStartMarker;
}

void CIdentifier::dateDelimiter(const std::string& dateDelimiter)
{
    m_dateDelimiter = dateDelimiter;
}

void CIdentifier::timeDelimiter(const std::string& timeDelimiter)
{
    m_timeDelimiter = timeDelimiter;
}

void CIdentifier::dateTimeSeperator(const std::string& dateTimeSeperator)
{
    m_dateTimeSeperator = dateTimeSeperator;
}

void CIdentifier::processIdSeperator(const std::string& processIdSeperator)
{
    m_processIdSeperator = processIdSeperator;
}

void CIdentifier::scopeIdBoundary(const std::string& scopeIdBoundary)
{
    m_scopeIdBoundary = scopeIdBoundary;
}

void CIdentifier::firstValidTimeConsoles(
    const std::set<std::string>& firstValidTimeConsoles) //!OCLINT (Long var
                                                         //! name)
{
    m_firstValidTimeConsoles = firstValidTimeConsoles;
}

void CIdentifier::lostTraceIdentifier(const std::string& lostTraceIdentifier)
{
    m_lostTraceIdentifier = lostTraceIdentifier;
}

void CIdentifier::unknown(const std::string& unknown)
{
    m_unknown = unknown;
}

void CAdvancedConfig::identifier(const CIdentifier& identifier)
{
    m_identifier = identifier;
}

void CAdvancedConfig::numbersOfToppers(const int& numbersOfToppers)
{
    m_numbersOfToppers = numbersOfToppers;
}

void CAdvancedConfig::processGroup(
    const std::map<std::string, std::set<std::string>>& processGroup)
{
    m_processGroup = processGroup;
}

void CAdvancedConfig::processFile(
    const std::pair<std::string, std::vector<std::string>>& processFile)
{
    m_processFile = processFile;
}

void CAdvancedConfig::scopeFile(
    const std::pair<std::string, std::vector<std::string>>& scopeFile)
{
    m_scopeFile = scopeFile;
}

void CAdvancedConfig::consoleFile(
    const std::pair<std::string, std::vector<std::string>>& consoleFile)
{
    m_consoleFile = consoleFile;
}

void CAdvancedConfig::logTypeFile(
    const std::pair<std::string, std::vector<std::string>>& logTypeFile)
{
    m_logTypeFile = logTypeFile;
}

void CAdvancedConfig::toppersFile(
    const std::pair<std::string, std::vector<std::string>>& toppersFile)
{
    m_toppersFile = toppersFile;
}

void CAdvancedConfig::summaryFile(const std::string& summaryFile)
{
    m_summaryFile = summaryFile;
}
} /* namespace NTraceAnalyser */