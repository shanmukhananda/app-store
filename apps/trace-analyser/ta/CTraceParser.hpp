#pragma once

#include "CTraceConfig.hpp"
#include "CTraceReporter.hpp"
#include "CTraceTypes.hpp"

namespace NTraceAnalyser
{
enum class ParseOption
{
    INVALID,
    TXT,
    TRC
};

class CTraceParser
{
public:
    explicit CTraceParser(const ITraceConfig& cfg);
    void parse();
    void generateReport();

private:
    void
    split(const std::string&, const char, std::vector<std::string>& output);
    void split(const std::string&, const std::string&,
               std::vector<std::string>& output);
    bool isIgnored(const CTraceMessage&);
    bool isProcessIgnored(const std::string&);
    bool isScopeIgnored(const std::string&);
    bool isConsoleIgnored(const std::string&);
    bool isLogTypeIgnored(const std::string&);
    bool isMessageIgnored(const std::string&);
    bool isTimeStampIgnored(const double&);
    bool isBeginsWith(const std::string& mainstr, const std::string& substr);
    bool isEndsWith(const std::string& mainstr, const std::string& substr);
    bool isContains(const std::string& mainstr, const std::string& substr);
    bool isContainsAt(const std::string& mainstr, const std::string& substr,
                      std::size_t pos);
    bool isPartOfBeginsWithList(const std::string& message);
    bool isPartOfContainsList(const std::string& message);
    bool isPartOfEndsWithList(const std::string& message);
    bool isPartOfContainsAtList(const std::string& message);
    void parseFile(const std::string& filename);
    void processTRCLine(const std::string&);
    void parseTRCScopeInfo(const std::string&);
    ParseOption getFileExtension(const std::string&);
    void parseTRCLine(const std::string&);
    void parseTXTLine(const std::string&);
    CLoggerReceiverTime getLogReceiverTime(const std::string&);
    std::string getTRCProcessName(const std::string&);
    std::string getTRCScopeName(const std::string&);

    const ITraceConfig& m_cfg;
    int m_scopeBoundaryCount;
    CTraceReporter m_reporter;
    std::map<int, std::string> m_processname_id; // Map(ProcessNameID,
                                                 // ProcessName)
    std::map<int, std::string> m_scopename_id;   // Map(ScopeID, ScopeName)

#ifdef UNIT_TEST
    friend class CTraceParserTest;
#endif // UNIT_TEST
};
} // namespace NTraceAnalyser