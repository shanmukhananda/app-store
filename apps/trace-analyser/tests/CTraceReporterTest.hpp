#pragma once

#include <gmock/gmock.h>

namespace NTraceAnalyser
{
class CTraceReporterTest : public ::testing::Test
{
public:
    CTraceReporterTest();
    ~CTraceReporterTest() override;
    void SetUp() override;
    void TearDown() override;
    void testIsValidTimeConsole();
};
}
