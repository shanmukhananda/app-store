#pragma once

#include "ta/CTraceParser.hpp"

#include <gmock/gmock.h>

namespace NTraceAnalyser
{
class CTraceParserTest : public ::testing::Test
{
public:
    CTraceParserTest();
    ~CTraceParserTest() override;
    void SetUp() override;
    void TearDown() override;
    void testContains();
    void testStringBasedSplit();
    void testCharBasedSplit();
};

} // namespace NTraceAnalyser
