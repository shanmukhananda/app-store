#include "CTraceReporterTest.hpp"
#include "CMockTraceConfig.hpp"
#include "ta/CTraceReporter.hpp"

namespace NTraceAnalyser
{
CTraceReporterTest::CTraceReporterTest()
{
}

CTraceReporterTest::~CTraceReporterTest()
{
}

void CTraceReporterTest::SetUp()
{
}

void CTraceReporterTest::TearDown()
{
}

void CTraceReporterTest::testIsValidTimeConsole()
{
    using ::testing::ReturnRef;

    CMockTraceConfig mock_cfg;
    CIdentifier identifier_;
    std::set<std::string> consoles = {"T", "L"};
    identifier_.firstValidTimeConsoles(consoles);
    CAdvancedConfig adv_cfg;
    adv_cfg.identifier(identifier_);
    CTraceReporter reporter(mock_cfg);

    {
        EXPECT_CALL(mock_cfg, advanced()).WillOnce(ReturnRef(adv_cfg));

        std::string console = "L";
        EXPECT_EQ(true, //!OCLINT
                  reporter.isValidTimeConsole(console));
    }

    {
        EXPECT_CALL(mock_cfg, advanced()).WillOnce(ReturnRef(adv_cfg));

        std::string console = "M";
        EXPECT_EQ(false, //!OCLINT
                  reporter.isValidTimeConsole(console));
    }
}

TEST_F(CTraceReporterTest, //!OCLINT
       testIsValidTimeConsole)
{
    testIsValidTimeConsole();
}

} // namespace NTraceAnalyser
