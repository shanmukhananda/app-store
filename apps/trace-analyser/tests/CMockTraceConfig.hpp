#pragma once

#include "ta/CTraceConfig.hpp"

#include <gmock/gmock.h>

namespace NTraceAnalyser
{
class CMockTraceConfig : public ITraceConfig
{
public:
    MOCK_CONST_METHOD0(input, const CInputConfig&());
    MOCK_CONST_METHOD0(advanced, const CAdvancedConfig&());
    MOCK_METHOD0(initialize, void());
};
} // namespace NTraceAnalyser