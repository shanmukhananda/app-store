#include "CTraceParserTest.hpp"
#include "CMockTraceConfig.hpp"
#include "ta/CTraceParser.hpp"

namespace NTraceAnalyser
{
CTraceParserTest::CTraceParserTest()
{
}
CTraceParserTest::~CTraceParserTest()
{
}
void CTraceParserTest::SetUp()
{
}

void CTraceParserTest::TearDown()
{
}

void CTraceParserTest::testContains()
{
    CMockTraceConfig cfg_;
    CTraceParser parser(cfg_);
    EXPECT_EQ(true, //!OCLINT
              parser.isContains("TestString", "Str"));
    EXPECT_EQ(false, //!OCLINT
              parser.isContains("TestString", "Sstr"));
}

void CTraceParserTest::testStringBasedSplit() //!OCLINT (high ncss, old code)
{
    {
        CMockTraceConfig cfg_;
        CTraceParser parser(cfg_);
        std::vector<std::string> output;
        std::string delim{"   "}; // 3 space
        std::string mainstr{"abc   def"};
        parser.split(mainstr, delim, output);
        EXPECT_EQ(2, output.size()); //!OCLINT (empty if from gtest)
    }
    {
        CMockTraceConfig cfg_;
        CTraceParser parser(cfg_);
        std::vector<std::string> output;
        std::string delim{"   "}; // 3 space
        std::string mainstr = "abc  def";
        parser.split(mainstr, delim, output);
        EXPECT_EQ(1, output.size()); //!OCLINT (empty if from gtest)
    }
    {
        CMockTraceConfig cfg_;
        CTraceParser parser(cfg_);
        std::vector<std::string> output;
        std::string delim{"   "}; // 3 space
        std::string mainstr = "abc    def";
        parser.split(mainstr, delim, output);
        EXPECT_EQ(2, output.size()); //!OCLINT (empty if from gtest)
    }
    {
        CMockTraceConfig cfg_;
        CTraceParser parser(cfg_);
        std::vector<std::string> output;
        std::string delim{"   "}; // 3 space
        std::string mainstr = "abcdef";
        parser.split(mainstr, delim, output);
        EXPECT_EQ(1, output.size()); //!OCLINT (empty if from gtest)
    }
    {
        CMockTraceConfig cfg_;
        CTraceParser parser(cfg_);
        std::vector<std::string> output;
        std::string delim{"   "};             // 3 space
        std::string mainstr = "abc      def"; // 6 space
        parser.split(mainstr, delim, output);
        EXPECT_EQ(2, output.size()); //!OCLINT (empty if from gtest)
    }
}

void CTraceParserTest::testCharBasedSplit()
{
    {
        CMockTraceConfig cfg_;
        CTraceParser parser(cfg_);
        const char delim = ',';
        std::vector<std::string> output;
        std::string mainstr = "abc,def";
        parser.split(mainstr, delim, output);
        EXPECT_EQ(2, output.size()); //!OCLINT (empty if from gtest)
    }
    {
        CMockTraceConfig cfg;
        CTraceParser parser(cfg);
        const char delim = ',';
        std::vector<std::string> output;
        std::string mainstr = "abcdef";
        parser.split(mainstr, delim, output);
        EXPECT_EQ(1, output.size()); //!OCLINT (empty if from gtest)
    }
}

TEST_F(CTraceParserTest, testCharBasedSplit) //!OCLINT (static from gtest)
{
    testCharBasedSplit();
}

TEST_F(CTraceParserTest, testContains) //!OCLINT (static from gtest)
{
    testContains();
}

TEST_F(CTraceParserTest, testStringBasedSplit) //!OCLINT (from gtest)
{
    testStringBasedSplit();
}
} // namespace NTraceAnalyser
