#include <boost/filesystem.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace fs = boost::filesystem;

bool is_image(const fs::path& file)
{
    return file.extension() == ".jpg" || file.extension() == ".jpeg" ||
           file.extension() == ".png";
}

std::vector<cv::String> images(const std::string& folder)
{
    std::vector<fs::path> files;
    std::copy_if(fs::directory_iterator(folder), fs::directory_iterator(),
                 std::back_inserter(files),
                 [](const fs::path& file) { return is_image(file); });
    std::sort(files.begin(), files.end());
    std::vector<cv::String> output;
    std::for_each(files.begin(), files.end(), [&output](const fs::path& file) {
        output.push_back(file.generic_string());
    });
    return output;
}

void draw_point_cloud(const std::vector<cv::Mat>& Rs,
                      const std::vector<cv::Mat>& Ts, const cv::Matx33d& K,
                      const std::vector<cv::Mat>& points)
{
    cv::viz::Viz3d window("Coordinate Frame");
    window.setWindowSize(cv::Size(500, 500));
    window.setWindowPosition(cv::Point(150, 150));
    window.setBackgroundColor();

    std::vector<cv::Vec3f> point_cloud_est;
    for (std::size_t i = 0; i < points.size(); ++i)
        point_cloud_est.push_back(cv::Vec3f(points[i]));

    std::vector<cv::Affine3d> path;
    for (std::size_t i = 0; i < Rs.size(); ++i)
        path.push_back(cv::Affine3d(Rs[i], Ts[i]));

    if (point_cloud_est.size() > 0)
    {
        cv::viz::WCloud cloud_widget(point_cloud_est, cv::viz::Color::green());
        window.showWidget("point_cloud", cloud_widget);
    }

    if (path.size() > 0)
    {
        window.showWidget("cameras_frames_and_lines",
                          cv::viz::WTrajectory(path, cv::viz::WTrajectory::BOTH,
                                               0.1, cv::viz::Color::green()));
        window.showWidget("cameras_frustums",
                          cv::viz::WTrajectoryFrustums(
                              path, K, 0.1, cv::viz::Color::yellow()));
        window.setViewerPose(path[0]);
    }

    window.spin();
}

int main(int argc, char** argv)
{
    if (argc != 5)
        throw std::invalid_argument("Usage: sfm <image_folder> <f> <cx> <cy>");

    std::string folder = argv[1];
    float f = std::stof(argv[2]);
    float cx = std::stof(argv[3]);
    float cy = std::stof(argv[4]);

    std::vector<cv::String> images_paths = images(folder);
    cv::Matx33d K = cv::Matx33d(f, 0, cx, 0, f, cy, 0, 0, 1);
    std::vector<cv::Mat> Rs, Ts, points3d;
    cv::sfm::reconstruct(images_paths, Rs, Ts, K, points3d, true);
    draw_point_cloud(Rs, Ts, K, points3d);
    return 0;
}
