#include <exception>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

int main(int argc, char** argv)
{
    if (argc != 6)
        throw std::invalid_argument(
            "Invalid number of arguments; Usage: " + std::string(argv[0]) +
            " input_video.mp4 image.png output_video.mp4 0 0");

    std::string input_video_path{argv[1]};
    std::string image_to_insert_path{argv[2]};
    std::string output_video_path{argv[3]};
    std::string insert_pos_x{argv[4]};
    std::string insert_pos_y{argv[5]};

    cv::VideoCapture input_video(input_video_path);

    if (false == input_video.isOpened())
        throw std::runtime_error("Cannot open the video file " +
                                 input_video_path);

    cv::Mat image_to_insert = cv::imread(image_to_insert_path);
    if (nullptr == image_to_insert.data)
        throw std::runtime_error("Cannot open the image file " +
                                 image_to_insert_path);

    auto video_frame_width = input_video.get(CV_CAP_PROP_FRAME_WIDTH);
    auto video_frame_height = input_video.get(CV_CAP_PROP_FRAME_HEIGHT);

    auto fps = input_video.get(CV_CAP_PROP_FPS);
    auto output_video_size = cv::Size(static_cast<int>(video_frame_width),
                                      static_cast<int>(video_frame_height));
    cv::VideoWriter output_video(output_video_path,
                                 CV_FOURCC('M', 'J', 'P', 'G'), fps,
                                 output_video_size, true);

    if (false == output_video.isOpened())
        throw std::runtime_error("Cannot open the video file " +
                                 output_video_path);

    cv::Mat input_frame;
    while (input_video.read(input_frame))
    {
        cv::Mat output_frame = input_frame.clone();
        auto pos_x = std::stoi(insert_pos_x);
        auto pos_y = std::stoi(insert_pos_y);

        auto region_of_insert =
            cv::Rect(pos_x, pos_y, image_to_insert.cols, image_to_insert.rows);
        image_to_insert.copyTo(output_frame(region_of_insert));
        output_video << output_frame;
    }
}