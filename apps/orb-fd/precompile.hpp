#pragma once

/**
precompile.hpp : include file for standard system include files,
or project specific include files that are used frequently, but
are changed infrequently

precompile.hpp file must be the first include in every .cpp.
Its is OK if you put precompile.hpp inside your .hpp file,
if required, but its not mandatory
*/

// OpenCV
#include <opencv2/core/types.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// Boost
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// C, C++ Standard
#include <chrono>
#include <exception>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>