#include "precompile.hpp"

#include "config.hpp"

namespace fd
{
const int& orb::nfeatures() const
{
    return _nfeatures;
}

const float& orb::scaleFactor() const
{
    return _scaleFactor;
}

const int& orb::nlevels() const
{
    return _nlevels;
}

const int& orb::edgeThreshold() const
{
    return _edgeThreshold;
}

const int& orb::firstLevel() const
{
    return _firstLevel;
}

const int& orb::WTA_K() const
{
    return _WTA_K;
}

const int& orb::scoreType() const
{
    return _scoreType;
}

const int& orb::patchSize() const
{
    return _patchSize;
}

const int& orb::fastThreshold() const
{
    return _fastThreshold;
}

void orb::read_json(const boost::property_tree::ptree& root)
{
    _nfeatures = root.get<int>("nfeatures");
    _scaleFactor = root.get<float>("scaleFactor");
    _nlevels = root.get<int>("nlevels");
    _edgeThreshold = root.get<int>("edgeThreshold");
    _firstLevel = root.get<int>("firstLevel");
    _WTA_K = root.get<int>("WTA_K");

    {
        auto scoreType_ = root.get<std::string>("scoreType");
        if ("HARRIS_SCORE" == scoreType_)
            _scoreType = cv::ORB::HARRIS_SCORE;
        else if ("FAST_SCORE" == scoreType_)
            _scoreType = cv::ORB::FAST_SCORE;
        else
            throw std::invalid_argument("Invalid score value");
    }

    _patchSize = root.get<int>("patchSize");
    _fastThreshold = root.get<int>("fastThreshold");
}

const std::string& app_input::video_path() const
{
    return _video_path;
}

const std::string& app_input::image_path() const
{
    return _image_path;
}

const std::string& app_input::input_source() const
{
    return _input_source;
}

bool app_input::log_keypoints() const
{
    return _log_keypoints;
}

void app_input::read_json(const boost::property_tree::ptree& root)
{
    _video_path = root.get<std::string>("video_path");
}

const int& ksize::width() const
{
    return _width;
}

const int& ksize::height() const
{
    return _height;
}

void ksize::read_json(const boost::property_tree::ptree& root)
{
    _width = root.get<int>("width");
    _height = root.get<int>("height");
}

const ksize& gaussian_blur::ksize_cfg() const
{
    return _ksize;
}

const double& gaussian_blur::sigmaX() const
{
    return _sigmaX;
}

const double& gaussian_blur::sigmaY() const
{
    return _sigmaY;
}

const int& gaussian_blur::borderType() const
{
    return _borderType;
}

void gaussian_blur::convert_bordertype(const std::string& borderType_)
{
    if ("BORDER_CONSTANT" == borderType_)
        _borderType = cv::BORDER_CONSTANT;
    else if ("BORDER_REPLICATE" == borderType_)
        _borderType = cv::BORDER_REPLICATE;
    else if ("BORDER_REFLECT" == borderType_)
        _borderType = cv::BORDER_REFLECT;
    else if ("BORDER_WRAP" == borderType_)
        _borderType = cv::BORDER_WRAP;
    else if ("BORDER_REFLECT_101" == borderType_)
        _borderType = cv::BORDER_REFLECT_101;
    else if ("BORDER_TRANSPARENT" == borderType_)
        _borderType = cv::BORDER_TRANSPARENT;
    else if ("BORDER_REFLECT101" == borderType_)
        _borderType = cv::BORDER_REFLECT101;
    else if ("BORDER_DEFAULT" == borderType_)
        _borderType = cv::BORDER_DEFAULT;
    else if ("BORDER_ISOLATED" == borderType_)
        _borderType = cv::BORDER_ISOLATED;
    else
        throw std::invalid_argument("Invalid Border Type");
}

void gaussian_blur::read_json(const boost::property_tree::ptree& root)
{
    const auto& ksize_root = root.get_child("ksize");
    _ksize.read_json(ksize_root);

    _sigmaX = root.get<double>("sigmaX");
    _sigmaY = root.get<double>("sigmaY");
    auto borderType_ = root.get<std::string>("borderType");
    convert_bordertype(borderType_);
}

config::config(int argc, char** argv)
{
    parse_arguments(argc, argv);
}

void config::parse_arguments(int argc, char** argv)
{
    if (argc != 2)
        throw std::invalid_argument(
            "Invalid number of arguments; Usage: " + std::string(argv[0]) +
            " --config=config.json");

    const std::string CONFIG = "--config=";

    for (int i = 1; i < argc; ++i)
    {
        std::string currentArgument(argv[i]);
        if (std::string::npos != currentArgument.find(CONFIG))
        {
            _config_file = currentArgument.substr(CONFIG.length());
        }
    }

    read_json();
}

void config::read_json()
{
    boost::property_tree::ptree root;
    std::ifstream jsonFile(_config_file);
    boost::property_tree::read_json(jsonFile, root);
    const auto& orb_root = root.get_child("orb");
    _orb.read_json(orb_root);

    const auto& app_input_root = root.get_child("app_input");
    _app_input.read_json(app_input_root);

    const auto& gaussian_blur_root = root.get_child("gaussian_blur");
    _gaussian_blur.read_json(gaussian_blur_root);
}

const orb& config::orb_cfg() const
{
    return _orb;
}

const app_input& config::app_input_cfg() const
{
    return _app_input;
}

const gaussian_blur& config::gaussian_blur_cfg() const
{
    return _gaussian_blur;
}
} // namespace fd
