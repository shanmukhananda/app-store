#pragma once

#include "precompile.hpp"

namespace fd
{
class orb
{
public:
    const int& nfeatures() const;
    const float& scaleFactor() const;
    const int& nlevels() const;
    const int& edgeThreshold() const;
    const int& firstLevel() const;
    const int& WTA_K() const;
    const int& scoreType() const;
    const int& patchSize() const;
    const int& fastThreshold() const;
    void read_json(const boost::property_tree::ptree&);

private:
    int _nfeatures;
    float _scaleFactor;
    int _nlevels;
    int _edgeThreshold;
    int _firstLevel;
    int _WTA_K;
    int _scoreType;
    int _patchSize;
    int _fastThreshold;
};

class ksize
{
public:
    const int& width() const;
    const int& height() const;
    void read_json(const boost::property_tree::ptree&);

private:
    int _width;
    int _height;
};

class gaussian_blur
{
public:
    const ksize& ksize_cfg() const;
    const double& sigmaX() const;
    const double& sigmaY() const;
    const int& borderType() const;
    void read_json(const boost::property_tree::ptree&);

private:
    void convert_bordertype(const std::string&);
    ksize _ksize;
    double _sigmaX;
    double _sigmaY;
    int _borderType;
};

class app_input
{
public:
    const std::string& video_path() const;
    const std::string& image_path() const;
    const std::string& input_source() const;
    bool log_keypoints() const;
    void read_json(const boost::property_tree::ptree&);

private:
    std::string _video_path;
    std::string _image_path;
    std::string _input_source;
    bool _log_keypoints;
};

class config
{
public:
    config(int argc, char** argv);
    const orb& orb_cfg() const;
    const app_input& app_input_cfg() const;
    const gaussian_blur& gaussian_blur_cfg() const;

private:
    void read_json();
    void parse_arguments(int argc, char** argv);
    std::string _config_file;

    orb _orb;
    app_input _app_input;
    gaussian_blur _gaussian_blur;
};
} // namespace fd
