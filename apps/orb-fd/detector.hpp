#pragma once

#include "precompile.hpp"

#include "config.hpp"

namespace fd
{
class detector
{
public:
    explicit detector(const config&);
    void detect();

private:
    void log_keypoints(const std::vector<cv::KeyPoint>&);
    void orb_from_image(const cv::Mat&);
    void create_orb_feature_detector();
    void detect_from_video(const std::string&);
    void smoothen_image(const cv::Mat&, cv::Mat&);

    const config& _cfg;
    std::size_t _frame_count;
    cv::Ptr<cv::ORB> _feature_detector;
};
} // namespace fd
