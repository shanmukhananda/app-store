#include "precompile.hpp"

#include "config.hpp"
#include "detector.hpp"

int main(int argc, char** argv)
{
    std::cout << "Started " << argv[0] << std::endl;

    {
        fd::config cfg(argc, argv);
        fd::detector feature_detector(cfg);
        feature_detector.detect();
    }

    std::cout << "Exiting " << argv[0] << std::endl;
    return 0;
}
