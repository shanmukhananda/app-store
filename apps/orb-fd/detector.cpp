#include "precompile.hpp"

#include "detector.hpp"

namespace fd
{
detector::detector(const config& cfg) : _cfg(cfg), _frame_count(0)
{
    create_orb_feature_detector();
}

void detector::detect()
{
    const auto& input_source = _cfg.app_input_cfg().input_source();
    if (input_source == "video")
    {
        const auto& video = _cfg.app_input_cfg().video_path();
        detect_from_video(video);
    }
    else if (input_source == "image")
    {
        const auto& image = _cfg.app_input_cfg().image_path();
        auto img = cv::imread(image);

        if (nullptr == img.data)
            throw std::runtime_error("Unable to read input image");

        orb_from_image(img);
    }
    else
        throw std::invalid_argument("Invalid input_source " + input_source);
}

void detector::smoothen_image(const cv::Mat& input, cv::Mat& output)
{
    const auto& ksize_width = _cfg.gaussian_blur_cfg().ksize_cfg().width();
    const auto& ksize_height = _cfg.gaussian_blur_cfg().ksize_cfg().height();
    auto size = cv::Size(ksize_width, ksize_height);
    const auto& sigma_x = _cfg.gaussian_blur_cfg().sigmaX();
    const auto& sigma_y = _cfg.gaussian_blur_cfg().sigmaY();
    const auto& borderType = _cfg.gaussian_blur_cfg().borderType();

    cv::GaussianBlur(input, output, size, sigma_x, sigma_y, borderType);
}

void detector::log_keypoints(const std::vector<cv::KeyPoint>& key_points)
{
    std::string output_header{"#x, y, size, angle, response, octave, class_id"};

    std::string out_filename{"keypoints"};
    out_filename += std::to_string(_frame_count);

    std::ofstream ofile(out_filename + ".csv");
    ofile << output_header << std::endl;
    const std::string delimiter{", "};
    for (const auto& keypoint : key_points)
    {
        std::string keypoint_str;
        keypoint_str += std::to_string(keypoint.pt.x) + delimiter;
        keypoint_str += std::to_string(keypoint.pt.y) + delimiter;
        keypoint_str += std::to_string(keypoint.size) + delimiter;
        keypoint_str += std::to_string(keypoint.angle) + delimiter;
        keypoint_str += std::to_string(keypoint.response) + delimiter;
        keypoint_str += std::to_string(keypoint.octave) + delimiter;
        keypoint_str += std::to_string(keypoint.class_id);

        ofile << keypoint_str << std::endl;
    }
}

void detector::orb_from_image(const cv::Mat& input_image_)
{
    auto input_image = input_image_.clone();
    cv::cvtColor(input_image, input_image, CV_BGR2GRAY);
    // detect features
    std::vector<cv::KeyPoint> key_points;
    _feature_detector->detect(input_image, key_points);

    if (true == _cfg.app_input_cfg().log_keypoints())
    {
        log_keypoints(key_points);
    }
    // calculate feature descriptors
    cv::Mat smooth_image;
    smoothen_image(input_image, smooth_image);
    cv::Mat descriptors;
    _feature_detector->compute(smooth_image, key_points, descriptors);

    // display features
    cv::Mat output_image;
    cv::drawKeypoints(smooth_image, key_points, output_image,
                      cv::Scalar(0, 255, 0), cv::DrawMatchesFlags::DEFAULT);
    cv::imshow("ORB Features", output_image);
    cv::waitKey(30);
}

void detector::create_orb_feature_detector()
{
    const auto& nfeatures = _cfg.orb_cfg().nfeatures();
    const auto& scaleFactor = _cfg.orb_cfg().scaleFactor();
    const auto& nlevels = _cfg.orb_cfg().nlevels();
    const auto& edgeThreshold = _cfg.orb_cfg().edgeThreshold();
    const auto& firstLevel = _cfg.orb_cfg().firstLevel();
    const auto& WTA_K = _cfg.orb_cfg().WTA_K();
    const auto& scoreType = _cfg.orb_cfg().scoreType();
    const auto& patchSize = _cfg.orb_cfg().patchSize();
    const auto& fastThreshold = _cfg.orb_cfg().fastThreshold();

    _feature_detector =
        cv::ORB::create(nfeatures, scaleFactor, nlevels, edgeThreshold,
                        firstLevel, WTA_K, scoreType, patchSize, fastThreshold);
}

void detector::detect_from_video(const std::string& video)
{
    cv::VideoCapture video_capture(video);

    if (false == video_capture.isOpened())
        throw std::runtime_error("Cannot open the video file " + video);

    cv::Mat frame;
    while (video_capture.read(frame))
    {
        ++_frame_count;
        orb_from_image(frame);
    }
}
} // namespace fd
