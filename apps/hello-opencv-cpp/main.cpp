#include <exception>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

int main(int argc, char** argv)
{
    if (argc != 2)
        throw std::invalid_argument(
            "Invalid arguments; Usage: hello-opencv-cpp image-to-load");

    cv::Mat image;
    image = cv::imread(argv[1]);

    if (nullptr == image.data)
        throw std::runtime_error("Could not open or find the image");

    cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);
    cv::imshow("Display window", image);
    cv::waitKey(0);

    return 0;
}
