#pragma once

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

template <typename T>
class CConcurrentQueue
{
public:
    CConcurrentQueue();
    ~CConcurrentQueue();
    T dequeue();
    void enqueue(const T& item);
    void enqueue(T&& item);

private:
    std::queue<T> mQueue;
    std::mutex mMutex;
    std::condition_variable mCond;
};

template <typename T>
CConcurrentQueue<T>::CConcurrentQueue()
{
}

template <typename T>
CConcurrentQueue<T>::~CConcurrentQueue()
{
}

template <typename T>
T CConcurrentQueue<T>::dequeue()
{
    std::unique_lock<std::mutex> locker(mMutex);
    while (mQueue.empty())
    {
        mCond.wait(locker);
    }

    auto item{std::move(mQueue.front())};
    mQueue.pop();
    return item;
}

template <typename T>
void CConcurrentQueue<T>::enqueue(const T& item)
{
    std::unique_lock<std::mutex> locker(mMutex);
    mQueue.push(item);
    mCond.notify_one();
}

template <typename T>
void CConcurrentQueue<T>::enqueue(T&& item)
{
    std::unique_lock<std::mutex> locker(mMutex);
    mQueue.push(std::move(item));
    mCond.notify_one();
}
