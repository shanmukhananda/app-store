#include "Log.hpp"

int main()
{
    LOG_SCOPE;
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    return 0;
}
