#pragma once

#include "CConcurrentQueue.hpp"

#include <string.h>
#include <atomic>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>

#ifndef __FILENAME__
#ifdef _WIN32

#define __FILENAME__ \
    (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#else

#define __FILENAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#endif /*WIN32*/

#endif /*__FILENAME__*/

namespace nlog
{
const char LogScopeSeperator = '\t';

class Logger
{
public:
    static Logger& instance()
    {
        static Logger instance;
        return instance;
    }
    void logscope(std::string&& str)
    {
        mLogQ.enqueue(std::move(str));
    }

private:
    Logger() : mStop(false), mOutputFile("log")
    {
        std::ios_base::sync_with_stdio(false);

        if (mOutputFile.good())
        {
            std::clog.rdbuf(mOutputFile.rdbuf());
        }

        auto thread_{
            std::unique_ptr<std::thread>(new std::thread(&Logger::run, this))};
        mLoggerThread = std::move(thread_);
    }
    ~Logger()
    {
        std::clog << "Exiting Logger" << std::endl;
        std::clog.rdbuf(std::cout.rdbuf());
        mLoggerThread->detach();
    }

    void run()
    {
        while (false == mStop.load())
        {
            auto item{mLogQ.dequeue()};
            std::clog << item;
        }
    }
    std::atomic<bool> mStop;
    std::ofstream mOutputFile;
    CConcurrentQueue<std::string> mLogQ;
    std::unique_ptr<std::thread> mLoggerThread;
};

class LogScope
{
public:
    LogScope(const char* function, const char* file, std::thread::id tid,
             std::size_t line = 0)
        : m_function(function)
        , m_file(file)
        , m_tid(tid)
        , m_line(line)
        , mLogScope(false)
    {
        auto logScopeEnv = std::getenv("LOG_SCOPE");
        if (nullptr != logScopeEnv)
        {
            mLogScope = (strcmp(logScopeEnv, "enable") == 0);
        }

        if (mLogScope)
        {
            std::stringstream ss;
            ss << "[=>>]" << LogScopeSeperator << m_file << LogScopeSeperator
               << m_line << LogScopeSeperator << m_tid << LogScopeSeperator
               << m_function << '\n';
            print(ss.str());
        }
    }

    ~LogScope()
    {
        if (mLogScope)
        {
            std::stringstream ss;
            ss << "[<<=]" << LogScopeSeperator << m_file << LogScopeSeperator
               << m_line << LogScopeSeperator << m_tid << LogScopeSeperator
               << m_function << '\n';
            print(ss.str());
        }
    }

private:
    void print(std::string&& str)
    {
        Logger::instance().logscope(std::move(str));
    }

    const char* m_function;
    const char* m_file;
    std::thread::id m_tid;
    std::size_t m_line;
    bool mLogScope;
};
}

#ifndef LOG_SCOPE
#pragma GCC diagnostic ignored "-Wattributes"
#define LOG_SCOPE                                               \
    nlog::LogScope __attribute__((annotate("oclint:suppress"))) \
        _LOG_SCOPE_VAR_(__FUNCTION__, __FILENAME__,             \
                        std::this_thread::get_id(), __LINE__)

#endif // !LOG_SCOPE
