#include "Utility.hpp"

#include <assert.h>

QJsonObject Utility::JStringToJobject(const QString& jsonString)
{
    QJsonParseError jerror;
    QJsonObject jobj;
    QJsonDocument jdoc =
        QJsonDocument::fromJson(jsonString.toStdString().c_str(), &jerror);

    if (QJsonParseError::NoError == jerror.error)
    {
        jobj = jdoc.object();
    }
    else
    {
        qCritical("%s %s %s %s", "ERROR in parsing JSON, ErrorName = ",
                  jerror.errorString().toStdString().c_str(),
                  "Input String = ", jsonString.toStdString().c_str());
    }

    return jobj;
}
