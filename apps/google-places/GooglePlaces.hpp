#pragma once

#include <QMainWindow>

#include "NetworkManager.hpp"

namespace Ui
{
class GooglePlaces;
}

class GooglePlaces : public QMainWindow
{
    Q_OBJECT

public:
    explicit GooglePlaces(QWidget* parent = 0);
    ~GooglePlaces();

private slots:
    void on_lineEdit_Search_cursorPositionChanged(int /*arg1*/, int /*arg2*/);

    void on_listView_Autocomplete_clicked(const QModelIndex& index);

    void onLocationParsed(const location& loc);

    void on_pushButton_reversegeo_clicked();

private:
    Ui::GooglePlaces* ui;
};
