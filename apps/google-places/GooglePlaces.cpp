#include "GooglePlaces.hpp"
#include "NetworkManager.hpp"

#include "ui_GooglePlaces.h"

#include <QDoubleValidator>

GooglePlaces::GooglePlaces(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::GooglePlaces)
{
    NetworkManager::Instance(); // create instance of manager
    ui->setupUi(this);
    ui->listView_Autocomplete->setModel(
        NetworkManager::Instance()->pAutoCompleteModel());
    ui->listView_reversegeo->setModel(
        NetworkManager::Instance()->pReverseGeoModel());

    ui->lineEdit_Latitude_reversegeo->setValidator(
        new GeoValidator(-90.0, 90.0, 10));

    ui->lineEdit_Longitude_reversegeo->setValidator(
        new GeoValidator(-180.0, 180.0, 10));

    connect(NetworkManager::Instance(), SIGNAL(LocationParsed(const location&)),
            this, SLOT(onLocationParsed(const location&)));
}

GooglePlaces::~GooglePlaces()
{
    delete ui;
}

void GooglePlaces::on_lineEdit_Search_cursorPositionChanged(int /*arg1*/,
                                                            int /*arg2*/)
{
    NetworkManager::Instance()->vRequestAutoComplete(
        ui->lineEdit_Search->text());
}

void GooglePlaces::on_listView_Autocomplete_clicked(const QModelIndex& index)
{
    NetworkManager::Instance()->vRequestLocationDetails(index.row());
}

void GooglePlaces::onLocationParsed(const location& loc)
{
    ui->lineEdit_description->setText(loc.formatedAddress);
    ui->lineEdit_Latitude->setText(loc.latitude);
    ui->lineEdit_Longitude->setText(loc.longitude);
    ui->lineEdit_name->setText(loc.name);
    ui->lineEdit_vicinity->setText(loc.vicinity);
}

void GooglePlaces::on_pushButton_reversegeo_clicked()
{
    QString latitude = ui->lineEdit_Latitude_reversegeo->text();
    QString longitude = ui->lineEdit_Longitude_reversegeo->text();

    if (latitude.length() > 0 && longitude.length() > 0)
    {
        QString latLong = latitude + "," + longitude;
        NetworkManager::Instance()->vRequestReverseGeo(latLong);
    }
}
