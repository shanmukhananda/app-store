#pragma once

#include "Utility.hpp"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QStringListModel>
#include <QValidator>
#include <QVector>
#include <QtGlobal>

struct predictions
{
    QString description;
    QString place_id;
};

struct location
{
    QString formatedAddress;
    QString latitude;
    QString longitude;
    QString name;
    QString vicinity;
};

// http://stackoverflow.com/questions/19571033/allow-entry-in-qlineedit-only-within-range-of-qdoublevalidator
class GeoValidator : public QDoubleValidator
{
public:
    GeoValidator(double bottom, double top, int decimals, QObject* parent = 0)
        : QDoubleValidator(bottom, top, decimals, parent)
    {
    }

    QValidator::State validate(QString& s, int& /*i*/) const
    {
        if (s.isEmpty() || s == "-")
        {
            return QValidator::Intermediate;
        }

        QChar decimalPoint = locale().decimalPoint();

        if (s.indexOf(decimalPoint) != -1)
        {
            int charsAfterPoint = s.length() - s.indexOf(decimalPoint) - 1;

            if (charsAfterPoint > decimals())
            {
                return QValidator::Invalid;
            }
        }

        bool ok;
        double d = locale().toDouble(s, &ok);

        if (ok && d >= bottom() && d <= top())
        {
            return QValidator::Acceptable;
        }
        else
        {
            return QValidator::Invalid;
        }
    }
};

class NetworkManager : public QObject
{
    Q_OBJECT
public:
    enum eCurrentRequest
    {
        autoComplete,
        locationDetails,
        reverseGeo
    };

    static NetworkManager* Instance();
    QStringListModel* pAutoCompleteModel() const;
    void vRequestAutoComplete(QString input);
    void vRequestLocationDetails(int modelIndex);
    void vRequestReverseGeo(QString input);

    void vSetCurrentRequest(eCurrentRequest reqType);
    eCurrentRequest eGetCurrentRequest();
    QStringListModel* pReverseGeoModel() const;
    void setPReverseGeoModel(QStringListModel* pReverseGeoModel);

signals:
    void LocationParsed(const location&);

public slots:
    void replyFinished(QNetworkReply* reply);

private:
    void vParsePrediction(const QString& reply);
    void vParseLocation(const QString& reply);
    void vParseReverseGeo(const QString& reply);
    explicit NetworkManager(QObject* parent = 0);

    static NetworkManager* m_pInstance;
    QStringListModel* m_pAutoCompleteModel;
    QNetworkAccessManager* m_pNetworkAccessMgr;
    eCurrentRequest m_eCurrentRequest;
    QVector<predictions> m_aPredictions;
    QStringListModel* m_pReverseGeoModel;
};
