#include "GooglePlaces.hpp"

#include <QApplication>

int main(int argc, char* argv[])
{
    QApplication application(argc, argv);
    GooglePlaces places;
    places.show();

    return application.exec();
}
