#include "NetworkManager.hpp"

#include <assert.h>
#include <QDebug>

const QString GOOGLE_API_KEY = "AIzaSyDYWs8KQsNSG2QHZyoJE40Z3vd38pWpZh0";

NetworkManager* NetworkManager::m_pInstance = NULL; //!OCLINT (old code using
                                                    //! singleton)

NetworkManager::NetworkManager(QObject* parent) : QObject(parent)
{
    m_pNetworkAccessMgr = new QNetworkAccessManager();
    m_pAutoCompleteModel = new QStringListModel();
    m_pReverseGeoModel = new QStringListModel();

    connect(m_pNetworkAccessMgr, SIGNAL(finished(QNetworkReply*)), this,
            SLOT(replyFinished(QNetworkReply*)));
}

QStringListModel* NetworkManager::pReverseGeoModel() const
{
    return m_pReverseGeoModel;
}

void NetworkManager::setPReverseGeoModel(QStringListModel* pReverseGeoModel)
{
    m_pReverseGeoModel = pReverseGeoModel;
}

NetworkManager* NetworkManager::Instance()
{
    if (NULL == m_pInstance)
    {
        m_pInstance = new NetworkManager();
    }

    return m_pInstance;
}

QStringListModel* NetworkManager::pAutoCompleteModel() const
{
    return m_pAutoCompleteModel;
}

void NetworkManager::vRequestAutoComplete(QString input)
{
    m_eCurrentRequest = autoComplete;
    QString url = QString(
                      "https://maps.googleapis.com/maps/api/place/autocomplete/"
                      "json?language=en&input=%1&key=%2")
                      .arg(input)
                      .arg(GOOGLE_API_KEY);
    QNetworkRequest request;
    request.setUrl(url);
    m_pNetworkAccessMgr->get(request);
}

void NetworkManager::vRequestLocationDetails(int modelIndex)
{
    assert(modelIndex < m_aPredictions.size());
    m_eCurrentRequest = locationDetails;
    QString url = QString(
                      "https://maps.googleapis.com/maps/api/place/details/"
                      "json?reference=%1&key=%2")
                      .arg(m_aPredictions[modelIndex].place_id)
                      .arg(GOOGLE_API_KEY);
    QNetworkRequest request;
    request.setUrl(url);
    m_pNetworkAccessMgr->get(request);
}

void NetworkManager::vRequestReverseGeo(QString input)
{
    m_eCurrentRequest = reverseGeo;
    QString url = QString(
                      "https://maps.googleapis.com/maps/api/geocode/"
                      "json?latlng=%1&key=%2")
                      .arg(input)
                      .arg(GOOGLE_API_KEY);
    QNetworkRequest request;
    request.setUrl(url);
    m_pNetworkAccessMgr->get(request);
}

void NetworkManager::vSetCurrentRequest(NetworkManager::eCurrentRequest reqType)
{
    m_eCurrentRequest = reqType;
}

NetworkManager::eCurrentRequest NetworkManager::eGetCurrentRequest()
{
    return m_eCurrentRequest;
}

void NetworkManager::replyFinished(QNetworkReply* reply)
{
    QString data = reply->readAll();

    if (locationDetails == m_eCurrentRequest)
    {
        vParseLocation(data);
    }
    else if (autoComplete == m_eCurrentRequest)
    {
        vParsePrediction(data);
    }
    else if (reverseGeo == m_eCurrentRequest)
    {
        vParseReverseGeo(data);
    }
}

void NetworkManager::vParsePrediction(const QString& reply)
{
    m_aPredictions.clear(); // delete old prediction data
    QJsonObject jobj = Utility::JStringToJobject(reply);
    const QJsonArray& predictionList = jobj["predictions"].toArray();

    QStringList uiList;

    foreach (const QJsonValue& value, predictionList)
    {
        const QJsonObject& jobjPredic = value.toObject();
        predictions predicts;
        predicts.description = jobjPredic["description"].toString();
        predicts.place_id = jobjPredic["reference"].toString();
        m_aPredictions.push_back(predicts);
        uiList << predicts.description;
    }

    m_pAutoCompleteModel->setStringList(uiList);
}

void NetworkManager::vParseLocation(const QString& reply)
{
    QJsonObject jobj = Utility::JStringToJobject(reply);
    location loca;
    const QJsonObject& jobjRes = jobj["result"].toObject();
    loca.formatedAddress = jobjRes["formatted_address"].toString();
    loca.name = jobjRes["name"].toString();
    loca.vicinity = jobjRes["vicinity"].toString();
    const QJsonObject& geo = jobjRes["geometry"].toObject();
    const QJsonObject& location = geo["location"].toObject();

    double lati = location["lat"].toDouble();
    double longi = location["lng"].toDouble();
    loca.latitude = QString::number(lati);
    loca.longitude = QString::number(longi);

    emit LocationParsed(loca);
}

void NetworkManager::vParseReverseGeo(const QString& reply)
{
    QJsonObject jobj = Utility::JStringToJobject(reply);
    const QJsonArray& jobjRes = jobj["results"].toArray();

    QStringList locationMatches;
    int count = 0;
    foreach (const QJsonValue& value, jobjRes)
    {
        ++count;
        const QJsonObject results = value.toObject();
        QString address = results["formatted_address"].toString();
        locationMatches << address;
        if (count == 1)
            break; // for timebeing I require only the first address
    }

    m_pReverseGeoModel->setStringList(locationMatches);
}
