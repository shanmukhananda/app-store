#include "cca/CEventBusAny.hpp"
#include "cca/Macros.hpp"

void test(std::shared_ptr<int> data)
{
    LOG_SCOPE;
    std::cout << "\ntest called data = " << *data << std::endl;
    using namespace std;
    std::this_thread::sleep_for(6s);
}

class TestEvent
{
public:
    void print() const
    {
        LOG_SCOPE;
        std::cout << "\ndata=" << data << std::endl;
    }
    int data;
};

class TestListener
{
public:
    void callback(std::shared_ptr<TestEvent> event_)
    {
        LOG_SCOPE;

        event_->print();
    }
    void callback2(std::shared_ptr<TestEvent> event_)
    {
        LOG_SCOPE;
        event_->print();
    }
};

struct A
{
    virtual ~A()
    {
    }
};

struct B : A
{
};
struct C : A
{
};

namespace test1
{
class ctest1
{
};
}

namespace test2
{
class ctest1
{
};
}

int main()
{
    LOG_SCOPE;
    CEventBusAny ebus;
    auto fobj = test;
    ebus.subscribe<std::shared_ptr<int>>(fobj);
    ebus.publish<std::shared_ptr<int>>(std::make_shared<int>(12));
    // using namespace std;
    // std::this_thread::sleep_for(6s);
    // ebus.unsubscribe<int>();
    ebus.publish<std::shared_ptr<int>>(std::make_shared<int>(13));
    std::cout << std::endl
              << __FUNCTION__ << ":" << std::this_thread::get_id()
              << "Did i wait?" << std::endl;

    auto event_ = std::make_shared<TestEvent>();
    event_->data = 22;
    TestListener listener;
    std::function<void(std::shared_ptr<TestEvent>)> memFobj1 =
        std::bind(&TestListener::callback, &listener, std::placeholders::_1);
    std::function<void(std::shared_ptr<TestEvent>)> memFobj2 =
        std::bind(&TestListener::callback2, &listener, std::placeholders::_1);

    ebus.subscribe<std::shared_ptr<TestEvent>>(memFobj1);
    ebus.subscribe<std::shared_ptr<TestEvent>>(memFobj2);

    ebus.publish<std::shared_ptr<TestEvent>>(std::move(event_));
    // e->data = 12;

    using namespace std;
    std::this_thread::sleep_for(600s);
    return 0;
}
