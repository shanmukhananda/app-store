#include "cca/CJobExecuter.hpp"

#include <iostream>

class PrintAndSleep : public CJob
{
public:
    virtual void execute()
    {
        int count = 10;
        while (--count)
        {
            using namespace std;
            if (false == isCancelled())
            {
                std::this_thread::sleep_for(2s);
                std::cout << __FUNCTION__ << ":" << std::this_thread::get_id()
                          << std::endl;
            }
            else
            {
                break;
            }
        }
    }
};

int main()
{
    std::cout << __FUNCTION__ << ":" << std::this_thread::get_id() << std::endl;

    CJobExecuter job_exec;
    auto job_ = std::make_shared<PrintAndSleep>();
    job_exec.execute(job_);

    while (true)
    {
        using namespace std;
        std::this_thread::sleep_for(6s);
        std::cout << __FUNCTION__ << ":" << std::this_thread::get_id()
                  << std::endl;
        std::cout << __FUNCTION__ << ":"
                  << "cancelling " << std::endl;

        job_exec.cancelAll();
    }
    return 0;
}
