#include <iostream>

class CMessage
{
public:
    int mSenderID;
    int mReceiverID;
    void* caller;
};

class IArithmeticStubVisitor;
class IArithmeticMessage
{
public:
    virtual void acceptVisitor(IArithmeticStubVisitor* visitor) = 0;
    virtual ~IArithmeticMessage()
    {
    }
};

class CAddRequest;
class CMultiplyRequest;
class IArithmeticStubVisitor
{
public:
    virtual void action(CAddRequest* msg) = 0;
    virtual void action(CMultiplyRequest* msg) = 0;
    virtual ~IArithmeticStubVisitor()
    {
    }
};

class CAddResponse;
class CMultiplyResponse;
class IArithmeticProxyVisitor
{
public:
    virtual void action(CAddResponse* msg) = 0;
    virtual void action(CMultiplyResponse* msg) = 0;
    virtual ~IArithmeticProxyVisitor()
    {
    }
};

class CAddRequest : public IArithmeticMessage
{
public:
    int x()
    {
        return 10;
    }
    virtual void acceptVisitor(IArithmeticStubVisitor* visitor) override
    {
        visitor->action(this);
    }
};

class CMultiplyRequest : public IArithmeticMessage
{
public:
    int y()
    {
        return 20;
    }
    virtual void acceptVisitor(IArithmeticStubVisitor* visitor) override
    {
        visitor->action(this);
    }
};

class CAirthmeticStub : public IArithmeticStubVisitor
{
public:
    virtual void action(CAddRequest* msg) override
    {
        std::cout << __FUNCTION__ << ":" << msg->x() << std::endl;
    }
    virtual void action(CMultiplyRequest* msg) override
    {
        std::cout << __FUNCTION__ << ":" << msg->y() << std::endl;
    }
};

int main()
{
    IArithmeticMessage* pAdd = new CAddRequest();
    IArithmeticMessage* pMul = new CMultiplyRequest();

    IArithmeticStubVisitor* pVis = new CAirthmeticStub();

    pAdd->acceptVisitor(pVis);
    pMul->acceptVisitor(pVis);

    delete pAdd;
    delete pMul;
    delete pVis;
    return 0;
}
