#pragma once

#include "cca/CStub.hpp"

#include "CalculatorTypes.hpp"

namespace Calculator
{
class CCalculatorStub : public CStub
{
public:
    explicit CCalculatorStub(std::weak_ptr<IBroker>,
                             std::weak_ptr<IMessageDelegate>,
                             const std::string&);
    virtual ~CCalculatorStub();

    virtual void requestAdd(std::unique_ptr<CAddRequest>);
    void
    requestAddFailed(std::unique_ptr<CRequestFailed>, const CMessageContext&);
    void responseAdd(std::unique_ptr<CAddResponse>, const CMessageContext&);

    void setSampleAttribute(std::unique_ptr<CSampleAttribute>);
    const std::unique_ptr<const CSampleAttribute>& getSampleAttribute() const;

    void signalSampleSignal(std::unique_ptr<CSampleSignal>);

private:
    void processRequest(std::unique_ptr<IMessage>);
    void notifyOnAttributeSubscription(std::unique_ptr<IMessage>);

    std::unique_ptr<const CSampleAttribute> mSampleAttribute;
};
} // namespace Calculator