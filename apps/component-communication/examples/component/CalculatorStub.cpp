#include "CalculatorStub.hpp"

namespace Calculator
{
CCalculatorStub::CCalculatorStub(std::weak_ptr<IBroker> broker,
                                 std::weak_ptr<IMessageDelegate> delegate,
                                 const std::string& rolename)
    : CStub(broker, CInterfaceKey(rolename, CVersion(1, 0)), delegate)
{
    LOG_SCOPE;
}

CCalculatorStub::~CCalculatorStub()
{
    LOG_SCOPE;
}

void CCalculatorStub::requestAdd(std::unique_ptr<CAddRequest> request)
{
    LOG_SCOPE;
    auto response{std::make_unique<CAddResponse>()};
    response->result(request->first() + request->second());

    { // test
        auto failed{std::make_unique<CRequestFailed>()};
        failed->description("Test");
        requestAddFailed(std::move(failed), request->context());

        auto attribute{std::make_unique<CSampleAttribute>()};
        attribute->attribute(1212);
        setSampleAttribute(std::move(attribute));

        auto sig{std::make_unique<CSampleSignal>()};
        signalSampleSignal(std::move(sig));
    }

    responseAdd(std::move(response), request->context());
}

void CCalculatorStub::requestAddFailed(std::unique_ptr<CRequestFailed> failed,
                                       const CMessageContext& ctx)
{
    LOG_SCOPE;
    sendMethodFailed(CalculatorMessageID::ID_requestAddFailed,
                     std::move(failed), ctx);
}

void CCalculatorStub::responseAdd(std::unique_ptr<CAddResponse> response,
                                  const CMessageContext& ctx)
{
    LOG_SCOPE;
    sendMethodResponse(CalculatorMessageID::ID_responseAdd, std::move(response),
                       ctx);
}

void CCalculatorStub::setSampleAttribute(
    std::unique_ptr<CSampleAttribute> attribute)
{
    LOG_SCOPE;
    // todo: compare if onchange is true
    auto attributeCopy{utility::copy_unique<CSampleAttribute>(attribute)};
    sendAttribute(CalculatorMessageID::ID_sampleAttribute,
                  std::move(attributeCopy));
}

const std::unique_ptr<const CSampleAttribute>&
CCalculatorStub::getSampleAttribute() const
{
    LOG_SCOPE;
    return mSampleAttribute;
}

void CCalculatorStub::processRequest(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageId()) //!OCLINT (All the responses are not handled yet)
    {
        case CalculatorMessageID::ID_requestAdd:
        {
            auto request{utility::static_unique_ptr_cast<CAddRequest>(msg)};
            requestAdd(std::move(request));
        }
        break;
        default:
            break;
    }
}

void CCalculatorStub::signalSampleSignal(std::unique_ptr<CSampleSignal> signal)
{
    LOG_SCOPE;
    sendSignal(CalculatorMessageID::ID_sampleSignal, std::move(signal));
}

void CCalculatorStub::notifyOnAttributeSubscription(
    std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageId()) //!OCLINT (All the responses are not handled yet)
    {
        case CalculatorMessageID::ID_sampleAttribute:
        {
            auto attributeCopy{
                utility::copy_unique<CSampleAttribute>(mSampleAttribute)};
            sendAttribute(CalculatorMessageID::ID_sampleAttribute,
                          std::move(attributeCopy));
        }
        break;
        default:
            break;
    }
}
} // namespace Calculator
