#include "CalculatorComponent.hpp"

namespace Calculator
{
CCalculatorComponent::CCalculatorComponent(std::weak_ptr<IBroker> broker)
    : CComponent(broker)
{
    LOG_SCOPE;
}

CCalculatorComponent::~CCalculatorComponent()
{
    LOG_SCOPE;
}

void CCalculatorComponent::setUp()
{
    LOG_SCOPE;
    mCalculatorStub = addStub<CCalculatorStub>("Calculator");
    mCalculatorProxy = addProxy<CCalculatorProxy>("Calculator");

    auto memFobj1 = std::bind(&CCalculatorComponent::eventSink1, this,
                              std::placeholders::_1);
    subscribe<int>(memFobj1);
}

void CCalculatorComponent::tearDown()
{
    LOG_SCOPE;
}

void CCalculatorComponent::test()
{
    publish<int>(std::make_shared<int>(12));
}

void CCalculatorComponent::eventSink1(std::shared_ptr<int>)
{
    LOG_SCOPE;
    // publish<int>(std::make_shared<int>(12));
}
} // namespace Calculator
