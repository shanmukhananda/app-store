#include "cca/Utils.hpp"

#include "CalculatorProxy.hpp"

namespace Calculator
{
CCalculatorProxy::CCalculatorProxy(std::weak_ptr<IBroker> broker,
                                   std::weak_ptr<IMessageDelegate> delegate,
                                   const std::string& rolename)
    : CProxy(broker, CInterfaceKey(rolename, CVersion(1, 0)), delegate)
{
    LOG_SCOPE;
}

CCalculatorProxy::~CCalculatorProxy()
{
    LOG_SCOPE;
}

uint32_t CCalculatorProxy::requestAdd(std::unique_ptr<CAddRequest> request)
{
    LOG_SCOPE;
    return sendMethodRequest(CalculatorMessageID::ID_requestAdd,
                             std::move(request));
}

void CCalculatorProxy::requestAddFailed(std::unique_ptr<CRequestFailed>)
{
    LOG_SCOPE;
}

void CCalculatorProxy::responseAdd(std::unique_ptr<CAddResponse>)
{
    LOG_SCOPE;
}

const std::unique_ptr<const CSampleAttribute>&
CCalculatorProxy::getSampleAttribute() const
{
    LOG_SCOPE;
    return mSampleAttribute;
}

void CCalculatorProxy::attributeSampleAttribute(
    const std::unique_ptr<const CSampleAttribute>&)
{
    LOG_SCOPE;
}

void CCalculatorProxy::subscribeSampleAttribute()
{
    LOG_SCOPE;
    sendNotificationRequest(RequestType::ATTRIBUTE_SUBSCRIPTION,
                            CalculatorMessageID::ID_sampleAttribute);
}

void CCalculatorProxy::unsubscribeSampleAttribute()
{
    LOG_SCOPE;
    sendNotificationRequest(RequestType::ATTRIBUTE_UNSUBSCRIPTION,
                            CalculatorMessageID::ID_sampleAttribute);
}

void CCalculatorProxy::signalSampleSignal(std::unique_ptr<CSampleSignal>)
{
    LOG_SCOPE;
}

void CCalculatorProxy::subscribeSampleSignal()
{
    LOG_SCOPE;
    sendNotificationRequest(RequestType::SIGNAL_SUBSCRIPTION,
                            CalculatorMessageID::ID_sampleSignal);
}

void CCalculatorProxy::unsubscribeSampleSignal()
{
    LOG_SCOPE;
    sendNotificationRequest(RequestType::SIGNAL_UNSUBSCRIPTION,
                            CalculatorMessageID::ID_sampleSignal);
}

void CCalculatorProxy::processResponse(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageId()) //!OCLINT (All the responses are not handled yet)
    {
        case CalculatorMessageID::ID_responseAdd:
        {
            auto response{utility::static_unique_ptr_cast<CAddResponse>(msg)};
            responseAdd(std::move(response));
        }
        break;
        default:
            break;
    }
}

void CCalculatorProxy::processRequestFailed(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageId()) //!OCLINT (All the responses are not handled yet)
    {
        case CalculatorMessageID::ID_requestAddFailed:
        {
            auto failed{utility::static_unique_ptr_cast<CRequestFailed>(msg)};
            requestAddFailed(std::move(failed));
        }
        break;
        default:
            break;
    }
}

void CCalculatorProxy::processAttributeBroadcast(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageId()) //!OCLINT (All the responses are not handled yet)
    {
        case CalculatorMessageID::ID_sampleAttribute:
        {
            auto attribute{
                utility::static_unique_ptr_cast<CSampleAttribute>(msg)};
            mSampleAttribute = std::move(attribute);
            attributeSampleAttribute(mSampleAttribute);
        }
        break;
        default:
            break;
    }
}

void CCalculatorProxy::processSignalBroadcast(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageId()) //!OCLINT (All the responses are not handled yet)
    {
        case CalculatorMessageID::ID_sampleSignal:
        {
            auto signal{utility::static_unique_ptr_cast<CSampleSignal>(msg)};
            signalSampleSignal(std::move(signal));
        }
        break;
        default:
            break;
    }
}
} // namespace Calculator
