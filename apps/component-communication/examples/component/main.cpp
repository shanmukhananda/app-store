#include "CalculatorComponent.hpp"
#include "CalculatorProxy.hpp"
#include "CalculatorStub.hpp"

#include "cca/CBroker.hpp"

#include "boost/any.hpp"
#include "boost/variant.hpp"

#include <atomic>
#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <thread>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>
#include <vector>

int testipc()
{
    LOG_SCOPE;
    {
        ////auto delegate = std::make_shared<CMessageDelegate>();
        // auto broker = std::make_shared<CBroker>();
        // using namespace Calculator;
        ////auto calcStub = std::make_shared<CCalculatorStub>(broker);
        // auto calcProxy = std::make_shared<CCalculatorProxy>(broker);

        // broker->addClient(calcProxy);
        // broker->addServer(calcStub);

        // auto pRequest = std::make_unique<CAddRequest>();
        // pRequest->first(12);
        // pRequest->second(32);
        // using namespace std::chrono;

        // calcProxy->requestAdd(std::move(pRequest));
        ///*
        // std::this_thread::sleep_for(1s);
        // std::cout << "Use count of delegate = " << delegate.use_count() <<
        // std::endl;
        // std::cout << "Use count of broker = " << broker.use_count() <<
        // std::endl;
        // std::cout << "Use count of calcStub = " << calcStub.use_count() <<
        // std::endl;
        // std::cout << "Use count of calcProxy = " << calcProxy.use_count() <<
        // std::endl;
        //*/
    }
    return 0;
}

int testComponent()
{
    LOG_SCOPE;
    {
        std::shared_ptr<CBroker> broker; // = std::make_shared<CBroker>();
        using namespace Calculator;
        auto component = std::make_shared<CCalculatorComponent>(broker);
        component->setUp();
        component->test();
        using namespace std::chrono;
        std::this_thread::sleep_for(300s);
    }
    return 0;
}

int main()
{
    LOG_SCOPE;

    testComponent();
    return 0;
}