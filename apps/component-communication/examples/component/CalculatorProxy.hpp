#pragma once

#include "cca/CProxy.hpp"

#include "CalculatorTypes.hpp"

namespace Calculator
{
class CCalculatorProxy : public CProxy
{
public:
    explicit CCalculatorProxy(std::weak_ptr<IBroker>,
                              std::weak_ptr<IMessageDelegate>,
                              const std::string&);
    virtual ~CCalculatorProxy();

    uint32_t requestAdd(std::unique_ptr<CAddRequest>);

    const std::unique_ptr<const CSampleAttribute>& getSampleAttribute() const;
    void subscribeSampleAttribute();
    void unsubscribeSampleAttribute();

    void subscribeSampleSignal();
    void unsubscribeSampleSignal();

private:
    virtual void requestAddFailed(std::unique_ptr<CRequestFailed>);
    virtual void responseAdd(std::unique_ptr<CAddResponse>);
    virtual void componentConnected() override
    {
        LOG_SCOPE;
        subscribeSampleSignal();
        subscribeSampleAttribute();
    }
    virtual void componentDisconnected() override
    {
        LOG_SCOPE;
        unsubscribeSampleSignal();
        unsubscribeSampleAttribute();
    }
    virtual void
    attributeSampleAttribute(const std::unique_ptr<const CSampleAttribute>&);
    virtual void signalSampleSignal(std::unique_ptr<CSampleSignal>);

    virtual void processResponse(std::unique_ptr<IMessage>) override;
    virtual void processRequestFailed(std::unique_ptr<IMessage>) override;
    virtual void processAttributeBroadcast(std::unique_ptr<IMessage>) override;
    virtual void processSignalBroadcast(std::unique_ptr<IMessage>) override;

    std::unique_ptr<const CSampleAttribute> mSampleAttribute;
};

} // namespace Calculator
