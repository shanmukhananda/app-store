#pragma once

#include "cca/MessagePassing.hpp"

namespace Calculator
{
namespace CalculatorMessageID
{
const uint32_t START = 1;
const auto ID_invalid = START + 1;
const auto ID_requestAdd = START + 2;
const auto ID_requestAddFailed = START + 3;
const auto ID_responseAdd = START + 4;
const auto ID_sampleAttribute = START + 5;
const auto ID_sampleSignal = START + 6;
};

class CAddRequest : public CMessage
{
public:
    void first(int p);
    int first() const;
    void second(int p);
    int second() const;

private:
    int mFirst;
    int mSecond;
};

class CAddResponse : public CMessage
{
public:
    void result(int r);
    int result() const;

private:
    int mResult;
};

class CSampleAttribute : public CMessage
{
public:
    bool operator==(const CSampleAttribute& rhs) const;
    bool operator!=(const CSampleAttribute& rhs) const;

    void attribute(int a);
    int attribute() const;

private:
    int mAttribute;
};

class CSampleSignal : public CMessage
{
public:
    void signal(int s);
    int signal() const;

private:
    int mSignal;
};
} // namespace Calculator
