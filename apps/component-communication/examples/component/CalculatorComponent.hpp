#pragma once

#include "cca/CComponent.hpp"

#include "CalculatorProxy.hpp"
#include "CalculatorStub.hpp"

namespace Calculator
{
class CCalculatorComponent : public CComponent
{
public:
    explicit CCalculatorComponent(std::weak_ptr<IBroker>);
    ~CCalculatorComponent();
    virtual void setUp() override;
    virtual void tearDown() override;
    void eventSink1(std::shared_ptr<int> x);
    void test();

private:
    std::shared_ptr<CCalculatorStub> mCalculatorStub;
    std::shared_ptr<CCalculatorProxy> mCalculatorProxy;
};
} // namespace Calculator
