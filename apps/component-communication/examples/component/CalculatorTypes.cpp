#include "CalculatorTypes.hpp"

namespace Calculator
{
int CAddRequest::first() const
{
    return mFirst;
}

int CAddRequest::second() const
{
    return mSecond;
}

void CAddRequest::first(int first_)
{
    mFirst = first_;
}

void CAddRequest::second(int second_)
{
    mSecond = second_;
}

void CAddResponse::result(int result_)
{
    mResult = result_;
}

int CAddResponse::result() const
{
    return mResult;
}

bool CSampleAttribute::operator==(const CSampleAttribute& rhs) const
{
    return this->mAttribute == rhs.mAttribute;
}

bool CSampleAttribute::operator!=(const CSampleAttribute& rhs) const
{
    return !(*this == rhs);
}

void CSampleAttribute::attribute(int attribute_)
{
    mAttribute = attribute_;
}

int CSampleAttribute::attribute() const
{
    return mAttribute;
}

void CSampleSignal::signal(int signal_)
{
    mSignal = signal_;
}

int CSampleSignal::signal() const
{
    return mSignal;
}

} // namespace Calculator
