#pragma once

#include "CConcurrentQueue.hpp"
#include "boost/any.hpp"

#include <atomic>
#include <exception>
#include <future>
#include <memory>
#include <typeindex>
#include <unordered_map>

class CEventBusAny
{
public:
    CEventBusAny();
    ~CEventBusAny();

    template <typename TEvent>
    void subscribe(const std::function<void(TEvent)>& fobj);

    template <typename TEvent>
    void publish(TEvent&& data);

    template <typename TEvent>
    void unsubscribe();

private:
    void start();
    void stop();
    void run();

    std::atomic<bool> mStop;
    std::unordered_map<std::type_index, std::vector<boost::any>> mSubscribers;
    CConcurrentQueue<std::packaged_task<void()>> mDispatchQueue;
    std::mutex mMutex;
    std::unique_ptr<std::thread> mDispatcher;
};

inline CEventBusAny::CEventBusAny() : mStop(false)
{
    start();
}

inline CEventBusAny::~CEventBusAny()
{
    stop();
}

inline void CEventBusAny::start()
{
    std::unique_lock<std::mutex> locker(mMutex);
    auto t =
        std::unique_ptr<std::thread>(new std::thread(&CEventBusAny::run, this));
    mDispatcher = std::move(t);
}

inline void CEventBusAny::stop()
{
    std::unique_lock<std::mutex> locker(mMutex);
    mStop.exchange(true);
    std::packaged_task<void()> task([]() {});
    mDispatchQueue.enqueue(std::move(task));
    mDispatcher->join();
}

inline void CEventBusAny::run()
{
    while (false == mStop)
    {
        auto item = mDispatchQueue.dequeue();
        item();
    }
}

template <typename TEvent>
inline void CEventBusAny::subscribe(const std::function<void(TEvent)>& fobj)
{
    std::unique_lock<std::mutex> locker(mMutex);

    auto fobjCopy = boost::any(fobj);
    mSubscribers[std::type_index(typeid(TEvent))].push_back(fobjCopy);
}

template <typename TEvent>
inline void CEventBusAny::publish(TEvent&& data)
{
    std::unique_lock<std::mutex> locker(mMutex);

    auto lamdaFunc = [=]() {
        auto iter = mSubscribers.find(std::type_index(typeid(TEvent)));

        if (iter != mSubscribers.end())
        {
            for (auto fptr : iter->second)
            {
                using CallBackType = const std::function<void(TEvent)>;
                auto functObj = boost::any_cast<CallBackType>(fptr);
                functObj(data);
            }
        }
    };

    std::packaged_task<void()> task(lamdaFunc);
    mDispatchQueue.enqueue(std::move(task));
}

template <typename TEvent>
inline void CEventBusAny::unsubscribe()
{
    std::unique_lock<std::mutex> locker(mMutex);
    auto& ptrList = mSubscribers[std::type_index(typeid(TEvent))];
    ptrList.clear();
}
