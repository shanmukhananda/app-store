#pragma once

#include "CConcurrentQueue.hpp"
#include "CJob.hpp"

#include <atomic>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>

class CJobExecuter
{
public:
    CJobExecuter();
    ~CJobExecuter();
    int execute(std::shared_ptr<CJob> pJob);
    bool cancel(const int& id);
    void cancelAll();

private:
    void start();
    void stop();
    void run();

    int mJobId;
    std::atomic<bool> mStop;
    CConcurrentQueue<std::shared_ptr<CJob>> mJobQueue;
    std::unordered_map<int, std::shared_ptr<CJob>> mJobIdTbl;
    std::mutex mMutex;
    std::unique_ptr<std::thread> mJobRunner;
};

inline CJobExecuter::CJobExecuter() : mJobId(0), mStop(false)
{
    LOG_SCOPE;
    start();
}

inline CJobExecuter::~CJobExecuter()
{
    LOG_SCOPE;
    stop();
}

inline void CJobExecuter::start()
{
    LOG_SCOPE;
    auto thread_{std::unique_ptr<std::thread>(
        new std::thread(&CJobExecuter::run, this))};
    mJobRunner = std::move(thread_);
}

inline void CJobExecuter::stop()
{
    LOG_SCOPE;
    mStop.exchange(true);
    mJobQueue.enqueue(nullptr);
    mJobRunner->join();
}

inline int CJobExecuter::execute(std::shared_ptr<CJob> pJob)
{
    LOG_SCOPE;
    std::unique_lock<std::mutex> locker(mMutex);
    ++mJobId;
    mJobIdTbl.insert(std::make_pair(mJobId, pJob));
    mJobQueue.enqueue(pJob);
    return mJobId;
}

inline bool CJobExecuter::cancel(const int& id)
{
    LOG_SCOPE;
    std::unique_lock<std::mutex> locker(mMutex);
    bool ret{false};

    auto search{mJobIdTbl.find(id)};
    if (search != mJobIdTbl.end())
    {
        search->second->cancel();
        ret = true;
    }

    return ret;
}

inline void CJobExecuter::cancelAll()
{
    LOG_SCOPE;
    std::unique_lock<std::mutex> locker(mMutex);
    for (const auto& item : mJobIdTbl)
    {
        item.second->cancel();
    }
}

inline void CJobExecuter::run()
{
    LOG_SCOPE;
    while (false == mStop)
    {
        auto item{mJobQueue.dequeue()};

        if (item && false == item->isCancelled())
        {
            item->execute();
        }
    }
}
