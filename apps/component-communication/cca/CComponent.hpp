#pragma once

#include "CConcurrentQueue.hpp"
#include "CEventBus.hpp"
#include "Macros.hpp"
#include "MessagePassing.hpp"

#include <memory>
#include <vector>

class CComponent : public IMessageDelegate,
                   public ITaskDelegate,
                   public std::enable_shared_from_this<CComponent>,
                   public CEventBus<CComponent>
{
public:
    explicit CComponent(std::weak_ptr<IBroker>);
    ~CComponent();

    virtual void setUp() = 0;
    virtual void tearDown() = 0;

    template <typename TStub>
    std::shared_ptr<TStub> addStub(const std::string&);

    template <typename TProxy>
    std::shared_ptr<TProxy> addProxy(const std::string&);

private:
    void start();
    void stop();
    void run();

    virtual void acceptMessage(std::unique_ptr<IMessage>,
                               std::shared_ptr<IMessageWorker>) override;
    virtual void acceptTask(std::packaged_task<void()>) override;

    std::weak_ptr<IBroker> mBroker;
    std::atomic<bool> mStop;
    CConcurrentQueue<
        boost::variant<std::packaged_task<void()>,
                       std::pair<std::unique_ptr<IMessage>,
                                 std::shared_ptr<IMessageWorker>> // std::pair
                       > // boost::variant
        >
        mComponentQ;
    std::unique_ptr<std::thread> mComponentThread;
    std::mutex mEventBusLock;
};

inline CComponent::CComponent(std::weak_ptr<IBroker> broker)
    : mBroker(broker), mStop(false)
{
    LOG_SCOPE;
    start();
}

inline CComponent::~CComponent()
{
    LOG_SCOPE;
    stop();
}

inline void CComponent::start()
{
    LOG_SCOPE;
    auto thread_{
        std::unique_ptr<std::thread>(new std::thread(&CComponent::run, this))};
    mComponentThread = std::move(thread_);
}

inline void CComponent::stop()
{
    LOG_SCOPE;
    mStop.exchange(true);
    mComponentQ.enqueue(std::make_pair(nullptr, nullptr));
    mComponentThread->join();
}

inline void CComponent::run()
{
    LOG_SCOPE;
    while (false == mStop)
    {
        auto item{mComponentQ.dequeue()};

        switch (item.which())
        {
            case 0:
            {
                auto task{
                    std::move(boost::get<std::packaged_task<void()>>(item))};
                task();
            }
            break;
            case 1:
            {
                auto messageItem{std::move(
                    boost::get<std::pair<std::unique_ptr<IMessage>,
                                         std::shared_ptr<IMessageWorker>>>(
                        item))};

                if (messageItem.first && messageItem.second)
                {
                    messageItem.second->workOnMessage(
                        std::move(messageItem.first));
                }
            }
            break;
            default:
                break;
        } // switch (item.which())
    }     // while (false == mStop)
}

inline void CComponent::acceptMessage(std::unique_ptr<IMessage> msg,
                                      std::shared_ptr<IMessageWorker> worker)
{
    LOG_SCOPE;
    mComponentQ.enqueue(std::make_pair(std::move(msg), worker));
}

inline void CComponent::acceptTask(std::packaged_task<void()> task)
{
    LOG_SCOPE;
    mComponentQ.enqueue(std::move(task));
}

template <typename TStub>
inline std::shared_ptr<TStub> CComponent::addStub(const std::string& rolename)
{
    LOG_SCOPE;
    auto stub{std::make_shared<TStub>(mBroker, shared_from_this(), rolename)};

    if (false == mBroker.expired())
    {
        mBroker.lock()->addServer(stub);
    }

    return stub;
}

template <typename TProxy>
inline std::shared_ptr<TProxy> CComponent::addProxy(const std::string& rolename)
{
    LOG_SCOPE;
    auto proxy{std::make_shared<TProxy>(mBroker, shared_from_this(), rolename)};

    if (false == mBroker.expired())
    {
        mBroker.lock()->addClient(proxy);
    }

    return proxy;
}