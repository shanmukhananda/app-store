#pragma once

#include "CConcurrentQueue.hpp"
#include "Macros.hpp"
#include "MessagePassing.hpp"

#include <atomic>
#include <future>
#include <list>
#include <memory>
#include <typeindex>
#include <unordered_map>

template <typename TEventDelegate>
class CEventBus
{
public:
    explicit CEventBus();
    ~CEventBus();

    template <typename TEvent>
    uint32_t subscribe(std::function<void(std::shared_ptr<TEvent>)> fobj);

    template <typename TEvent>
    void publish(std::shared_ptr<TEvent>&& data);

    template <typename TEvent>
    void unsubscribe();

    template <typename TEvent>
    void unsubscribe(uint32_t);

private:
    template <typename TEvent>
    void publishTask(std::shared_ptr<TEvent> data);

    template <typename TEvent>
    void addSubscriber(std::pair<uint32_t, std::shared_ptr<const void>>&&);

    template <typename TEvent>
    std::list<std::pair<uint32_t, std::shared_ptr<const void>>>
    getSubscribers();

    std::atomic<uint32_t> mSubscriberID;
    std::unordered_map<
        std::type_index,
        std::list<std::pair<uint32_t,
                            std::shared_ptr<const void>> // std::pair
                  >                                      // std::list
        >
        mSubscribers; // Map(EventType, SubscriberList(SubcriberID,
                      // EventHandler))
    CConcurrentQueue<std::packaged_task<void()>> mDispatchQueue;
    std::mutex mMutex;
    std::unique_ptr<std::thread> mDispatcher;
};

template <typename TEventDelegate>
inline CEventBus<TEventDelegate>::CEventBus() : mSubscriberID(0)
{
    LOG_SCOPE;
}

template <typename TEventDelegate>
inline CEventBus<TEventDelegate>::~CEventBus()
{
    LOG_SCOPE;
}

template <typename TEventDelegate>
template <typename TEvent>
inline uint32_t CEventBus<TEventDelegate>::subscribe(
    std::function<void(std::shared_ptr<TEvent>)> fobj)
{
    LOG_SCOPE;
    ++mSubscriberID;
    auto fp{std::shared_ptr<const void>(
        new std::function<void(std::shared_ptr<TEvent>)>(fobj))};
    auto idCallbackPair{std::make_pair(mSubscriberID.load(), fp)};
    addSubscriber<TEvent>(std::move(idCallbackPair));
    return mSubscriberID;
}

template <typename TEventDelegate>
template <typename TEvent>
void CEventBus<TEventDelegate>::publishTask(std::shared_ptr<TEvent> data)
{
    LOG_SCOPE;
    auto eventHandlers{getSubscribers<TEvent>()};
    for (auto listItem : eventHandlers)
    {
        using CallBackType = const std::function<void(std::shared_ptr<TEvent>)>;
        auto callbackPtr{static_cast<CallBackType*>(listItem.second.get())};
        auto& functionObject{*callbackPtr};
        functionObject(std::move(data));
    }
}

template <typename TEventDelegate>
template <typename TEvent>
inline void CEventBus<TEventDelegate>::addSubscriber(
    std::pair<uint32_t, std::shared_ptr<const void>>&& idCallbackPair)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);
    mSubscribers[std::type_index(typeid(TEvent))].push_back(idCallbackPair);
}

template <typename TEventDelegate>
template <typename TEvent>
inline std::list<std::pair<uint32_t, std::shared_ptr<const void>>>
CEventBus<TEventDelegate>::getSubscribers()
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);
    auto iter{mSubscribers.find(std::type_index(typeid(TEvent)))};
    std::list<std::pair<uint32_t, std::shared_ptr<const void>>> retValue;
    if (iter != mSubscribers.end())
    {
        retValue = iter->second;
    }
    return retValue;
}

template <typename TEventDelegate>
template <typename TEvent>
inline void CEventBus<TEventDelegate>::publish(std::shared_ptr<TEvent>&& data)
{
    LOG_SCOPE;
    auto tempTask = std::bind(&CEventBus::publishTask<TEvent>, this, data);
    std::packaged_task<void()> task{tempTask};
    TEventDelegate* component{static_cast<TEventDelegate*>(this)};
    ITaskDelegate* eventDelegate{static_cast<ITaskDelegate*>(component)};
    eventDelegate->acceptTask(std::move(task));
}

template <typename TEventDelegate>
template <typename TEvent>
inline void CEventBus<TEventDelegate>::unsubscribe()
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    auto iter{mSubscribers.find(std::type_index(typeid(TEvent)))};
    if (iter != mSubscribers.end())
    {
        iter->second.clear();
    }
}

template <typename TEventDelegate>
template <typename TEvent>
inline void CEventBus<TEventDelegate>::unsubscribe(uint32_t id)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    auto iter{mSubscribers.find(std::type_index(typeid(TEvent)))};
    if (iter != mSubscribers.end())
    {
        auto& subslist{iter->second};

        subslist.remove_if(
            [&](const auto& eachitem) { return (eachitem.first == id); });
    }
}
