#pragma once

#include "IMessagePassing.hpp"

class CMessageConsumer : public IMessageConsumer
{
public:
    CMessageConsumer(const std::string&, const std::pair<int, int>& version,
                     IBroker* broker);
    virtual std::string name() const override;
    virtual std::pair<int, int> version() const override;
    virtual ~CMessageConsumer()
    {
    }

private:
    std::string mName;
    std::pair<int, int> mVersion;
    std::shared_ptr<IBroker> mBroker;
};

inline CMessageConsumer::CMessageConsumer(const std::string& name,
                                          const std::pair<int, int>& version,
                                          IBroker* broker)
    : mName(name), mVersion(version), mBroker(broker)
{
}

inline std::string CMessageConsumer::name() const
{
    return mName;
}

inline std::pair<int, int> CMessageConsumer::version() const
{
    return mVersion;
}
