#pragma once

#include "Macros.hpp"
#include "MessagePassing.hpp"

#include <atomic>
#include <memory>
#include <mutex>
#include <unordered_map>

class CProxy : public IMessageConsumer,
               public IMessageWorker,
               public std::enable_shared_from_this<CProxy>
{
public:
    CProxy(std::weak_ptr<IBroker>, const CInterfaceKey&,
           std::weak_ptr<IMessageDelegate>);
    virtual ~CProxy();
    uint32_t sendMethodRequest(const uint32_t messageID,
                               std::unique_ptr<CMessage> request);
    void
    sendNotificationRequest(RequestType requestType, const uint32_t messageID);

private:
    virtual CInterfaceKey interfaceKey() override;
    virtual void processMessage(std::unique_ptr<IMessage>) override;
    virtual void workOnMessage(std::unique_ptr<IMessage>) override;
    virtual void componentConnected() = 0;
    virtual void componentDisconnected() = 0;
    virtual void processResponse(std::unique_ptr<IMessage>) = 0;
    virtual void processRequestFailed(std::unique_ptr<IMessage>) = 0;
    virtual void processAttributeBroadcast(std::unique_ptr<IMessage>) = 0;
    virtual void processSignalBroadcast(std::unique_ptr<IMessage>) = 0;
    void processComponentConnected(std::unique_ptr<IMessage>);
    void processComponentDisconnected(std::unique_ptr<IMessage>);
    void registerReplyHandlers();
    void sendMessage(std::unique_ptr<CMessage>);

    std::weak_ptr<IBroker> mBroker;
    CInterfaceKey mInterfaceKey;
    std::weak_ptr<IMessageDelegate> mDelegate;
    std::atomic<std::uint32_t> mSequenceNumber;
    std::unordered_map<ReplyType, std::function<void(std::unique_ptr<IMessage>)>>
        mReplyHandlers;
};

inline CProxy::CProxy(std::weak_ptr<IBroker> broker, const CInterfaceKey& ikey,
                      std::weak_ptr<IMessageDelegate> d)
    : mBroker(broker), mInterfaceKey(ikey), mDelegate(d), mSequenceNumber(0)
{
    LOG_SCOPE;
    registerReplyHandlers();
}

inline CProxy::~CProxy()
{
    LOG_SCOPE;
}

inline void CProxy::processMessage(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    if (false == mDelegate.expired())
    {
        auto sp{mDelegate.lock()};
        if (sp != nullptr)
        {
            sp->acceptMessage(std::move(msg), shared_from_this());
        }
    }
}

inline void CProxy::workOnMessage(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    auto replyType{boost::get<ReplyType>(msg->messageType())};

    auto iter{mReplyHandlers.find(replyType)};

    if (iter != mReplyHandlers.end())
    {
        iter->second(std::move(msg));
    }
}

inline CInterfaceKey CProxy::interfaceKey()
{
    LOG_SCOPE;
    return mInterfaceKey;
}

inline void CProxy::registerReplyHandlers()
{
    LOG_SCOPE;
    mReplyHandlers[ReplyType::METHOD_RESPONSE] =
        std::bind(&CProxy::processResponse, this, std::placeholders::_1);

    mReplyHandlers[ReplyType::METHOD_FAILED] =
        std::bind(&CProxy::processRequestFailed, this, std::placeholders::_1);

    mReplyHandlers[ReplyType::ATTRIBUTE_BROADCAST] = std::bind(
        &CProxy::processAttributeBroadcast, this, std::placeholders::_1);

    mReplyHandlers[ReplyType::SIGNAL_BROADCAST] =
        std::bind(&CProxy::processSignalBroadcast, this, std::placeholders::_1);

    mReplyHandlers[ReplyType::COMPONENET_CONNECTED] = std::bind(
        &CProxy::processComponentConnected, this, std::placeholders::_1);

    mReplyHandlers[ReplyType::COMPONENET_DISCONNECTED] = std::bind(
        &CProxy::processComponentDisconnected, this, std::placeholders::_1);
}

inline void CProxy::sendMessage(std::unique_ptr<CMessage> msg)
{
    LOG_SCOPE;
    if (false == mBroker.expired())
    {
        mBroker.lock()->sendMessage(std::move(msg));
    }
}

inline uint32_t CProxy::sendMethodRequest(const uint32_t messageID,
                                          std::unique_ptr<CMessage> request)
{
    LOG_SCOPE;
    auto seqNo{++mSequenceNumber};
    CMessageContext ctx{shared_from_this(), seqNo};
    request->interfaceKey(mInterfaceKey);
    request->context(ctx);
    request->messageType(RequestType::METHOD_REQUEST);
    request->messageId(messageID);
    sendMessage(std::move(request));

    return seqNo;
}

inline void CProxy::sendNotificationRequest(RequestType requestType,
                                            const uint32_t messageID)
{
    LOG_SCOPE;
    auto msg{std::make_unique<CMessage>()};
    msg->messageType(requestType);
    CMessageContext ctx{shared_from_this(), 0};
    msg->context(ctx);
    msg->messageId(messageID);
    msg->interfaceKey(mInterfaceKey);
    sendMessage(std::move(msg));
}

inline void CProxy::processComponentConnected(std::unique_ptr<IMessage>)
{
    LOG_SCOPE;
    componentConnected();
}

inline void CProxy::processComponentDisconnected(std::unique_ptr<IMessage>)
{
    LOG_SCOPE;
    componentDisconnected();
}