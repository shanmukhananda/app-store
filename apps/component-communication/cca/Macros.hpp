#pragma once

#include "Utils.hpp"

#ifdef _MSC_VER

#pragma warning(push)
#pragma warning(disable : 4996) // warning C4996: 'vsnprintf': This function or
// variable may be unsafe.

#define ONCE                                                                \
    __pragma(warning(push)) __pragma(warning(disable : 4127)) while (false) \
        __pragma(warning(pop))

#else

#define ONCE while (false)

#endif /*_MSC_VER*/

#ifndef __FILENAME__
#ifdef _MSC_VER

#define __FILENAME__ \
    (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#else
#define __FILENAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#endif /*_MSC_VER*/
#endif /*__FILENAME__*/

#pragma GCC diagnostic ignored "-Wattributes"
#define LOCATION                                                        \
    std::string(__FILENAME__) + "(" + std::to_string(__LINE__) + ") " + \
        std::string(__FUNCTION__)
#define LOG_ENTER_EXIT                                                 \
    debugging::CEnterExit __attribute__((annotate("oclint:suppress"))) \
        ___enter_exit_obj___(LOCATION)
#define LOG_SCOPE_TIME                                                 \
    debugging::CScopeTime __attribute__((annotate("oclint:suppress"))) \
        ___scope_time_obj___(LOCATION)
#define NO_OPERATION \
    do               \
    {                \
    }                \
    ONCE

#ifndef ENABLE_SCOPE_TIME
#if ENABLE_ENTER_EXIT
#undef LOG_SCOPE
#define LOG_SCOPE LOG_ENTER_EXIT
#else
#undef LOG_SCOPE
#define LOG_SCOPE NO_OPERATION
#endif
#endif

#ifndef ENABLE_ENTER_EXIT
#if ENABLE_SCOPE_TIME
#undef LOG_SCOPE
#define LOG_SCOPE LOG_SCOPE_TIME
#else
#undef LOG_SCOPE
#define LOG_SCOPE NO_OPERATION
#endif
#endif
