#pragma once

#include <string.h>
#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <unordered_map>

#undef major
#undef minor

namespace utility
{
template <typename TDerrived, typename TBase>
std::unique_ptr<TDerrived> static_unique_ptr_cast(std::unique_ptr<TBase>& base)
{
    return std::unique_ptr<TDerrived>(static_cast<TDerrived*>(base.release()));
}

template <typename TDerrived, typename TBase>
std::unique_ptr<TDerrived> static_unique_ptr_cast(std::unique_ptr<TBase>&& base)
{
    return std::unique_ptr<TDerrived>(static_cast<TDerrived*>(base.release()));
}

template <typename T>
std::unique_ptr<T> copy_unique(const std::unique_ptr<T>& source)
{
    return source ? std::make_unique<T>(*source) : nullptr;
}

template <typename T>
std::unique_ptr<T> copy_unique(const std::unique_ptr<const T>& source)
{
    return source ? std::make_unique<T>(*source) : nullptr;
}

template <typename T>
std::unique_ptr<T>
copy_unique(const std::unique_ptr<T>& source, std::mutex& lock)
{
    std::lock_guard<std::mutex> locker(lock);
    return source ? std::make_unique<T>(*source) : nullptr;
}

template <typename T>
std::unique_ptr<T>
copy_unique(const std::unique_ptr<const T>& source, std::mutex& lock)
{
    std::lock_guard<std::mutex> locker(lock);
    return source ? std::make_unique<T>(*source) : nullptr;
}
}

namespace debugging
{
class CEnterExit
{
public:
    explicit CEnterExit(const std::string& nameParam) : name(nameParam)
    {
        std::stringstream ss;
        ss << "=>> " << std::this_thread::get_id() << " " << name << "\n";
        std::cout << ss.str();
    }
    ~CEnterExit()
    {
        std::stringstream ss;
        ss << "<<= " << std::this_thread::get_id() << " " << name << "\n";
        std::cout << ss.str();
    }

private:
    std::string name;
};

class CScopeTime
{
public:
    explicit CScopeTime(const std::string& nameParam)
        : begin(std::chrono::steady_clock::now()), name(nameParam)
    {
    }
    ~CScopeTime()
    {
        end = std::chrono::steady_clock::now();
        auto timeTaken{
            std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
                .count()};
        std::stringstream ss;
        ss << std::this_thread::get_id() << " " << name << " " << timeTaken
           << "\n";
        std::cout << ss.str();
    }

private:
    std::chrono::steady_clock::time_point begin;
    std::chrono::steady_clock::time_point end;
    std::string name;
};
}