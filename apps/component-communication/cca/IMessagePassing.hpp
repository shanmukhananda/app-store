#pragma once

#include "CMessage.hpp"

#include <boost/any.hpp>
#include <boost/variant.hpp>
#include <cstdint>
#include <memory>
#include <string>
#include <utility>

class CMessageContext
{
public:
    CMessageContext();
    void* token() const;
    void token(void*);
    uint32_t sequenceNumber() const;
    void sequenceNumber(const uint32_t);
    uint32_t messageId();
    void messageId(const uint32_t);

private:
    void* mToken;
    uint32_t mSequenceNumber;
    uint32_t mMsgId;
};

enum class eRequestType
{
    INVALID,
    METHOD_REQUEST,
    ATTRIBUTE_SUBSCRIPTION,
    ATTRIBUTE_UNSUBSCRIPTION,
    SIGNAL_SUBSCRIPTION,
    SIGNAL_UNSUBSCRIPTION
};

enum class eReplyType
{
    INVALID,
    METHOD_RESPONSE,
    METHOD_FAILED,
    SIGNAL_BROADCAST,
    ATTRIBUTE_BROADCAST,
    COMPONENET_CONNECTED,
    COMPONENET_DISCONNECTED
};

class IMessageConsumer;
class IMessage
{
public:
    virtual std::string name() = 0;
    virtual boost::variant<std::nullptr_t, eRequestType, eReplyType>
    messageType() = 0;
    virtual CMessageContext context() = 0;
    virtual ~IMessage()
    {
    }
};

class IBroker
{
public:
    virtual void sendMessage(IMessage*) = 0;
    virtual void addClient(IMessageConsumer*) = 0;
    virtual void addServer(IMessageConsumer*) = 0;
    virtual void removeClient(IMessageConsumer*) = 0;
    virtual void removeServer(IMessageConsumer*) = 0;
    virtual ~IBroker()
    {
    }
};

class IMessageConsumer
{
public:
    virtual std::pair<int, int> version() const = 0;
    virtual void processMessage(IMessage*) = 0;
    virtual std::string name() const = 0;
    virtual ~IMessageConsumer()
    {
    }
};

inline CMessageContext::CMessageContext() : mToken(nullptr), mSequenceNumber(0)
{
}

inline void* CMessageContext::token() const
{
    return mToken;
}

inline void CMessageContext::token(void* c)
{
    mToken = c;
}

inline uint32_t CMessageContext::sequenceNumber() const
{
    return mSequenceNumber;
}

inline void CMessageContext::sequenceNumber(uint32_t s)
{
    mSequenceNumber = s;
}

inline uint32_t CMessageContext::messageId()
{
    return mMsgId;
}

inline void CMessageContext::messageId(const uint32_t id)
{
    mMsgId = id;
}