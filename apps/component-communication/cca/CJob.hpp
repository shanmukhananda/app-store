#pragma once

#include "Macros.hpp"

#include <atomic>

class CJob
{
public:
    CJob();
    virtual ~CJob();
    virtual void execute() = 0;
    virtual void cancel();
    bool isCancelled();

private:
    std::atomic<bool> mCancel;
};

inline CJob::CJob() : mCancel(false)
{
    LOG_SCOPE;
}

inline CJob::~CJob()
{
    LOG_SCOPE;
}

inline void CJob::cancel()
{
    LOG_SCOPE;
    mCancel.exchange(true);
}

inline bool CJob::isCancelled()
{
    LOG_SCOPE;
    return mCancel;
}