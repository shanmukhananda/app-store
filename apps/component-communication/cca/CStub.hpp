#pragma once

#include "Macros.hpp"
#include "MessagePassing.hpp"
#include "Utils.hpp"

#include <atomic>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <unordered_set>

class CStub : public IMessageConsumer,
              public IMessageWorker,
              public std::enable_shared_from_this<CStub>
{
public:
    CStub(std::weak_ptr<IBroker> broker, const CInterfaceKey&,
          std::weak_ptr<IMessageDelegate>);
    virtual ~CStub();
    void sendSignal(const uint32_t messageID, std::unique_ptr<CMessage>);
    void sendAttribute(const uint32_t messageID, std::unique_ptr<CMessage>);
    void sendMethodResponse(const uint32_t messageID, std::unique_ptr<CMessage>,
                            const CMessageContext&);
    void sendMethodFailed(const uint32_t messageID, std::unique_ptr<CMessage>,
                          const CMessageContext&);

private:
    virtual CInterfaceKey interfaceKey() override;
    virtual void processMessage(std::unique_ptr<IMessage>) override;
    virtual void workOnMessage(std::unique_ptr<IMessage>) override;
    void registerReplyHandlers();
    void addSubscriber(const std::unique_ptr<IMessage>&);
    void removeSubscriber(const std::unique_ptr<IMessage>&);
    void publish(std::unique_ptr<CMessage>);
    void sendMessage(std::unique_ptr<CMessage>);

    virtual void processRequest(std::unique_ptr<IMessage>) = 0;
    virtual void notifyOnAttributeSubscription(std::unique_ptr<IMessage>) = 0;
    void processAttributeSubscription(std::unique_ptr<IMessage>);
    void processAttributeUnSubscription(std::unique_ptr<IMessage>);
    void processSignalSubscription(std::unique_ptr<IMessage>);
    void processSignalUnSubscription(std::unique_ptr<IMessage>);

    std::weak_ptr<IBroker> mBroker;
    CInterfaceKey mInterfaceKey;
    std::weak_ptr<IMessageDelegate> mDelegate;

    std::unordered_map<uint32_t,
                       std::unordered_set<std::shared_ptr<IMessageConsumer>>>
        mClients;
    std::unordered_map<RequestType,
                       std::function<void(std::unique_ptr<IMessage>)>>
        mRequestHandlers;
};

inline CStub::CStub(std::weak_ptr<IBroker> broker, const CInterfaceKey& ikey,
                    std::weak_ptr<IMessageDelegate> d)
    : mBroker(broker), mInterfaceKey(ikey), mDelegate(d)
{
    LOG_SCOPE;
    registerReplyHandlers();
}

inline CStub::~CStub()
{
    LOG_SCOPE;
}

inline void CStub::registerReplyHandlers()
{
    LOG_SCOPE;
    mRequestHandlers[RequestType::METHOD_REQUEST] =
        std::bind(&CStub::processRequest, this, std::placeholders::_1);

    mRequestHandlers[RequestType::ATTRIBUTE_SUBSCRIPTION] = std::bind(
        &CStub::processAttributeSubscription, this, std::placeholders::_1);

    mRequestHandlers[RequestType::ATTRIBUTE_UNSUBSCRIPTION] = std::bind(
        &CStub::processAttributeUnSubscription, this, std::placeholders::_1);

    mRequestHandlers[RequestType::SIGNAL_SUBSCRIPTION] = std::bind(
        &CStub::processSignalSubscription, this, std::placeholders::_1);

    mRequestHandlers[RequestType::SIGNAL_UNSUBSCRIPTION] = std::bind(
        &CStub::processSignalUnSubscription, this, std::placeholders::_1);
}

inline void CStub::processMessage(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    if (false == mDelegate.expired())
    {
        auto sp{mDelegate.lock()};
        if (sp != nullptr)
        {
            sp->acceptMessage(std::move(msg), shared_from_this());
        }
    }
}

inline void CStub::workOnMessage(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    auto requestType{boost::get<RequestType>(msg->messageType())};

    auto iter{mRequestHandlers.find(requestType)};

    if (iter != mRequestHandlers.end())
    {
        iter->second(std::move(msg));
    }
}

inline CInterfaceKey CStub::interfaceKey()
{
    LOG_SCOPE;
    return mInterfaceKey;
}

inline void CStub::addSubscriber(const std::unique_ptr<IMessage>& msg)
{
    LOG_SCOPE;
    const auto& subscriber{msg->context().token()};

    if (false == subscriber.expired())
    {
        mClients[msg->messageId()].insert(subscriber.lock());
    }
}

inline void CStub::removeSubscriber(const std::unique_ptr<IMessage>& msg)
{
    LOG_SCOPE;
    const auto& subscriber{msg->context().token()};

    if (false == subscriber.expired())
    {
        auto rawPtr{subscriber.lock().get()};
        auto& clients{mClients[msg->messageId()]};

        for (auto iter = clients.begin(); iter != clients.end(); ++iter)
        {
            if (iter->get() == rawPtr)
            {
                clients.erase(*iter);
            }
        }
    }
}

inline void CStub::publish(std::unique_ptr<CMessage> msg)
{
    LOG_SCOPE;
    if (msg != nullptr)
    {
        auto& clients{mClients[msg->messageId()]};

        std::size_t currentCount{0};
        for (const auto& client : clients)
        {
            CMessageContext ctx{client, 0};
            std::unique_ptr<CMessage> msgToSend;

            // for last client re-use already allocated msg
            if (++currentCount == clients.size())
            {
                msgToSend = std::move(msg);
            }
            else
            {
                msgToSend = utility::copy_unique<CMessage>(msg);
            }

            msgToSend->context(ctx);
            msgToSend->interfaceKey(mInterfaceKey);
            msgToSend->messageId(msg->messageId());
            msgToSend->messageType(msg->messageType());
            sendMessage(std::move(msgToSend));
        }
    }
}

inline void CStub::sendMessage(std::unique_ptr<CMessage> msg)
{
    LOG_SCOPE;
    if (false == mBroker.expired())
    {
        mBroker.lock()->sendMessage(std::move(msg));
    }
}

inline void CStub::processAttributeSubscription(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    addSubscriber(msg);
    notifyOnAttributeSubscription(std::move(msg));
}

inline void CStub::processAttributeUnSubscription(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    removeSubscriber(msg);
}

inline void CStub::processSignalSubscription(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    addSubscriber(msg);
}

inline void CStub::processSignalUnSubscription(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    removeSubscriber(msg);
}

inline void
CStub::sendSignal(const uint32_t messageID, std::unique_ptr<CMessage> msg)
{
    LOG_SCOPE;
    msg->messageType(ReplyType::SIGNAL_BROADCAST);
    msg->messageId(messageID);
    publish(std::move(msg));
}

inline void
CStub::sendAttribute(const uint32_t messageID, std::unique_ptr<CMessage> msg)
{
    LOG_SCOPE;
    if (msg != nullptr)
    {
        msg->messageType(ReplyType::ATTRIBUTE_BROADCAST);
        msg->messageId(messageID);
        publish(std::move(msg));
    }
}

inline void CStub::sendMethodFailed(const uint32_t messageID,
                                    std::unique_ptr<CMessage> failed,
                                    const CMessageContext& ctx)
{
    LOG_SCOPE;
    failed->context(ctx);
    failed->interfaceKey(mInterfaceKey);
    failed->messageType(ReplyType::METHOD_FAILED);
    failed->messageId(messageID);
    sendMessage(std::move(failed));
}

inline void CStub::sendMethodResponse(const uint32_t messageID,
                                      std::unique_ptr<CMessage> response,
                                      const CMessageContext& ctx)
{
    LOG_SCOPE;
    response->context(ctx);
    response->interfaceKey(mInterfaceKey);
    response->messageType(ReplyType::METHOD_RESPONSE);
    response->messageId(messageID);
    sendMessage(std::move(response));
}
