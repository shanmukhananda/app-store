#pragma once

#include "IMessagePassing.hpp"

#include <cstdint>

class CMessage
{
public:
    CMessage();
    CMessageContext context() const;
    void context(const CMessageContext);
    eMessageType messageType() const;
    void messageType(eMessageType);
    uint32_t messageId();
    void messageId(const uint32_t);

private:
    CMessageContext mContext;
    eMessageType mMsgType;
    uint32_t mMsgId;
};

inline CMessage::CMessage()
    : mContext(), mMsgType(eMessageType::INVALID), mMsgId(0)
{
}

inline CMessageContext CMessage::context() const
{
    return mContext;
}

inline void CMessage::context(CMessageContext c)
{
    mContext = c;
}

inline eMessageType CMessage::messageType() const
{
    return mMsgType;
}

inline void CMessage::messageType(eMessageType t)
{
    mMsgType = t;
}

inline uint32_t CMessage::messageId()
{
    return mMsgId;
}

inline void CMessage::messageId(uint32_t i)
{
    mMsgId = i;
}
