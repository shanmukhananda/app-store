project(cca)

set(source_list
    CBroker.hpp
    CComponent.hpp
    CConcurrentQueue.hpp
    CEventBus.hpp
    CJob.hpp
    CJobExecuter.hpp
    CMessage.hpp
    CMessageConsumer.hpp
    CProxy.hpp
    CStub.hpp
    IMessagePassing.hpp
    Macros.hpp
    MessagePassing.hpp
    Utils.hpp)
