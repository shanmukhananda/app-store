#pragma once

#include "CConcurrentQueue.hpp"
#include "Macros.hpp"
#include "MessagePassing.hpp"

#include <atomic>
#include <iostream>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class CBroker : public IBroker
{
public:
    CBroker();
    virtual void sendMessage(std::unique_ptr<IMessage>);
    virtual void addClient(std::shared_ptr<IMessageConsumer>);
    virtual void addServer(std::shared_ptr<IMessageConsumer>);
    virtual void removeClient(std::shared_ptr<IMessageConsumer>);
    virtual void removeServer(std::shared_ptr<IMessageConsumer>);
    virtual ~CBroker();

private:
    void start();
    void stop();
    void addNewServer(std::shared_ptr<IMessageConsumer>);
    void removeServerImpl(std::shared_ptr<IMessageConsumer>);
    void sendConnectedMessage(std::shared_ptr<IMessageConsumer>);
    void sendDisconnectedMessage(std::shared_ptr<IMessageConsumer>);
    void sendMessageImpl(std::unique_ptr<IMessage>);
    void runServer();
    void runClient();
    std::shared_ptr<IMessageConsumer>
    getServer(const std::unique_ptr<IMessage>&);
    std::shared_ptr<IMessageConsumer>
    getClient(const std::unique_ptr<IMessage>&);

    std::atomic<bool> mStop;
    std::unique_ptr<std::thread> mServerDispatcher;
    std::unique_ptr<std::thread> mClientDispatcher;
    CConcurrentQueue<std::unique_ptr<IMessage>> mServerQ;
    CConcurrentQueue<std::unique_ptr<IMessage>> mClientQ;
    std::unordered_map<CInterfaceKey, std::shared_ptr<IMessageConsumer>>
        mServerTbl; // Map(Interface, Server)
    std::unordered_map<CInterfaceKey,
                       std::unordered_set<std::shared_ptr<IMessageConsumer>>>
        mClientTbl; // Map(Interface, Set(Clients))
    std::unordered_map<CInterfaceKey, std::shared_ptr<IMessageConsumer>>
        mConnectedServer; // Map(Interface, Server)
    std::unordered_map<CInterfaceKey,
                       std::unordered_set<std::shared_ptr<IMessageConsumer>>>
        mConnectedClients; // Map(Interface, Set(Clients))
    std::mutex mMutex;
};

inline CBroker::CBroker() : mStop(false)
{
    LOG_SCOPE;
    start();
}

inline CBroker::~CBroker()
{
    LOG_SCOPE;
    stop();
}

inline void CBroker::sendMessageImpl(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    switch (msg->messageType().which())
    {
        case 0:
            mServerQ.enqueue(std::move(msg));
            break;
        case 1:
            mClientQ.enqueue(std::move(msg));
            break;
        default:
            break;
    }
}

inline void CBroker::sendMessage(std::unique_ptr<IMessage> msg)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);
    sendMessageImpl(std::move(msg));
}

inline void CBroker::addClient(std::shared_ptr<IMessageConsumer> client)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    auto& clientList{mClientTbl[client->interfaceKey()]};
    clientList.insert(client);

    auto iter{mServerTbl.find(client->interfaceKey())};
    if (iter != mServerTbl.end())
    {
        auto& server{iter->second};
        mConnectedServer.insert(std::make_pair(client->interfaceKey(), server));
        auto& connectedClients{mConnectedClients[client->interfaceKey()]};
        connectedClients.insert(client);
        sendConnectedMessage(client);
    }
}

inline void CBroker::addNewServer(std::shared_ptr<IMessageConsumer> server)
{
    LOG_SCOPE;
    mServerTbl[server->interfaceKey()] = server;

    auto& clientList{mClientTbl[server->interfaceKey()]};
    auto& connectedClients{mConnectedClients[server->interfaceKey()]};

    for (auto& client : clientList)
    {
        mConnectedServer.insert(std::make_pair(server->interfaceKey(), server));
        connectedClients.insert(client);
        sendConnectedMessage(client);
    }
}

inline void CBroker::addServer(std::shared_ptr<IMessageConsumer> server)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    auto& existingServer{mServerTbl[server->interfaceKey()]};

    if (existingServer != nullptr)
    {
        removeServerImpl(existingServer);
    }

    addNewServer(server);
}

inline void CBroker::removeClient(std::shared_ptr<IMessageConsumer> client)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    auto& clientList{mClientTbl[client->interfaceKey()]};
    clientList.erase(client);

    auto& connectedClients{mConnectedClients[client->interfaceKey()]};
    connectedClients.erase(client);
}

inline void CBroker::removeServer(std::shared_ptr<IMessageConsumer> server)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);
    removeServerImpl(server);
}

inline void CBroker::removeServerImpl(std::shared_ptr<IMessageConsumer> server)
{
    LOG_SCOPE;
    auto& connectedClients{mConnectedClients[server->interfaceKey()]};

    for (auto& client : connectedClients)
    {
        sendDisconnectedMessage(client);
    }

    mServerTbl.erase(server->interfaceKey());
    mConnectedServer.erase(server->interfaceKey());
}

inline void
CBroker::sendConnectedMessage(std::shared_ptr<IMessageConsumer> client)
{
    LOG_SCOPE;
    auto msg{std::make_unique<CMessage>()};
    msg->messageType(ReplyType::COMPONENET_CONNECTED);
    msg->interfaceKey(client->interfaceKey());
    CMessageContext ctx{client, 0};
    msg->context(ctx);
    sendMessageImpl(std::move(msg));
}

inline void
CBroker::sendDisconnectedMessage(std::shared_ptr<IMessageConsumer> client)
{
    LOG_SCOPE;
    auto msg{std::make_unique<CMessage>()};
    msg->messageType(ReplyType::COMPONENET_DISCONNECTED);
    msg->interfaceKey(client->interfaceKey());
    CMessageContext ctx{client, 0};
    msg->context(ctx);
    sendMessageImpl(std::move(msg));
}

inline void CBroker::start()
{
    LOG_SCOPE;
    auto server{std::unique_ptr<std::thread>(
        new std::thread(&CBroker::runServer, this))};
    mServerDispatcher = std::move(server);

    auto client{std::unique_ptr<std::thread>(
        new std::thread(&CBroker::runClient, this))};
    mClientDispatcher = std::move(client);
}

inline void CBroker::stop()
{
    LOG_SCOPE;
    mStop.exchange(true);
    mServerQ.enqueue(nullptr);
    mClientQ.enqueue(nullptr);
    mServerDispatcher->join();
    mClientDispatcher->join();
}

inline std::shared_ptr<IMessageConsumer>
CBroker::getServer(const std::unique_ptr<IMessage>& msg)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    std::shared_ptr<IMessageConsumer> ret;
    auto iter{mConnectedServer.find(msg->interfaceKey())};
    if (iter != mConnectedServer.end())
    {
        ret = iter->second;
    }

    return ret;
}

inline std::shared_ptr<IMessageConsumer>
CBroker::getClient(const std::unique_ptr<IMessage>& msg)
{
    LOG_SCOPE;
    std::lock_guard<std::mutex> locker(mMutex);

    std::shared_ptr<IMessageConsumer> ret;
    auto iter{mConnectedClients.find(msg->interfaceKey())};
    if (iter != mConnectedClients.end())
    {
        auto& matchingClients{iter->second};
        auto source{msg->context().token()};

        if (false == source.expired())
        {
            auto rawPtr{source.lock().get()};

            auto miter{std::find_if(matchingClients.begin(),
                                    matchingClients.end(), [&](auto& data) {
                                        return (data.get() == rawPtr);
                                    })};

            if (miter != matchingClients.end())
            {
                ret = *miter;
            }
        }
    }

    return ret;
}

inline void CBroker::runServer()
{
    LOG_SCOPE;
    while (false == mStop)
    {
        auto msg{mServerQ.dequeue()};

        if (msg != nullptr)
        {
            auto server{getServer(msg)};
            if (server != nullptr)
            {
                server->processMessage(std::move(msg));
            }
        }
    }
}

inline void CBroker::runClient()
{
    LOG_SCOPE;
    while (false == mStop)
    {
        auto msg{mClientQ.dequeue()};

        if (msg != nullptr)
        {
            auto client{getClient(msg)};

            if (client != nullptr)
            {
                client->processMessage(std::move(msg));
            }
        }
    }
}
