#pragma once

#include "Utils.hpp"

#include <cstdint>
#include <future>
#include <memory>
#include <string>
#include <utility>

#include <boost/variant.hpp>

enum class RequestType
{
    INVALID,
    METHOD_REQUEST,
    ATTRIBUTE_SUBSCRIPTION,
    ATTRIBUTE_UNSUBSCRIPTION,
    SIGNAL_SUBSCRIPTION,
    SIGNAL_UNSUBSCRIPTION
};

enum class ReplyType
{
    INVALID,
    METHOD_RESPONSE,
    METHOD_FAILED,
    SIGNAL_BROADCAST,
    ATTRIBUTE_BROADCAST,
    COMPONENET_CONNECTED,
    COMPONENET_DISCONNECTED
};

enum class RequestFailed
{
    INVALID,
    INPUT_ERROR
};

class CVersion
{
public:
    CVersion();
    ~CVersion() = default;
    CVersion(int major, int minor);
    void major(int);
    int major() const;
    void minor(int);
    int minor() const;

private:
    int mMajor;
    int mMinor;
};

class CInterfaceKey
{
public:
    CInterfaceKey() = default;
    ~CInterfaceKey() = default;
    CInterfaceKey(const std::string& name, const CVersion& version);
    std::string name() const;
    void name(const std::string&);
    CVersion version() const;
    void version(const CVersion&);
    bool operator==(const CInterfaceKey&) const;

private:
    std::string mName;
    CVersion mVersion;
};

namespace std
{
template <>
struct hash<CInterfaceKey>
{
    size_t operator()(const CInterfaceKey& k) const
    {
        return (hash<int>()(k.version().major()) ^
                hash<int>()(k.version().minor()) ^ hash<string>()(k.name()));
    }
};
}

class IMessageConsumer;
class CMessageContext
{
public:
    CMessageContext();
    ~CMessageContext() = default;
    CMessageContext(std::weak_ptr<IMessageConsumer> token, uint32_t seqNo);
    std::weak_ptr<IMessageConsumer> token() const;
    void token(std::weak_ptr<IMessageConsumer>);
    uint32_t sequenceNumber() const;
    void sequenceNumber(const uint32_t);

private:
    uint32_t mSequenceNumber;
    std::weak_ptr<IMessageConsumer> mToken;
};

class IMessage
{
public:
    virtual CInterfaceKey interfaceKey() const = 0;
    virtual boost::variant<RequestType, ReplyType> messageType() const = 0;
    virtual CMessageContext context() const = 0;
    virtual uint32_t messageId() const = 0;
    virtual ~IMessage()
    {
    }
};

class IBroker
{
public:
    virtual void sendMessage(std::unique_ptr<IMessage>) = 0;
    virtual void addClient(std::shared_ptr<IMessageConsumer>) = 0;
    virtual void addServer(std::shared_ptr<IMessageConsumer>) = 0;
    virtual void removeClient(std::shared_ptr<IMessageConsumer>) = 0;
    virtual void removeServer(std::shared_ptr<IMessageConsumer>) = 0;
    virtual ~IBroker()
    {
    }
};

class IMessageConsumer
{
public:
    virtual void processMessage(std::unique_ptr<IMessage>) = 0;
    virtual CInterfaceKey interfaceKey() = 0;
    virtual ~IMessageConsumer()
    {
    }
};

class IMessageWorker
{
public:
    virtual void workOnMessage(std::unique_ptr<IMessage>) = 0;
    virtual ~IMessageWorker()
    {
    }
};

class IMessageDelegate
{
public:
    virtual void acceptMessage(std::unique_ptr<IMessage>,
                               std::shared_ptr<IMessageWorker>) = 0;
    virtual ~IMessageDelegate()
    {
    }
};

class ITaskDelegate
{
public:
    virtual void acceptTask(std::packaged_task<void()>) = 0;
    virtual ~ITaskDelegate()
    {
    }
};

class CMessage : public IMessage
{
public:
    CMessage();
    virtual ~CMessage();
    virtual CMessageContext context() const override;
    void context(const CMessageContext&);
    virtual uint32_t messageId() const override;
    void messageId(const uint32_t);
    virtual CInterfaceKey interfaceKey() const override;
    void interfaceKey(const CInterfaceKey&);
    virtual boost::variant<RequestType, ReplyType> messageType() const override;
    void messageType(const boost::variant<RequestType, ReplyType>&);

private:
    CMessageContext mContext;
    uint32_t mMsgId;
    CInterfaceKey mInterfaceKey;
    boost::variant<RequestType, ReplyType> mMsgType;
};

class CRequestFailed : public CMessage
{
public:
    CRequestFailed();
    virtual ~CRequestFailed();
    void failedType(RequestFailed);
    RequestFailed failedType() const;
    void description(const std::string&);
    std::string description() const;

private:
    RequestFailed mFailedType;
    std::string mDescription;
};

//-----------------------------------------------------------------------------

inline CMessageContext::CMessageContext() : mSequenceNumber(0)
{
}

inline CMessageContext::CMessageContext(std::weak_ptr<IMessageConsumer> token,
                                        uint32_t seqNo)
    : mSequenceNumber(seqNo), mToken(token)
{
}

inline std::weak_ptr<IMessageConsumer> CMessageContext::token() const
{
    return mToken;
}

inline void CMessageContext::token(std::weak_ptr<IMessageConsumer> c)
{
    mToken = c;
}

inline uint32_t CMessageContext::sequenceNumber() const
{
    return mSequenceNumber;
}

inline void CMessageContext::sequenceNumber(uint32_t s)
{
    mSequenceNumber = s;
}

inline CMessage::CMessage() : mContext(), mMsgId(0)
{
}

inline CMessage::~CMessage()
{
}

inline CMessageContext CMessage::context() const
{
    return mContext;
}

inline void CMessage::context(const CMessageContext& c)
{
    mContext = c;
}

inline uint32_t CMessage::messageId() const
{
    return mMsgId;
}

inline void CMessage::messageId(const uint32_t id)
{
    mMsgId = id;
}

inline CInterfaceKey CMessage::interfaceKey() const
{
    return mInterfaceKey;
}

inline void CMessage::interfaceKey(const CInterfaceKey& ikey)
{
    mInterfaceKey = ikey;
}

inline boost::variant<RequestType, ReplyType> CMessage::messageType() const
{
    return mMsgType;
}

inline void
CMessage::messageType(const boost::variant<RequestType, ReplyType>& msgType_)
{
    mMsgType = msgType_;
}

inline CVersion::CVersion() : mMajor(0), mMinor(0)
{
}

inline CVersion::CVersion(int major, int minor) : mMajor(major), mMinor(minor)
{
}

inline void CVersion::major(int m)
{
    mMajor = m;
}

inline int CVersion::major() const
{
    return mMajor;
}

inline void CVersion::minor(int m)
{
    mMinor = m;
}

inline int CVersion::minor() const
{
    return mMinor;
}

inline CInterfaceKey::CInterfaceKey(const std::string& name,
                                    const CVersion& version)
    : mName(name), mVersion(version)
{
}

inline std::string CInterfaceKey::name() const
{
    return mName;
}

inline void CInterfaceKey::name(const std::string& name_)
{
    mName = name_;
}

inline CVersion CInterfaceKey::version() const
{
    return mVersion;
}

inline void CInterfaceKey::version(const CVersion& version_)
{
    mVersion = version_;
}

inline bool CInterfaceKey::operator==(const CInterfaceKey& rhs) const
{
    return (mName == rhs.name() && mVersion.major() == rhs.version().major());
}

inline CRequestFailed::CRequestFailed()
{
}

inline CRequestFailed::~CRequestFailed()
{
}

inline void CRequestFailed::failedType(RequestFailed f)
{
    mFailedType = f;
}

inline RequestFailed CRequestFailed::failedType() const
{
    return mFailedType;
}

inline void CRequestFailed::description(const std::string& d)
{
    mDescription = d;
}

inline std::string CRequestFailed::description() const
{
    return mDescription;
}