# App Store #

Refer [Wiki](https://bitbucket.org/shanmukhananda/app-store/wiki/Home)

| Service            | Task  | Status                                        |
| ------------------ | ----- | --------------------------------------------- |
| Shippable          | Build | [![Build Status][shpbl_status]][shpbl_builds] |
| Bitbucket Pipeline | Build | [![Build Status][bbp_status]][bbp_builds]     |

[shpbl_status]:https://api.shippable.com/projects/59edd70faa46240700d761b9/badge?branch=master
[shpbl_builds]:https://app.shippable.com/bitbucket/shanmukhananda/app-store

[bbp_status]:https://bitbucket-badges.atlassian.io/badge/shanmukhananda/app-store.svg
[bbp_builds]:https://bitbucket.org/shanmukhananda/app-store/addon/pipelines/home
